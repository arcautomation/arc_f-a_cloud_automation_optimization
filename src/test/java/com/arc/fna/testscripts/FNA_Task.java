package com.arc.fna.testscripts;


import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;






import com.arc.fna.pages.CollectionTaskPage;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;


@Listeners(EmailReport.class)

public class FNA_Task 
{
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	CollectionTaskPage collectionTaskPage;
	
	@Parameters("browser")
	@BeforeMethod(groups="sekhar_test")
	public WebDriver beforeTest(String browser) 
	{
		
		if(browser.equalsIgnoreCase("firefox")) 
		{
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		else if (browser.equalsIgnoreCase("chrome"))
		{ 
		File dest = new File("./drivers/win/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver( options );
		driver.get(PropertyReader.getProperty("SkysiteProdURL"));	 
		} 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
	  return driver;
	}
	
	
	/** TC_001&002 (Collection Task): Verify User Add Task with name and subject.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = true, description = "TC_001&002 (Collection Task): Verify User Add Task with name and subject.")
	public void verifyaddteam_member_Task () throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_001&002 (Collection Task): Verify User Add Task with name and subject.");
			
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			String Collection_Name = PropertyReader.getProperty("SelectCollectionTask"); 			
			Log.assertThat(fnaHomePage.select_collection(Collection_Name), "Select collection Successful with valid credential", "Select collection unsuccessful with valid credential", driver);
			driver.switchTo().defaultContent();
			collectionTaskPage = new CollectionTaskPage(driver).get();
			String TaskName= collectionTaskPage.Random_Task();
			String SubjectName = collectionTaskPage.Random_Subject();
			String ContactName = PropertyReader.getProperty("CollectionEmp");	
			String Email = PropertyReader.getProperty("CollectionEmployee");
			Log.assertThat(collectionTaskPage.Add_Task_withSubject(TaskName,SubjectName,ContactName,Email), "Add team member Successful with valid credential", "Add team member  unsuccessful with valid credential", driver);
				
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	

	/** TC_003&004 (Collection Task): Verify Attach file(internal and external)in task.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 1, enabled = true, description = "TC_003&004 (Collection Task): Verify Attach file(internal and external)in task.",groups="sekhar_test")
	public void verifyAttach_File_Task () throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_003&004 (Collection Task): Verify Attach file(internal and external)in task.");
			
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			String Collection_Name = PropertyReader.getProperty("SelectCollectionTask"); 			
			Log.assertThat(fnaHomePage.select_collection(Collection_Name), "Select collection Successful with valid credential", "Select collection unsuccessful with valid credential", driver);
			driver.switchTo().defaultContent();
			collectionTaskPage = new CollectionTaskPage(driver).get();
			String TaskName= collectionTaskPage.Random_Task();
			String SubjectName = collectionTaskPage.Random_Subject();
			String ContactName = PropertyReader.getProperty("CollectionEmp");	
			String Email = PropertyReader.getProperty("CollectionEmployee");
			Log.assertThat(collectionTaskPage.Add_Task_withSubject(TaskName,SubjectName,ContactName,Email), "Add team member Successful with valid credential", "Add team member unsuccessful with valid credential", driver);
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();		
			Log.assertThat(collectionTaskPage.Attach_file_task(FolderPath,TaskName,SubjectName,ContactName), "With Attach file in collection task Successful with valid credential", "With Attach file in collection task unsuccessful with valid credential", driver);
				
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
}