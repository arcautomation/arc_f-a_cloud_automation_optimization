package com.arc.fna.testscripts;


import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.AccountTeamPage;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;


@Listeners(EmailReport.class)

public class FNA_AccountTeam 
{
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	AccountTeamPage accountTeamPage;
	
	
	@Parameters("browser")
	@BeforeMethod(groups="sekhar_test")
	public WebDriver beforeTest(String browser) 
	{
		
		if(browser.equalsIgnoreCase("firefox")) 
		{
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		else if (browser.equalsIgnoreCase("chrome"))
		{ 
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
						
		
			HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
			chromePrefs.put("profile.default_content_settings.popups", 0);
			chromePrefs.put("safebrowsing.enabled", "true"); 
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("prefs", chromePrefs);		
	
			options.addArguments("--start-maximized");
			driver = new ChromeDriver( options );
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));	 
		} 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
	  return driver;
	}
	
	/** TC_001 (Account Team): Verify user adding multiple projects to Account team.
	 *  Scripted By: Sekhar 		 
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = true, description = "TC_001 (Account Team): Verify user Account team adding to multiple projects.")
	public void verifyAddMultipleprojects_toAccountTeam() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_001 (Account Team): Verify user Account team adding to multiple projects.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("CollectionAccountteam");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String CollectionName1= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName1);			
			driver.switchTo().defaultContent();
			accountTeamPage = new AccountTeamPage(driver).get();			
			Log.assertThat(accountTeamPage.Manageteam_Association_Accountteam(CollectionName,CollectionName1), "Add multiple projects to Account team successfull", "Add multiple projects to Account team Unsuccessfull", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_002 (Account Team): Verify user adding multiple Account team to collection.
	 *  Scripted By: Sekhar 		 
	 * @throws Exception
	 */
	@Test(priority = 1, enabled = true, description = "TC_002 (Account Team): Verify user Account team adding to multiple projects.")
	public void verifyAddmultipleAccountteam_Collection() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_002 (Account Team): Verify user Account team adding to multiple projects.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("CollectionAccountteam");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);							
			driver.switchTo().defaultContent();
			accountTeamPage = new AccountTeamPage(driver).get();			
			Log.assertThat(accountTeamPage.AddMultiple_Account_team_Collection(CollectionName), "Add Account team to project successfull", "Add Account team to project Unsuccessfull", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_003 (Account Team): Verify addition of team to project directly from account team.
	 *  Scripted By: Sekhar 		 
	 * @throws Exception
	 */
	@Test(priority = 2, enabled = true, description = "TC_003 (Account Team): Verify addition of team to project directly from account team.")
	public void verifyaddition_team_Collection_Accountteam() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_003 (Account Team): Verify addition of team to project directly from account team.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("CollectionAccountteam");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);							
			driver.switchTo().defaultContent();
			accountTeamPage = new AccountTeamPage(driver).get();			
			Log.assertThat(accountTeamPage.addition_team_Collection_Accountteam(CollectionName), "addition of team to project directly from account team successfull", "addition of team to project directly from account team Unsuccessfull", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_004 (Account Team): Verify Edit team in account team.
	 *  Scripted By: Sekhar 		 
	 * @throws Exception
	 */
	@Test(priority = 3, enabled = true, description = "TC_004 (Account Team): Verify Edit team in account team.")
	public void verifyAdd_Edit_Accountteam() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_004 (Account Team): Verify Edit team in account team.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("CollectionAccountteam");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);							
			driver.switchTo().defaultContent();
			accountTeamPage = new AccountTeamPage(driver).get();	
			String TeamName= accountTeamPage.Random_Teamname();
			String ContactName = PropertyReader.getProperty("ContactAccountTeam");
			String EditTeam= accountTeamPage.Random_Editname();			
			Log.assertThat(accountTeamPage.Add_Edit_Accountteam(TeamName,ContactName,EditTeam), "Edit team in account team successfull", "Edit team in account team Unsuccessfull", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_005 (Account Team): Verify Delete team in account team.
	 *  Scripted By: Sekhar 		 
	 * @throws Exception
	 */
	@Test(priority = 4, enabled = true, description = "TC_005 (Account Team): Verify Delete team in account team.")
	public void verifyAdd_Delete_Accountteam() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_005 (Account Team): Verify Delete team in account team.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("CollectionAccountteam");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);							
			driver.switchTo().defaultContent();
			accountTeamPage = new AccountTeamPage(driver).get();	
			String TeamName= accountTeamPage.Random_Teamname();
			String ContactName = PropertyReader.getProperty("ContactAccountTeam");				
			Log.assertThat(accountTeamPage.Add_Delete_Accountteam(TeamName,ContactName), "Delete team in account team successfull", "Delete team in account team Unsuccessfull", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
}