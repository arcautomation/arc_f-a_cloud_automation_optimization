package com.arc.fna.testscripts;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.FNAAlbumPage;
import com.arc.fna.pages.FNA_BatchAttributePage;
import com.arc.fna.pages.FNA_HomeExtraModulePAge;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.RecordingClass;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)
public class FNA_BatchAttribute 
{
	 
		static WebDriver driver;
		LoginPage loginpage;
	    FnaHomePage fnahomepage;
	    FNAAlbumPage fnaalbumpage;
	    FNA_HomeExtraModulePAge fnahomextramodule;
	    FNA_BatchAttributePage fnabatchattribute;
	    @Parameters("browser")
	    @BeforeMethod
	    public WebDriver beforeTest(String browser) 
	    {
			
	    	if(browser.equalsIgnoreCase("firefox")) 
	    	{
	    		File dest = new File("./drivers/win/geckodriver.exe");
	    		//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
	    		System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
	    		driver = new FirefoxDriver();
	    		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	        }
	    	else if (browser.equalsIgnoreCase("chrome")) 
	    	{ 
	    		File dest = new File("./drivers/win/chromedriver.exe");
	    		System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
	            Map<String, Object> prefs = new HashMap<String, Object>();
	            prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
	            ChromeOptions options = new ChromeOptions();
	        	options.addArguments("--start-maximized");
	            options.setExperimentalOption("prefs", prefs);
	            driver = new ChromeDriver( options );
	    		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	 
	        } 
	    	else if (browser.equalsIgnoreCase("safari"))
	    	{
				System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
				driver = new SafariDriver();
				driver.get(PropertyReader.getProperty("SkysiteProdURL"));
				
	    	}
	   
	    	return driver;
	}
	   
	    
	    
	    /**TC_001(Batch attributes):Verify and update general properties of the file
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=0, enabled=true, description= "TC_001(Batch attributes):Verify and update general properties of the file")
	    public void modifyattributesVendorPName() throws Exception 
	    {
	    	  try 
	    	  {
				    Log.testCaseInfo("TC_001(Batch attributes):Verify and update general properties of the file");
				    
				    loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("Emailfortest");
				    String pWord=PropertyReader.getProperty("Password1");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnahomepage.selectcollection(collectionname);
					fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					
					String filename =fnabatchattribute.Random_filename();
				    String  title=fnabatchattribute.Random_title();
				    String description=fnabatchattribute.Random_description();
				  	String Foldername= PropertyReader.getProperty("Foldername");
					fnabatchattribute.selectFileandModifyAttribute(Foldername);
					
					Log.assertThat(fnabatchattribute.modifyGeneralAttributes(filename,title,description)," File details are updated and now visible under web table "," File details are not  updated and not visible under web table ", driver);
				   
			  }
			   
			  catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		             
		      }
		      finally
		      {
		             Log.endTestCase();
		             driver.quit();
		             
		      }
			 
			 
		}
	    
	    
	    /**TC_002(Batch Attrbibutes):Update and verify custom properties like Vendor and Project no of a file
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=1, enabled=true, description="Update and verify custom properties like Vendor and Project no of a file")
	    public void updateAndVerifyVendorPNo() throws Exception 
	    {
	    	  try 
	    	  {
				    Log.testCaseInfo("TC_002(Batch Attrbibutes):Update and verify custom properties like Vendor and Project no of a file");
				    
				    loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("Emailfortest");
				    String pWord=PropertyReader.getProperty("Password1");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnahomepage.selectcollection(collectionname);
					String FolderName= fnahomepage.Random_Foldername();		
					Log.assertThat(fnahomepage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
					
					File fis=new  File(PropertyReader.getProperty("FolderPathFileForSearch"));
      		       	String filepath=fis.getAbsolutePath().toString();
      		        File fis1=new  File(PropertyReader.getProperty("tempfilepath"));
		       	    String tempfilepath=fis1.getAbsolutePath().toString();
		       	    fnahomepage.UploadFileForDeletion(filepath,tempfilepath);
		       	    
					fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					fnabatchattribute.selectFileandModifyAttribute(FolderName);
					String Projectno=fnabatchattribute.Random_Projectno();
					
	     		    Log.assertThat(fnabatchattribute.customPropertiesVendorAndPNo(Projectno),"Project no and vendor name has been updated","Project no and vendor has not been updated");
	     		    
	     		    fnabatchattribute.deleteFolderFromCollection();
	    					   
			  }
			   
			  catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
		      
	    	  finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
			 
	   }
	    
	    
	    
	    /**TC_003(Batch Attrbibutes):Update and verify custom properties like Document type,invoice and Project name of a file
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=2, enabled=true, description="Update and verify custom properties like Document type,invoice and Project name of a file")
	    public void updateAndVerifyDtypeAndInvoiceAndProName() throws Exception 
	    {
	    	  try 
	    	  {
				    Log.testCaseInfo("TC_003(Batch Attrbibutes):Update and verify custom properties like Document type,invoice and Project name of a file");
				    
				    loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("Emailfortest");
				    String pWord=PropertyReader.getProperty("Password1");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnahomepage.selectcollection(collectionname);
					String FolderName= fnahomepage.Random_Foldername();		
					Log.assertThat(fnahomepage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
					
					File fis=new  File(PropertyReader.getProperty("FolderPathFileForSearch"));
    		       	String filepath=fis.getAbsolutePath().toString();
    		        File fis1=new  File(PropertyReader.getProperty("tempfilepath"));
		       	    String tempfilepath=fis1.getAbsolutePath().toString();
		       	    fnahomepage.UploadFileForDeletion(filepath,tempfilepath);
					
		       	    fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					fnabatchattribute.selectFileandModifyAttribute(FolderName);
				    String projectname=fnabatchattribute.Random_Projectname();
				    String invoice=fnabatchattribute.Random_Invoice();
				    
				    Log.assertThat(fnabatchattribute.modifyDocumentTypeInvoiceAndPName(projectname,invoice),"Document type,invoice and project name are updated and visible","Document type,invoice and project name are not updated and visible");
				    
				    fnabatchattribute.deleteFolderFromCollection();
			    	   
			  }
			   
			   catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
		      
	    	  finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
	     }
	    
	    
	    
	    /**TC_004(Batch_Attribute):Verify Search with document type 
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=3, enabled=true, description="Verify Search with document type")
	    public void verifySearchDocumentType() throws Exception 
	    {
	    	  try 
	    	  {
	    		  	
	    		  	Log.testCaseInfo("TC_004(Batch_Attribute):Verify Search with document type ");
	    		  
	    		  	loginpage = new LoginPage(driver).get();
	    		  	String uName=PropertyReader.getProperty("Emailfortest");
	    		  	String pWord=PropertyReader.getProperty("Password1");
	    		  	fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
	    		  	fnahomepage.loginValidation();
				  
	    		  	String collectionname=PropertyReader.getProperty("Collectioname");
	    		  	fnahomepage.selectcollection(collectionname);
	    		  	fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					String Foldername= PropertyReader.getProperty("Foldernamesearch");
					fnabatchattribute.selectFileandModifyAttribute(Foldername);
					
		            Log.assertThat(fnabatchattribute.DocumentTypeSearch()," Document Search with document type attribute is successfull","Document cannot be searched with document type attribute");
		        			
			  }
			   
			  catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
		      
	    	  finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
	    	  
	  }
	    
	    
	    
	    /**TC_005(Batch_Attribute):Verify Search with invoice
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=4, enabled=true, description="Verify Search with invoice")
	    public void verifySearchInvoice() throws Exception 
	    {
	    	  try 
	    	  {
	    		  
	    		  	Log.testCaseInfo("TC_005(Batch_Attribute):Verify Search with invoice");
	    		  	
				    loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("Emailfortest");
				    String pWord=PropertyReader.getProperty("Password1");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnahomepage.selectcollection(collectionname);
					fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					String Foldername= PropertyReader.getProperty("Foldernamesearch");
					fnabatchattribute.selectFileandModifyAttribute(Foldername);
					
		            Log.assertThat(fnabatchattribute.searchInvoice(),"Search with invoice is successfull", "Search with invoice is not  successfull");
		            
			  }
			   
			   catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
	    	  
		      finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
	    	
	    }
	    
	    
	    
	    /**TC_006(Batch_Attribute):Verify Search with Project number
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=5, enabled=true, description="Verify Search with Project number")
	    public void verifySearchwithProjectNumber() throws Exception 
	    {
	    	  try 
	    	  {
	    		    Log.testCaseInfo("TC_006(Batch_Attribute):Verify Search with Project number");
	    		    
				    loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("Emailfortest");
				    String pWord=PropertyReader.getProperty("Password1");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnahomepage.selectcollection(collectionname);
					fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					String Foldername= PropertyReader.getProperty("Foldernamesearch");
					fnabatchattribute.selectFileandModifyAttribute(Foldername);
					
		            Log.assertThat(fnabatchattribute.searchWithProjectNumber(),"Search with project number is successfull", "Search with project number is not  successfull");
			  }
			   
			   catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
	    	  
		      finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
	    	
	 }
	    
	    
	    /**TC_007(Batch_Attribute):Verify Search with Vendor
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=6, enabled=true, description="Verify Search with Vendor")
	    public void verifySearchwithVendor() throws Exception 
	    {
	    	  try 
	    	  {
	    		    
	    		  	Log.testCaseInfo("TC_007(Batch_Attribute):Verify Search with Vendor");
	    		  	
				    loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("Emailfortest");
				    String pWord=PropertyReader.getProperty("Password1");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnahomepage.selectcollection(collectionname);
					fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					String Foldername= PropertyReader.getProperty("Foldernamesearch");
					fnabatchattribute.selectFileandModifyAttribute(Foldername);
					
		            Log.assertThat(fnabatchattribute.searchWithVendor(),"Search with vendor is successfull", "Search with invoice is not  successfull");
		            
			  }
			   
			   catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
		      finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
	    	
	    }
	    
	    
	    
	    
	    /**TC_008(Batch_Attribute):Verify Search with project name
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=7, enabled=true, description="Verify Search with project name")
	    public void verifySearchwithProjectName() throws Exception 
	    {
	    	  try 
	    	  {
	    		    
	    		  	Log.testCaseInfo("TC_008(Batch_Attribute):Verify Search with project name");
	    		    
				    loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("Emailfortest");
				    String pWord=PropertyReader.getProperty("Password1");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnahomepage.selectcollection(collectionname);
					fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					String Foldername= PropertyReader.getProperty("Foldernamesearch");
					fnabatchattribute.selectFileandModifyAttribute(Foldername);
					
		            Log.assertThat(fnabatchattribute.searchWithProjectName(),"Search with  project name is successfull", "Search with project name is not working");
			  }
			   
			   catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
		      finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
	    	
	    }
	    
	    
	    
	    
	    /**TC_009(Batch_Attribute):Verify Search with Keyword
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=8, enabled=true, description="Verify Search with Keyword")
	    public void verifySearchwithKeyword() throws Exception 
	    {
	    	  try 
	    	  {
	    		    Log.testCaseInfo("TC_009(Batch_Attribute):Verify Search with Keyword");
				    
	    		    loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("Emailfortest");
				    String pWord=PropertyReader.getProperty("Password1");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnahomepage.selectcollection(collectionname);
					fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					String Foldername= PropertyReader.getProperty("Keywordsearch");
					fnabatchattribute.selectFileandModifyAttribute(Foldername);
					
		            Log.assertThat(fnabatchattribute.searchWithKeyword(),"Search with Keyword is successfull", "Search with Keyword is not working");
		            
			  }
			   
			   catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
		      finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
	    	
	    }
	    
	
	    
	    
	    /**TC_0010(Batch_Attribute):Verify update default attribute to file while upload and search with that attribute
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=9, enabled=true, description="Verify add attribute to file while upload and search with that attribute")
	    public void verifyAddAttributetoFileAndSearch() throws Exception 
	    {
	    	  try 
	    	  {
	    		    Log.testCaseInfo("TC_0010(Batch_Attribute):Verify add attribute to file while upload and search with that attribute");
				    
	    		    loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("Emailfortest");
				    String pWord=PropertyReader.getProperty("Password1");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnahomepage.selectcollection(collectionname);
					String FolderName= fnahomepage.Random_Foldername();		
					Log.assertThat(fnahomepage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
					
					File fis=new  File(PropertyReader.getProperty("Filepathforupload"));
      		       	String filepath=fis.getAbsolutePath().toString();
      		        File fis1=new  File(PropertyReader.getProperty("tempfilepath"));
		       	    String tempfilepath=fis1.getAbsolutePath().toString();
		       	    fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					String invoice=fnabatchattribute.Random_Invoice();
					fnabatchattribute.UploadFileAddAttribute(filepath, tempfilepath,invoice);
					
					fnabatchattribute.selectFileandModifyAttribute(FolderName);
					
			        Log.assertThat(fnabatchattribute.searchInvoiceonupdatedfile(invoice),"Search with updated invoice is successfull", "Search with updated invoice is not successfull");
			        
			        fnabatchattribute.deleteFolderFromCollection();
			        
	    	  }
			   
			   catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
		      finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
	    	
	    }   
	    
	    
	    
	    
	    /**TC_0011(Batch_Attribute):Verify update custom attribute to file while upload and search with that attribute
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=10, enabled=true, description="Verify update custom attribute to file while upload and search with that attribute")
	    public void verifyAddCustomAttributetoFileAndSearch() throws Exception 
	    {
	    	  try 
	    	  {
	    		    
	    		  	Log.testCaseInfo("TC_0011(Batch_Attribute):Verify update custom attribute to file while upload and search with that attribute");
				    
	    		  	loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("Emailfortest");
				    String pWord=PropertyReader.getProperty("Password1");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname2");
					fnahomepage.selectcollection(collectionname);
					String FolderName= fnahomepage.Random_Foldername();		
					Log.assertThat(fnahomepage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
					
					File fis=new  File(PropertyReader.getProperty("Filepathforupload"));
      		       	String filepath=fis.getAbsolutePath().toString();
      		        File fis1=new  File(PropertyReader.getProperty("tempfilepath"));
		       	    String tempfilepath=fis1.getAbsolutePath().toString();
		       	    fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					fnabatchattribute.UploadFileAddCustomAttribute(filepath, tempfilepath);
					
					fnabatchattribute.selectFileandModifyAttribute(FolderName);
					
			        Log.assertThat(fnabatchattribute.searchWithVendorwhenaddingattribute(),"Search with vendor is successfull", "Search with vendor is not successfull");
			        
			        fnabatchattribute.deleteFolderFromCollection();
	    	  }
			   
			   catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
		      finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
	    	
	    } 
	    
	    
	    
	    
	    /**TC_0012(Batch_Attribute):Verify search with All attribute & And operator
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=11, enabled=true, description="Verify search with All attribute & And operator")
	    public void verifySearchWithAllAttributeAndOperator() throws Exception 
	    {
	    	  try 
	    	  {
	    		    
	    		  	Log.testCaseInfo("TC_0012(Batch_Attribute):Verify search with All attribute & And operator");
				    
	    		  	loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("Emailfortest");
				    String pWord=PropertyReader.getProperty("Password1");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnahomepage.selectcollection(collectionname);
					fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					String Foldername= PropertyReader.getProperty("Foldernamesearchallattribute");
					fnabatchattribute.selectFileandModifyAttribute(Foldername);
		           
					Log.assertThat(fnabatchattribute.searchWithAllAttributeAndOperator()," Document Search with All attribute using and & operator working successfully","Document Search with All attribute using & operator not  working ");
		          	
			  }
			   
			   catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
		     
	    	  finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
	    
	    } 	
	    
	    
	    
	    
	    /**TC_0013(Batch Attrbibutes):Update attribute then modify then search with that attribute
	     * @throws Exception 
	     * 
	     */
	    @Test(priority=13, enabled=true, description="Update attribute then modify then search with that attribute")
	    public void VerifyadditionofAttributeandmodifyingThenSearching() throws Exception 
	    {
	    	  try 
	    	  {
				   
	    		  	Log.testCaseInfo("TC_0013(Batch Attrbibutes):Update attribute then modify then search with that attribute");
	    		  	
				    loginpage = new LoginPage(driver).get();
				    String uName=PropertyReader.getProperty("Emailfortest");
				    String pWord=PropertyReader.getProperty("Password1");
				    fnahomepage=loginpage.loginWithValidCredential(uName,pWord);
				    fnahomepage.loginValidation();
				    
				    String collectionname=PropertyReader.getProperty("Collectioname");
					fnahomepage.selectcollection(collectionname);
					String FolderName= fnahomepage.Random_Foldername();		
					Log.assertThat(fnahomepage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			
					File fis=new  File(PropertyReader.getProperty("FolderPathFileForSearch"));
      		       	String filepath=fis.getAbsolutePath().toString();
      		        File fis1=new  File(PropertyReader.getProperty("tempfilepath"));
		       	    String tempfilepath=fis1.getAbsolutePath().toString();
		       	    fnahomepage.UploadFileForDeletion(filepath,tempfilepath);
		       	    
					fnabatchattribute=fnahomepage.clickdocumentsforbatchattribute();
					fnabatchattribute.selectFileandModifyAttribute(FolderName);
					String Projectno=fnabatchattribute.Random_Projectno();
					
	     		    Log.assertThat(fnabatchattribute.customPropertiesVendorAndPNo(Projectno),"Project no and vendor name has been updated","Project no and vendor has not been updated");
	     			
	     		    fnabatchattribute.selectFileandModifyAttribute(FolderName);	     			
	     		    String updatedprojectno=fnabatchattribute.Random_UpdateProjectno();
	     			String vendornoupdate=PropertyReader.getProperty("UpdatedVendor2");	     			
	     			fnabatchattribute.modifyAddedAttributes(updatedprojectno,vendornoupdate);   
	     			
	     			fnabatchattribute.selectFileandModifyAttribute(FolderName);
	     			fnabatchattribute.searchUpdatedValue(updatedprojectno,vendornoupdate);
	     		    fnabatchattribute.deleteFolderFromCollection();
	     		    
	    	
	    	  }
			   
			  catch(Exception e)
		      {
		             e.getCause();
		             Log.exception(e, driver);
		      }
		      finally
		      {
		             Log.endTestCase();
		             driver.quit();
		      }
			 	    
	    
	    }
	    
}