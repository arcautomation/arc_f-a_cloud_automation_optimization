package com.arc.fna.testscripts;


import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.CommunicationPage;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;


@Listeners(EmailReport.class)
public class FNA_Communications
{
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	CommunicationPage communicationPage;
	
	@Parameters("browser")
	@BeforeMethod(groups="sekhar_test")
	public WebDriver beforeTest(String browser) 
	{
		
		if(browser.equalsIgnoreCase("firefox")) 
		{
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		else if (browser.equalsIgnoreCase("chrome")) 
		{ 
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			driver = new ChromeDriver( options );
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));	 
		} 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
	  return driver;
	}
	
	/** TC_001 (Communications): Verify user able add communication with Subject  
	*  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = true, description = "TC_001 (Communications):  Verify user able add communication with Subject")
	public void verifyCreate_Communication() throws Exception
	{		
		try
		{	
			Log.testCaseInfo("TC_001 (Communications): Verify user able add communication with Subject");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String Collection_Name = PropertyReader.getProperty("Communicationcolle"); 			
			Log.assertThat(fnaHomePage.select_collection(Collection_Name), "Select collection Successful with valid credential", "Select collection unsuccessful with valid credential", driver);
			driver.switchTo().defaultContent();
			communicationPage = new CommunicationPage(driver).get();			
			String SubjectName= communicationPage.Random_Subject();
			String ContactName = PropertyReader.getProperty("CollectionEmp");	
			String Email = PropertyReader.getProperty("CollectionEmployee");	
			Log.assertThat(communicationPage.Create_communication(ContactName,Email,SubjectName), "Create Communication Successful with valid credential", "Create Communication unsuccessful with valid credential", driver);
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_002&003 (Communications): Verify User Add attachment from computer and collection.
	*  Scripted By: Sekhar 
	 * @throws Exception
	 */
	@Test(priority = 1, enabled = true, description = "TC_002&003 (Communications): Verify User Add attachment from computer and collection.")
	public void verifyAttchments_Communications() throws Exception
	{		
		try
		{			
			Log.testCaseInfo("TC_002&003 (Communications): Verify User Add attachment from computer and collection.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String Collection_Name = PropertyReader.getProperty("Communicationcolle"); 			
			Log.assertThat(fnaHomePage.select_collection(Collection_Name), "Select collection Successful with valid credential", "Select collection unsuccessful with valid credential", driver);
			driver.switchTo().defaultContent();
			communicationPage = new CommunicationPage(driver).get();			
			String SubjectName= communicationPage.Random_Subject();
			String ContactName = PropertyReader.getProperty("CollectionEmp");	
			String Email = PropertyReader.getProperty("CollectionEmployee");	
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();		
			Log.assertThat(communicationPage.Attchments_communication(ContactName,Email,SubjectName,FolderPath), "With Attachments in communications Successful with valid credential", "With Attachments in communications unsuccessful with valid credential", driver);
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
}