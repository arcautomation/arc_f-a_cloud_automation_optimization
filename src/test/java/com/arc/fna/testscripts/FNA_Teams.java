package com.arc.fna.testscripts;


import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.CollectionTeamsPage;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;


@Listeners(EmailReport.class)
public class FNA_Teams {
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	CollectionTeamsPage collectionTeamsPage;
	
	@Parameters("browser")
	@BeforeMethod(groups="sekhar_test")
	public WebDriver beforeTest(String browser) {
		
		if(browser.equalsIgnoreCase("firefox")) {
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	}
		else if (browser.equalsIgnoreCase("chrome")) { 
		File dest = new File("./drivers/win/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver( options );
		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	 
	  } 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
	  return driver;
	}
	
	/** TC_01&02 (Collection Teams): Verify add existing team member from more option added team member as member in to that particular team.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = true, description = "TC_01&02 (Collection Teams): Verify add existing team member from more option added team member as member in to that particular team.")
	public void verifyaddteam_member () throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_01&02 (Collection Teams):Verify add existing team member from more option added team member as member in to that particular team.");
			
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			fnaHomePage.Accepted_Popup_close1();
			String Collection_Name = PropertyReader.getProperty("SelectCollection"); 			
			Log.assertThat(fnaHomePage.select_collection(Collection_Name), "Select collection Successful with valid credential", "Select collection unsuccessful with valid credential", driver);
			driver.switchTo().defaultContent();
			collectionTeamsPage = new CollectionTeamsPage(driver).get();
			String Teams= collectionTeamsPage.Random_Teams();
			String ContactName = PropertyReader.getProperty("Collectionowner");		
			Log.assertThat(collectionTeamsPage.Add_team_member(Teams,ContactName), "Add team member Successful with valid credential", "Add team member  unsuccessful with valid credential", driver);
			String ContactName1 = PropertyReader.getProperty("CollectionEmp");
			Log.assertThat(collectionTeamsPage.Add_Existing_team_member(ContactName1), "Add existing team member Successful with valid credential", "Add existing team member  unsuccessful with valid credential", driver);
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	/** TC_03 (Collection Teams): Verify Delete team member from more option that particular team.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 1, enabled = true, description = "TC_03 (Collection Teams): Verify Delete team member from more option that particular team.")
	public void verifydelete_teammember () throws Exception
	{
		
		try
		{	
			Log.testCaseInfo("TC_03 (Collection Teams):Verify Delete team member from more option that particular team.");
			
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			String Collection_Name = PropertyReader.getProperty("SelectCollection"); 			
			Log.assertThat(fnaHomePage.select_collection(Collection_Name), "Select collection Successful with valid credential", "Select collection unsuccessful with valid credential", driver);
			driver.switchTo().defaultContent();
			collectionTeamsPage = new CollectionTeamsPage(driver).get();
			String Teams= collectionTeamsPage.Random_Teams();
			String ContactName = PropertyReader.getProperty("Collectionowner");			
			Log.assertThat(collectionTeamsPage.Add_team_member(Teams,ContactName), "Add team member Successful with valid credential", "Add team member  unsuccessful with valid credential", driver);
			String ContactName1 = PropertyReader.getProperty("CollectionEmp");
			Log.assertThat(collectionTeamsPage.Add_Existing_team_member(ContactName1), "Add existing team member Successful with valid credential", "Add existing team member  unsuccessful with valid credential", driver);
			Log.assertThat(collectionTeamsPage.Delete_team_member(ContactName1), "Delete team member Successful with valid credential", "Delete team member unsuccessful with valid credential", driver);
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_04 (Collection Teams): Verify member search should word if user select add existing team member.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 2, enabled = true, description = "TC_04 (Collection Teams): Verify member search should word if user select add existing team member.")
	public void verifysearch_Existingteammember () throws Exception
	{
		
		try
		{	
			Log.testCaseInfo("TC_04 (Collection Teams):Verify member search should word if user select add existing team member.");
			
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			String Collection_Name = PropertyReader.getProperty("SelectCollection"); 			
			Log.assertThat(fnaHomePage.select_collection(Collection_Name), "Select collection Successful with valid credential", "Select collection unsuccessful with valid credential", driver);
			driver.switchTo().defaultContent();
			collectionTeamsPage = new CollectionTeamsPage(driver).get();
			String Teams= collectionTeamsPage.Random_Teams();
			String ContactName = PropertyReader.getProperty("Collectionowner");			
			Log.assertThat(collectionTeamsPage.Add_team_member(Teams,ContactName), "Add team member Successful with valid credential", "Add team member  unsuccessful with valid credential", driver);
			String ContactName1 = PropertyReader.getProperty("CollectionEmp");
			Log.assertThat(collectionTeamsPage.Add_Existing_team_member(ContactName1), "Add existing team member Successful with valid credential", "Add existing team member  unsuccessful with valid credential", driver);
			Log.assertThat(collectionTeamsPage.Search_team_member(ContactName1), "Search member in existing team member Successful with valid credential", "Search member in existing team member  unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_05 (Collection Teams): Verify member search should word if user select add team member from address book.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 3, enabled = true, description = "TC_05 (Collection Teams): Verify member search should word if user select add team member from address book.")
	public void verifysearch_addteammember_addressbook () throws Exception
	{
		try
		{	

			Log.testCaseInfo("TC_05 (Collection Teams):Verify member search should word if user select add team member from address book.");
			
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			String Collection_Name = PropertyReader.getProperty("SelectCollection"); 			
			Log.assertThat(fnaHomePage.select_collection(Collection_Name), "Select collection Successful with valid credential", "Select collection unsuccessful with valid credential", driver);
			driver.switchTo().defaultContent();
			collectionTeamsPage = new CollectionTeamsPage(driver).get();
			String Teams= collectionTeamsPage.Random_Teams();
			String ContactName = PropertyReader.getProperty("Collectionowner");			
			Log.assertThat(collectionTeamsPage.Add_team_member(Teams,ContactName), "Add team member Successful with valid credential", "Add team member  unsuccessful with valid credential", driver);
			String ContactName1 = PropertyReader.getProperty("CollectionEmp");
			Log.assertThat(collectionTeamsPage.Add_team_member_fromaddressbook(ContactName1), "Add team member from address book Successful with valid credential", "Add team member from address book  unsuccessful with valid credential", driver);
			Log.assertThat(collectionTeamsPage.Search_team_member(ContactName1), "Search member in team member from address book Successful with valid credential", "Search member in team member from address book unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_06 (Collection Teams): Verify edit team and delete teams.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 4, enabled = true, description = "TC_06 (Collection Teams): Verify edit team and delete teams..")
	public void verifyEditicon_Deleteicon () throws Exception
	{
		
		try
		{		
			Log.testCaseInfo("TC_06 (Collection Teams): Verify edit team and delete teams.");
			
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			String Collection_Name = PropertyReader.getProperty("SelectCollection"); 			
			Log.assertThat(fnaHomePage.select_collection(Collection_Name), "Select collection Successful with valid credential", "Select collection unsuccessful with valid credential", driver);
			driver.switchTo().defaultContent();
			collectionTeamsPage = new CollectionTeamsPage(driver).get();
			String Teams= collectionTeamsPage.Random_Teams();
			String ContactName = PropertyReader.getProperty("Collectionowner");
			Log.assertThat(collectionTeamsPage.Add_team_member(Teams,ContactName), "Add team member Successful with valid credential", "Add team member  unsuccessful with valid credential", driver);
			Log.assertThat(collectionTeamsPage.Editteam_Deleteteams(Teams), "edit team and delete team Successful with valid credential", "edit team and delete team unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	/** TC_07 (Collection Teams): Verify Delete the Owner team.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 5, enabled = true, description = "TC_07 (Collection Teams): Verify Delete the Owner team.")
	public void verifyDelete_Ownerteam () throws Exception
	{
		
		try
		{	
			Log.testCaseInfo("TC_07 (Collection Teams): Verify Delete the Owner team.");
			
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();			
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);			
			driver.switchTo().defaultContent();
			collectionTeamsPage = new CollectionTeamsPage(driver).get();
			Log.assertThat(collectionTeamsPage.Delete_Ownerteam(), "Delete the owner team Successful with valid credential", "Delete the owner team unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	/*
	*//** TC_08 (Collection Teams): Verify Edit button should take the user to add ,update, delete the contacts in to the team.
	  *  Scripted By: Sekhar
	 * @throws Exception
	 *//*
	@Test(priority = 6, enabled = true, description = "TC_08 (Collection Teams): Verify Edit team.")
	public void verifyEditTeam() throws Exception
	{
		
		try
		{	
			Log.testCaseInfo("TC_08 (Collection Teams): Verify Edit team.");
			
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();			
			String Collection_Name = PropertyReader.getProperty("SelectCollection"); 			
			Log.assertThat(fnaHomePage.select_collection(Collection_Name), "Select collection Successful with valid credential", "Select collection unsuccessful with valid credential", driver);
			driver.switchTo().defaultContent();
			collectionTeamsPage = new CollectionTeamsPage(driver).get();			
			String Teams = PropertyReader.getProperty("CollectionEditName");
			String Conactname= collectionTeamsPage.Random_Contact();
			String Editname= collectionTeamsPage.Random_Edit();
			Log.assertThat(collectionTeamsPage.Editteam_Addmember_edit_Delete_Contact(Teams,Conactname,Editname), "Edit team after add & edit & delete contact Successful with valid credential", "Edit team after add & edit & delete contact unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	*//** TC_09 (Collection Teams): Verify all options in Edit team.
	  *  Scripted By: Sekhar 
	 * @throws Exception
	 *//*
	@Test(priority = 7, enabled = true, description = "TC_09 (Collection Teams): Verify alloptions in Edit team")
	public void verifyalloption_Editteam() throws Exception
	{
		
		try
		{	
			Log.testCaseInfo("TC_09 (Collection Teams): Verify alloptions in Edit team.");
			
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();			
			String Collection_Name = PropertyReader.getProperty("SelectCollection"); 			
			Log.assertThat(fnaHomePage.select_collection(Collection_Name), "Select collection Successful with valid credential", "Select collection unsuccessful with valid credential", driver);
			driver.switchTo().defaultContent();
			collectionTeamsPage = new CollectionTeamsPage(driver).get();			
			String Teams = PropertyReader.getProperty("CollectionEditName");
			Log.assertThat(collectionTeamsPage.Verify_alloption_Editteam(Teams), "all options in Edit team Successful with valid credential", "all options in Edit team unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}*/
			
	
}

