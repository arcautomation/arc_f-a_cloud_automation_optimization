package com.arc.fna.testscripts;


import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.GridExportPage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;


@Listeners(EmailReport.class)

public class FNA_GridExport
{
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	GridExportPage gridExportPage;
	
	@Parameters("browser")
	@BeforeMethod(groups="sekhar_test")
	public WebDriver beforeTest(String browser) 
	{
		
		if(browser.equalsIgnoreCase("firefox")) 
		{
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		else if (browser.equalsIgnoreCase("chrome"))
		{ 
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		

			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			driver = new ChromeDriver( options );
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));	 
		} 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		return driver;
	}
	
	/** TC_001 (Grid Export): Verify user exporting the csv and excel in collection team level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = true, description = "TC_001 (Grid Export): Verify user exporting the csv and excel in collection team level.")
	public void verifyExport_CollectionTeam() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_001 (Grid Export): Verify user exporting the csv and excel in collection team level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String Collection_Name = PropertyReader.getProperty("FNASelectCollection23");			
			fnaHomePage.selectcollection(Collection_Name);
			driver.switchTo().defaultContent();
			gridExportPage = new GridExportPage(driver).get();				
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";			
			String DownloadPath1="C:\\" + "Users\\" + usernamedir + "\\Downloads\\";
			String fileName = PropertyReader.getProperty("FNATeamFilename");
			Log.assertThat(gridExportPage.Export_CSV_CollectionTeam(DownloadPath,DownloadPath1,fileName), "Export as CSV in Teams Successfull", "Export as CSV in Teams UnSuccessful", driver);
			Log.assertThat(gridExportPage.Export_Excel_CollectionTeam(DownloadPath,DownloadPath1), "Export as Excel in Teams Successfull", "Export as Excel in Teams UnSuccessful", driver);
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_002 (Grid Export): Verify user exporting the csv and excel in collection list level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 1, enabled = true, description = "TC_002 (Grid Export): Verify user exporting the excel in collection list level.")
	public void verifyExport_CollectionList() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_002 (Grid Export): Verify user exporting the excel in collection list level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();				
			driver.switchTo().defaultContent();
			gridExportPage = new GridExportPage(driver).get();				
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";			
			String DownloadPath1="C:\\" + "Users\\" + usernamedir + "\\Downloads\\";
			String fileName = PropertyReader.getProperty("FNATeamFilename");
			Log.assertThat(gridExportPage.Export_CSV_CollectionList(DownloadPath,DownloadPath1,fileName), "Export as CSV in list Successfull", "Export as CSV in list UnSuccessful", driver);
			Log.assertThat(gridExportPage.Export_Excel_CollectionList(DownloadPath,DownloadPath1), "Export as Excel in list Successfull", "Export as Excel in list UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}	
	
	/** TC_003 (Grid Export): Verify user exporting the csv and excel in collection File Level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 2, enabled = true, description = "TC_003 (Grid Export): Verify user exporting the excel in collection File Level.",groups="sekhar_test")
	public void verifyExport_Collection_FileLevel() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_003 (Grid Export): Verify user exporting the excel in collection File Level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String Collection_Name = PropertyReader.getProperty("FNASelectCollection23");			
			fnaHomePage.selectcollection(Collection_Name);
			String FolderName = PropertyReader.getProperty("SelFolder");			
			fnaHomePage.Select_Folder(FolderName);	
			driver.switchTo().defaultContent();
			gridExportPage = new GridExportPage(driver).get();				
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";			
			String DownloadPath1="C:\\" + "Users\\" + usernamedir + "\\Downloads\\";
			String fileName = PropertyReader.getProperty("FNATeamFilename1");
			Log.assertThat(gridExportPage.Export_CSV_CollectionFile(DownloadPath,DownloadPath1,fileName), "Export as CSV in file level Successfull", "Export as CSV in file level  UnSuccessful", driver);
			Log.assertThat(gridExportPage.Export_Excel_CollectionFile(DownloadPath,DownloadPath1), "Export as Excel in file level  Successfull", "Export as Excel in file level  UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}	
	
	/** TC_004 (Grid Export): Verify user exporting the csv and excel in collection folder Level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 3, enabled = true, description = "TC_004 (Grid Export): Verify user exporting the excel in collection folder Level.")
	public void verifyExport_Collection_FolderLevel() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_004 (Grid Export): Verify user exporting the excel in collection folder Level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String Collection_Name = PropertyReader.getProperty("FNASelectCollection23");			
			fnaHomePage.selectcollection(Collection_Name);
			String FolderName = PropertyReader.getProperty("SelFolder");			
			fnaHomePage.Select_Folder(FolderName);	
			driver.switchTo().defaultContent();
			gridExportPage = new GridExportPage(driver).get();				
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";			
			String DownloadPath1="C:\\" + "Users\\" + usernamedir + "\\Downloads\\";
			String fileName = PropertyReader.getProperty("FNATeamFilename1");
			Log.assertThat(gridExportPage.Export_CSV_CollectionFolder(DownloadPath,DownloadPath1,fileName), "Export as CSV in folder Successfull", "Export as CSV in folder UnSuccessful", driver);
			Log.assertThat(gridExportPage.Export_Excel_CollectionFolder(DownloadPath,DownloadPath1), "Export as Excel in folder Successfull", "Export as Excel in folder UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}	
	
	/** TC_005 (Grid Export): Verify user exporting the csv and excel in manage users.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 4, enabled = true, description = "TC_005 (Grid Export): Verify user exporting the excel in manage users.")
	public void verifyExport_Manage_Users() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_005 (Grid Export): Verify user exporting the excel in manage users.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			driver.switchTo().defaultContent();
			gridExportPage = new GridExportPage(driver).get();				
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";			
			String DownloadPath1="C:\\" + "Users\\" + usernamedir + "\\Downloads\\";
			String fileName = PropertyReader.getProperty("FNATeamFilename12");
			Log.assertThat(gridExportPage.Export_CSV_Manage_Users(DownloadPath,DownloadPath1,fileName), "Export as CSV in manage users Successfull", "Export as CSV in manage users UnSuccessful", driver);
			Log.assertThat(gridExportPage.Export_Excel_Manage_Users(DownloadPath,DownloadPath1), "Export as Excel in manage users Successfull", "Export as Excel in manage users UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}	
		
	/** TC_006 (Grid Export): Verify user exporting the csv and excel in Communication.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 5, enabled = true, description = "TC_006 (Grid Export): Verify user exporting the excel in Communication.")
	public void verifyExport_Communication() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_006 (Grid Export): Verify user exporting the excel in Communication.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();			
			String Collection_Name = PropertyReader.getProperty("FNASelectCollection23");			
			fnaHomePage.selectcollection(Collection_Name);
			driver.switchTo().defaultContent();
			gridExportPage = new GridExportPage(driver).get();				
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";			
			String DownloadPath1="C:\\" + "Users\\" + usernamedir + "\\Downloads\\";
			String fileName = PropertyReader.getProperty("FNATeamFilename");
			Log.assertThat(gridExportPage.Export_CSV_Communication(DownloadPath,DownloadPath1,fileName), "Export as CSV in communications Successfull", "Export as CSV in communications UnSuccessful", driver);
			Log.assertThat(gridExportPage.Export_Excel_Communication(DownloadPath,DownloadPath1), "Export as Excel in communications Successfull", "Export as Excel in communications UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}	
		
}