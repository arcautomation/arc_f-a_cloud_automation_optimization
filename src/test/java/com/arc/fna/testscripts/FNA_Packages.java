package com.arc.fna.testscripts;


import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.ArchivedCollectionsPage;
import com.arc.fna.pages.CollectionPackagePage;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.pages.ProjectsFilesPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;


@Listeners(EmailReport.class)

public class FNA_Packages 
{
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	CollectionPackagePage collectionPackagePage;
	
	@Parameters("browser")
	@BeforeMethod(groups="sekhar_test")
	public WebDriver beforeTest(String browser) 
	{
		
		if(browser.equalsIgnoreCase("firefox")) 
		{
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		else if (browser.equalsIgnoreCase("chrome"))
		{ 
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		

			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			driver = new ChromeDriver( options );
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));	 
		} 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		return driver;
	}
	/** TC_001 (Packages): Verify user add new package in packages from collection files.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = true, description = "TC_001 (Packages): Verify user add new package in packages from collection files.")
	public void verifyCreatePackage_Within_Packages() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_001 (Packages): Verify user add new package in packages from collection files.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String Collection_Name = PropertyReader.getProperty("FNASelectCollection12");			
			fnaHomePage.selectcollection(Collection_Name);
			driver.switchTo().defaultContent();
			collectionPackagePage = new CollectionPackagePage(driver).get();			
			String PackageName= collectionPackagePage.Random_PackageName();	
			Log.assertThat(collectionPackagePage.AddPackage_Within_Packages(PackageName), "Create Package Successfull", "Create Package UnSuccessful", driver);
				
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_002 (Packages): Verify add attachment from collection in packages.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 1, enabled = true, description = "TC_002 (Packages): Verify add attachment from collection in packages.")
	public void verifyAdd_Attachment_FromCollection_Package() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_002 (Packages): Verify add attachment from collection in packages.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String Collection_Name = PropertyReader.getProperty("FNASelectCollection12");			
			fnaHomePage.selectcollection(Collection_Name);
			driver.switchTo().defaultContent();
			collectionPackagePage = new CollectionPackagePage(driver).get();			
			String PackageName= collectionPackagePage.Random_PackageName();	
			String Description = PropertyReader.getProperty("Description");
			Log.assertThat(collectionPackagePage.Add_Attachment_FromCollection(PackageName,Description), "Add attachment fron collection in Package Successfull", "Add attachment fron collection in Package UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_003 (Packages): Verify add attachment and Edit remove collection files in packages.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 2, enabled = true, description = "TC_003 (Packages): Verify add attachment and Edit remove collection files in packages.",groups="sekhar_test")
	public void verifyAdd_Attachment_Edit_remove_collectionfiles() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_003 (Packages): Verify add attachment and Edit remove collection files in packages.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String Collection_Name = PropertyReader.getProperty("FNASelectCollection12");			
			fnaHomePage.selectcollection(Collection_Name);
			driver.switchTo().defaultContent();
			collectionPackagePage = new CollectionPackagePage(driver).get();			
			String PackageName= collectionPackagePage.Random_PackageName();	
			String Description = PropertyReader.getProperty("Description");
			Log.assertThat(collectionPackagePage.Add_Attachment_FromCollection(PackageName,Description), "Add attachment fron collection in Package Successfull", "Add attachment fron collection in Package UnSuccessful", driver);
			Log.assertThat(collectionPackagePage.Add_Attachment_Edit_remove_collectionfiles(), "Edit and remove collectionfiles in Package Successfull", "Edit and remove collectionfiles in Package UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
}