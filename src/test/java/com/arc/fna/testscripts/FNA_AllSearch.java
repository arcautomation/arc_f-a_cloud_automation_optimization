package com.arc.fna.testscripts;


import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.AccountTeamPage;
import com.arc.fna.pages.AdvancedSearchPage;
import com.arc.fna.pages.CommunicationPage;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.GlobalSearchPage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.pages.MessageboardPage;
import com.arc.fna.pages.ModuleSearchPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;


@Listeners(EmailReport.class)

public class FNA_AllSearch 
{
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	AccountTeamPage accountTeamPage;
	GlobalSearchPage globalSearchPage;
	ModuleSearchPage moduleSearchPage;
	CommunicationPage communicationPage;
	MessageboardPage messageboardPage;
	AdvancedSearchPage advancedSearchPage;
	
	@Parameters("browser")
	@BeforeMethod(groups="sekhar_test")
	public WebDriver beforeTest(String browser) 
	{
		
		if(browser.equalsIgnoreCase("firefox")) 
		{
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		else if (browser.equalsIgnoreCase("chrome"))
		{ 
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
						
		
			HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
			chromePrefs.put("profile.default_content_settings.popups", 0);
			chromePrefs.put("safebrowsing.enabled", "true"); 
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("prefs", chromePrefs);		
	
			options.addArguments("--start-maximized");
			driver = new ChromeDriver( options );
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));	 
		} 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
	  return driver;
	}
	
	/** TC_003 (Manage User): Verify grid search of manage user page.
     * Scripted By: Sekhar
     * @throws Exception
    */
    @Test(priority = 0, enabled = true, description = "TC_003 (Manage User): Verify grid search of manage user page")
    public void verifyGridSearchManageUserPage() throws Exception
    {
           try
           {
                  Log.testCaseInfo("TC_003 (Manage User): Verify grid search of manage user page");
                  loginPage = new LoginPage(driver).get();                
                  String uName = PropertyReader.getProperty("ManageUsername");
      		   	   String pWord = PropertyReader.getProperty("Password");			
      		   	   fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
      		   	   fnaHomePage.loginValidation();
                  Log.assertThat(fnaHomePage.gridSearchWithUserNameManageUser(), "Grid search with user name of manage User Page Successful with valid credential", "Grid search with user name of manage User Page unsuccessful with valid credential", driver);
                  Log.assertThat(fnaHomePage.gridSearchWithMailManageUser(), "Grid search with mail of manage User Page Successful with valid credential", "Grid search with mail of manage User Page unsuccessful with valid credential", driver);
                  Log.assertThat(fnaHomePage.gridSearchWithPhoneManageUser(), "Grid search with Phone work of manage User Page Successful with valid credential", "Grid search with Phone work of manage User Page unsuccessful with valid credential", driver);
           }
           catch(Exception e)
           {
           	AnaylizeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
           }
           finally
           {
           	Log.endTestCase();
           	driver.quit();
           }
    }      

    /** TC_004 (Manage User): Verify grid search using user license of manage user page.
     *  Scripted By: Sekhar
      * @throws Exception
     */
     @Test(priority = 1, enabled = true, description = "Verify grid search using user license of manage user page")
     public void verifyGridSearchUserLicenseManageUserPage() throws Exception
     {
    	 try
    	 {
    		 Log.testCaseInfo("TC_004 (Manage User): Verify grid search using user license of manage user page");
    		 loginPage = new LoginPage(driver).get();                
    		 String uName = PropertyReader.getProperty("ManageUsername");
    		 String pWord = PropertyReader.getProperty("Password");			
    		 fnaHomePage=loginPage.loginWithValidCredential(uName,pWord); 
    		 fnaHomePage.loginValidation();
    		 Log.assertThat(fnaHomePage.gridSearch_UserLicense_LiteUser_ManageUser(), "Grid search using user license with lite user of manage User Page Successful with valid credential", "Grid search using user license with lite user of manage User Page unsuccessful with valid credential", driver);
    		 Log.assertThat(fnaHomePage.gridSearch_UserLicense_SharedUser_ManageUser(), "Grid search using user license with shared user of manage User Page Successful with valid credential", "Grid search using user license with shared user of manage User Page unsuccessful with valid credential", driver);
    		 Log.assertThat(fnaHomePage.gridSearch_UserLicense_Employee_ManageUser(), "Grid search using user license with employeee of manage User Page Successful with valid credential", "Grid search using user license with employee of manage User Page unsuccessful with valid credential", driver);
    	 }
            catch(Exception e)
            {
           	 AnaylizeLog.analyzeLog(driver);
                   e.getCause();
                   Log.exception(e, driver);
            }
            finally
            {
                   Log.endTestCase();
                   driver.quit();
            }
     }
     
     /** TC_005 (Manage User): Verify search reset button of manage user page.
      *  Scripted By: Sekhar
       * @throws Exception
      */
      @Test(priority = 2, enabled = true, description = "TC_005 (Manage User): Verify search reset button of manage user page")
      public void verifyResetInGridSearch() throws Exception
      {
             try
             {
                   Log.testCaseInfo("TC_005 (Manage User): Verify search reset button of manage user page");
                   loginPage = new LoginPage(driver).get();                
                   String uName = PropertyReader.getProperty("ManageUsername");
        			String pWord = PropertyReader.getProperty("Password");			
        			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord); 
        			fnaHomePage.loginValidation();
        			Log.assertThat(fnaHomePage.search_Reset_ManageUser(), "Search reset button of manage User Page Successful with valid credential", "Search reset button of manage User Page unsuccessful with valid credential", driver);
             }
             catch(Exception e)
             {
           	  AnaylizeLog.analyzeLog(driver);
                    e.getCause();
                    Log.exception(e, driver);
             }
             finally
             {
                    Log.endTestCase();
                    driver.quit();
             }
      }
      
      /** TC_001 (Advanced Search): Verify on searching the content of the file with characters the file show up in search result.  
 	  *  Scripted By: Sekhar
 	 * @throws Exception
 	 */
 	@Test(priority = 3, enabled = true, description = "TC_001 (Advanced Search): Verify on searching the content of the file with characters the file show up in search result.")
 	public void verifyContent_Search() throws Exception
 	{
 		try
 		{			
 			Log.testCaseInfo("TC_001 (Advanced Search): Verify on searching the content of the file with characters the file show up in search result");
 			loginPage = new LoginPage(driver).get();		
 			String uName = PropertyReader.getProperty("CollectionAccountteam");
 			String pWord = PropertyReader.getProperty("Password");			
 			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
 			fnaHomePage.loginValidation();	
 			String Collection_Name = PropertyReader.getProperty("SelectCollectionrename11");			
 			fnaHomePage.selectcollection(Collection_Name);	
 			String FolderName = PropertyReader.getProperty("SelectFoldername12");			
 			fnaHomePage.Select_Folder(FolderName);	
 			driver.switchTo().defaultContent();
 			advancedSearchPage = new AdvancedSearchPage(driver).get();
 			String ContentName = PropertyReader.getProperty("Searchkeyword");	
 			Log.assertThat(advancedSearchPage.content_search(ContentName), "Content Search Successfull", "Content Search UnSuccessful", driver);
 		}
 		catch(Exception e)
 		{
 			AnaylizeLog.analyzeLog(driver);
 			e.getCause();
 			Log.exception(e, driver);
 		}
 		finally
 		{
 			Log.endTestCase();
 			driver.quit();
 		}
 	}	
 	
 	/** TC_002 (Advanced Search): Verify Edit the attributes all selected assign the attributes then preview save search.  
 	  *  Scripted By: Sekhar
 	 * @throws Exception
 	 */
 	@Test(priority = 4, enabled = true, description = "TC_002 (Advanced Search): Verify Edit the attributes all selected assign the attributes then preview save search.")
 	public void verifyEditAttributes_Preview() throws Exception
 	{
 		try
 		{			
 			Log.testCaseInfo("TC_002 (Advanced Search): Verify Edit the attributes all selected assign the attributes then preview save search.");
 			loginPage = new LoginPage(driver).get();		
 			String uName = PropertyReader.getProperty("CollectionAccountteam");
 			String pWord = PropertyReader.getProperty("Password");			
 			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
 			fnaHomePage.loginValidation();	
 			String CollectionName= fnaHomePage.Random_Collectionname();								
 			fnaHomePage.Create_Collections(CollectionName);	
 			String FolderName= fnaHomePage.Random_Foldername();		
 			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfully", "Adding folder UnSuccessfully", driver);
 			File Path=new File(PropertyReader.getProperty("MultipleFolderPath"));			
 			String FolderPath = Path.getAbsolutePath().toString();
 			Log.assertThat(fnaHomePage.Upload_MultipleFiles(FolderPath), "Multiple files uploaded Successfully", "Multiple files uploaded UnSuccessfully", driver);
         	driver.switchTo().defaultContent();
 			advancedSearchPage = new AdvancedSearchPage(driver).get();
 			String Documenttype_Name = PropertyReader.getProperty("Documenttype11");
 			Log.assertThat(advancedSearchPage.EditAttributes_Preview(Documenttype_Name), "Preview edit attribute files Successfully", "Preview edit attribute files UnSuccessfully", driver);
 		}
 		catch(Exception e)
 		{
 			AnaylizeLog.analyzeLog(driver);
 			e.getCause();
 			Log.exception(e, driver);
 		}
 		finally
 		{
 			Log.endTestCase();
 			driver.quit();
 		}
 	}		
 	
 	/** TC_003 (Advanced Search): Verify Search in the modify attributes section then assign the values.  
 	  *  Scripted By: Sekhar
 	 * @throws Exception
 	 */
 	@Test(priority = 5, enabled = true, description = "TC_003 (Advanced Search): Verify Search in the modify attributes section then assign the values.")
 	public void verifyModufyAttributes_Search() throws Exception
 	{
 		try
 		{			
 			Log.testCaseInfo("TC_003 (Advanced Search): Verify Search in the modify attributes section then assign the values.");
 			loginPage = new LoginPage(driver).get();		
 			String uName = PropertyReader.getProperty("CollectionAccountteam");
 			String pWord = PropertyReader.getProperty("Password");			
 			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
 			fnaHomePage.loginValidation();	
 			String CollectionName= fnaHomePage.Random_Collectionname();								
 			fnaHomePage.Create_Collections(CollectionName);	
 			String FolderName= fnaHomePage.Random_Foldername();		
 			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfully", "Adding folder UnSuccessfully", driver);
 			File Path=new File(PropertyReader.getProperty("MultipleFolderPath"));			
 			String FolderPath = Path.getAbsolutePath().toString();
 			Log.assertThat(fnaHomePage.Upload_MultipleFiles(FolderPath), "Multiple files uploaded Successfully", "Multiple files uploaded UnSuccessfully", driver);
 			driver.switchTo().defaultContent();
 			advancedSearchPage = new AdvancedSearchPage(driver).get();
 			String Documenttype_Name = PropertyReader.getProperty("Documenttype11");
 			Log.assertThat(advancedSearchPage.ModifyAttributes_Search(Documenttype_Name), "Modify Attributes Search Successfully", "Modify Attributes Search UnSuccessfully", driver);
 		}
 		catch(Exception e)
 		{
 			AnaylizeLog.analyzeLog(driver);
 			e.getCause();
 			Log.exception(e, driver);
 		}
 		finally
 		{
 			Log.endTestCase();
 			driver.quit();
 		}
 	}	
 	
 	/** TC_004 (Advanced Search): Verify root folder select then search sub folder results should populate.  
 	  *  Scripted By: Sekhar
 	 * @throws Exception
 	 */
 	@Test(priority = 6, enabled = true, description = "TC_004 (Advanced Search): Verify root folder select then search subfolder results should populate.")
 	public void verifySubfolder_searchresults() throws Exception
 	{
 		try
 		{			
 			Log.testCaseInfo("TC_004 (Advanced Search): Verify root folder select then search subfolder results should populate.");
 			loginPage = new LoginPage(driver).get();		
 			String uName = PropertyReader.getProperty("CollectionAccountteam");
 			String pWord = PropertyReader.getProperty("Password");			
 			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
 			fnaHomePage.loginValidation();	
 			String CollectionName= fnaHomePage.Random_Collectionname();								
 			fnaHomePage.Create_Collections(CollectionName);	
 			String FolderName= fnaHomePage.Random_Foldername();		
 			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfully", "Adding folder UnSuccessfully", driver);
 			String SubFolderName= fnaHomePage.Random_SubFoldername();	
 			Log.assertThat(fnaHomePage.Adding_SubFolder(SubFolderName), "Adding Subfolder Successfull", "Adding Subfolder UnSuccessful", driver);
 			File Path=new File(PropertyReader.getProperty("MultipleFolderPath"));			
 			String FolderPath = Path.getAbsolutePath().toString();
 			Log.assertThat(fnaHomePage.Upload_MultipleFiles(FolderPath), "Multiple files uploaded Successfully", "Multiple files uploaded UnSuccessfully", driver);
 			
 			driver.switchTo().defaultContent();
 			advancedSearchPage = new AdvancedSearchPage(driver).get();			
 			String Documenttype_Name = PropertyReader.getProperty("Documenttype12");			
 			Log.assertThat(advancedSearchPage.Subfolder_searchresults(SubFolderName,Documenttype_Name), "Select root folder search Successfully", "Select root folder search UnSuccessfully", driver);
 		}
 		catch(Exception e)
 		{
 			AnaylizeLog.analyzeLog(driver);
 			e.getCause();
 			Log.exception(e, driver);
 		}
 		finally
 		{
 			Log.endTestCase();
 			driver.quit();
 		}
 	}
	
	/** TC_001 (GlobalSearch): Verify user Global search with collection.
	 *  Scripted By: Sekhar 		 
	 * @throws Exception
	 */
	@Test(priority = 7, enabled = true, description = "TC_001 (GlobalSearch): Verify user Global search with collection.")
	public void verifyCollection_GlobalSearch() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_001 (GlobalSearch): Verify user Global search with collection.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);
			driver.switchTo().defaultContent();
			globalSearchPage = new GlobalSearchPage(driver).get();			
			Log.assertThat(globalSearchPage.Collection_GlobalSearch(CollectionName), "Global Search with collection successfull with valid credential", "Global Search with collection unsuccessfull with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}	
	
	/** TC_002 (GlobalSearch): Verify user Global search with collection after collection info.
	 *  Scripted By: Sekhar 		 
	 * @throws Exception
	 */
	
	@Test(priority = 8, enabled = true, description = "TC_002 (GlobalSearch): Verify user Global search with collection after collection info.")
	public void verifyGlobalSearch_Collection_Info() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_002 (GlobalSearch): Verify user Global search with collection after collection info.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			driver.switchTo().defaultContent();
			globalSearchPage = new GlobalSearchPage(driver).get();
			String CollectionName= globalSearchPage.Random_Collectionname();
			String Descrip = PropertyReader.getProperty("CollectionDesc");
			String Address = PropertyReader.getProperty("CollectionAddress");
			String City = PropertyReader.getProperty("Collectioncity");
			String Zip = PropertyReader.getProperty("CollectionZip");
			String Country = PropertyReader.getProperty("CollectionCountry");
			String State = PropertyReader.getProperty("CollectionState");
			globalSearchPage.Create_Collections_Search(CollectionName,Descrip,Address,City,Zip,Country,State);	
			Log.assertThat(globalSearchPage.Collection_GlobalSearch(CollectionName), "Global Search with collection successfull with valid credential", "Global Search with collection unsuccessfull with valid credential", driver);
			Log.assertThat(globalSearchPage.GlobalSearch_Collection_Info(CollectionName,Address,City,Zip,Country,State), "Global Search with collection info successfull with valid credential", "Global Search with collection info unsuccessfull with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_003 (GlobalSearch): Verify user Global search with file after four buttons display or not.
	 *  Scripted By: Sekhar 		 
	 * @throws Exception
	 */
	@Test(priority = 9, enabled = true, description = "TC_003 (GlobalSearch): Verify user Global search with file after four buttons display or not.")
	public void verifyFile_GlobalSearch_FourButtons() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_003 (GlobalSearch): Verify user Global search with file after four buttons display or not.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);			
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			File Path=new File(PropertyReader.getProperty("GlobalFilePath"));			
			String FolderPath = Path.getAbsolutePath().toString();			
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			driver.switchTo().defaultContent();
			globalSearchPage = new GlobalSearchPage(driver).get();			
			Log.assertThat(globalSearchPage.File_GlobalSearch_Fourbuttons(CollectionName), "Global Search with file name after 4 buttons displayed", "Global Search with file name after 4 buttons not displayed ", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}

	/** TC_004 (GlobalSearch): Verify user Edit collection after Global search with collection name.
	 *  Scripted By: Sekhar 		 
	 * @throws Exception
	 */
	
	@Test(priority = 10, enabled = true, description = "TC_004 (GlobalSearch): Verify user Edit collection after Global search with collection name.")
	public void verifyEdit_Collection_GlobalSearch() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_004 (GlobalSearch): Verify user Edit collection after Global search with collection name.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			driver.switchTo().defaultContent();
			globalSearchPage = new GlobalSearchPage(driver).get();
			String CollectionName= globalSearchPage.Random_Collectionname();
			String Descrip = PropertyReader.getProperty("CollectionDesc");
			String Address = PropertyReader.getProperty("CollectionAddress");
			String City = PropertyReader.getProperty("Collectioncity");
			String Zip = PropertyReader.getProperty("CollectionZip");
			String Country = PropertyReader.getProperty("CollectionCountry");
			String State = PropertyReader.getProperty("CollectionState");
			globalSearchPage.Create_Collections_Search(CollectionName,Descrip,Address,City,Zip,Country,State);	
			String Edit_CollectionName= globalSearchPage.Random_EditCollectionname();
			Log.assertThat(globalSearchPage.Edit_Collection(CollectionName,Edit_CollectionName), "Edit collection successfull with valid credential", "Edit collection unsuccessfull with valid credential", driver);
			Log.assertThat(globalSearchPage.Collection_GlobalSearch(Edit_CollectionName), "Global Search with Edit collection successfull with valid credential", "Global Search with Edit collection unsuccessfull with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_005 (GlobalSearch): Verify New Registration user default Custom Attributes showing or not under global search.
	 *  Scripted By: Sekhar 
	 * @throws Exception
	 */
	@Test(priority = 11, enabled = false, description = "TC_005 (GlobalSearch): Verify New Registration user default Custom Attributes showing or not under global search.")
	public void verifyNewRegistration_CustomAttributes_Globalsearch() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_005 (GlobalSearch): Verify New Registration user default Custom Attributes showing or not under global search.");
			globalSearchPage = new GlobalSearchPage(driver).get();
			String FristName= globalSearchPage.Random_TestName();			
			String State = PropertyReader.getProperty("CollectionState11");
			String Phonenum = PropertyReader.getProperty("CollectionPhonenum111");	
			String password = PropertyReader.getProperty("Password");	
			Log.assertThat(globalSearchPage.NewRegistration_CustomAttributes_GlobalSearch(FristName,State,Phonenum,password),"Default Custom Attributes in Global search successfull", "Default Custom Attributes in Global search Unsuccessfull", driver);	
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		} 
	}
	
	/** TC_001 (Module Search): Verify user Module search in communications level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 12, enabled = true, description = "TC_001 (Module Search): Verify user Module search in communications level.")
	public void verifyModulesearch_Communications() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_001 (Module Search): Verify user Module search in communications level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			String Collection_Name = PropertyReader.getProperty("FNASelectCollection24");			
			fnaHomePage.selectcollection(Collection_Name);	
			driver.switchTo().defaultContent();
			communicationPage = new CommunicationPage(driver).get();
			String SubjectName= communicationPage.Random_Subject();
			String ContactName = PropertyReader.getProperty("FNATeamFilename");	
			String Email = PropertyReader.getProperty("Coll_Favouriteuser");	
			Log.assertThat(communicationPage.Create_communication(ContactName,Email,SubjectName), "Create Communication Successful with valid credential", "Create Communication unsuccessful with valid credential", driver);
			driver.switchTo().defaultContent();
			moduleSearchPage = new ModuleSearchPage(driver).get();						
			Log.assertThat(moduleSearchPage.Module_Search_CommunicationsLevel(SubjectName), "Module search in Communications level Successfull", "Module search in Communications level UnSuccessfull", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_002 (Module Search): Verify user Edit file and module search in file level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 13, enabled = true, description = "TC_002 (Module Search): Verify user Edit file and module search in file level.")
	public void verifyEditFile_ModuleSearch_Files() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_002 (Module Search): Verify user Edit file and module search in file level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			String Collection_Name = PropertyReader.getProperty("FNASelectCollection24");			
			fnaHomePage.selectcollection(Collection_Name);
			String FolderName = PropertyReader.getProperty("SelFolder");			
			fnaHomePage.Select_Folder(FolderName);	
			driver.switchTo().defaultContent();
			moduleSearchPage = new ModuleSearchPage(driver).get();	
			String ReName= moduleSearchPage.Random_ReName();
			String Title= moduleSearchPage.Random_Title();
			Log.assertThat(moduleSearchPage.Edit_File_CollectionFolder(ReName,Title), "Edit File in file level Successfull", "Edit File in file level UnSuccessfull", driver);
			Log.assertThat(moduleSearchPage.Module_Search_FileLevel(ReName), "File module search in file level Successfull", "File module search in file level UnSuccessfull", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_003 (Module Search): Verify user Module search in Photos level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 14, enabled = true, description = "TC_003 (Module Search): Verify user Module search in Photos level.")
	public void verifyModulesearch_Photos() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_003 (Module Search): Verify user Module search in Photos level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			String Collection_Name = PropertyReader.getProperty("FNASelectCollection24");			
			fnaHomePage.selectcollection(Collection_Name);	
			driver.switchTo().defaultContent();
			moduleSearchPage = new ModuleSearchPage(driver).get();
			String Title_Name= moduleSearchPage.Random_Title();
			String Description= moduleSearchPage.Random_Description();
			Log.assertThat(moduleSearchPage.selectCollectionAlbum(Collection_Name,Title_Name,Description), "Selected Collection Album and Edit Album Successfull", "Selected Collection Album and Edit Album UnSuccessfull", driver);
			Log.assertThat(moduleSearchPage.Module_Search_Photo(Title_Name,Description), "Module search in photos Successfull", "Module search in photos UnSuccessfull", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_004 (Module Search): Verify user Grid search in File level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 15, enabled = true, description = "TC_004 (Module Search): Verify user Grid search in File level.")
	public void verifyGrid_Search_Filelevel() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_004 (Module Search): Verify user Grid search in File level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			String Collection_Name = PropertyReader.getProperty("FNASelectCollection24");			
			fnaHomePage.selectcollection(Collection_Name);
			String FolderName = PropertyReader.getProperty("FNASelectFolder12");			
			fnaHomePage.Select_Folder(FolderName);	
			driver.switchTo().defaultContent();
			moduleSearchPage = new ModuleSearchPage(driver).get();	
			String FileName = PropertyReader.getProperty("Filename111");			
			Log.assertThat(moduleSearchPage.Grid_Search_FileLevel(FileName), "Grid search in file level Successfull", "Grid search in file level UnSuccessfull", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	/** TC_004 (Message board): Verify Search with subject in message board.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 16, enabled = true, description = "TC_004 (Message board): Verify Search with subject in message board.")
	public void verifySearch_withSubject_Messageboard() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_004 (Message board): Verify Search with subject in message board.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String Collection_Name = PropertyReader.getProperty("FNASelectCollection22");			
			fnaHomePage.selectcollection(Collection_Name);
			driver.switchTo().defaultContent();
			messageboardPage = new MessageboardPage(driver).get();
			String SearchSubject = PropertyReader.getProperty("SearchwithSubject");				
			Log.assertThat(messageboardPage.Search_WithSubject_Messageboard(SearchSubject), "Search with Subject in message board Successfull", "Search with Subject in message board UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_006 (Start Up Page): Verify Search with folder name and file name. 
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 17, enabled = true, description = "TC_006 (Start Up Page): Verify Search with folder name and file name.")
	public void verifySearchwithfolderfile() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_006 (Start Up Page): Verify Search with folder name and file name.");
			loginPage = new LoginPage(driver).get();			
			String uName = PropertyReader.getProperty("StartupUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			fnaHomePage.selectcollection_Edit();
			fnaHomePage.start_Up_Page_Default_View();			
			Log.assertThat(fnaHomePage.Search_withfolderandfile(), "Search with folder name and file name Successful with valid credential", "Search with folder name and file name unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_002 (Start Up Page): Verify start file and the desired file for search.
	 *  Scripted By: Sekhar 
	 * @throws Exception
	 */
	@Test(priority = 18, enabled = true, description = "TC_002 (Start Up Page): Verify start file and the desired file for search")
	public void verifydesiredfile() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_002 (Start Up Page): Verify start file and the desired file for search");
			loginPage = new LoginPage(driver).get();			
			String uName = PropertyReader.getProperty("StartupUserName");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			fnaHomePage.selectcollection_Edit();
			fnaHomePage.start_Up_Page_Default_View();			
			Log.assertThat(fnaHomePage.start_Up_Page_desiredfile_select(), "start file and the desired file for search Successful with valid credential", "start file and the desired file for search unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
}