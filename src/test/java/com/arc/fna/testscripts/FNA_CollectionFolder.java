package com.arc.fna.testscripts;


import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;


@Listeners(EmailReport.class)

public class FNA_CollectionFolder {
	
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	
	
	
	@Parameters("browser")
	@BeforeMethod(groups="sekhar_test")
	public WebDriver beforeTest(String browser)
	{
		
		if(browser.equalsIgnoreCase("firefox"))
		{
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		else if (browser.equalsIgnoreCase("chrome"))
		{ 
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			driver = new ChromeDriver( options );
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	 
		} 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
	  return driver;
	}
	
	/** TC_008 (Collection Folder): Verify Create collection Folder and selected upload folder.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = true, description = "TC_008 (Collection Folder): Verify create collection Folder and selected upload folder.")
	public void verifyCreateCollection_SelectedUploadFolder () throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_008 (Collection Folder): Verify create collection Folder and selected upload folder.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			Log.assertThat(fnaHomePage.Adding_Select_Folder(), "Adding folder and selected Upload Folder in Folder level Successful", "Adding folder and selected Upload Folder in Folder level UnSuccessful", driver);
		
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_009 (Collection Folder): Verify Adding Sub folder and Expand Structure
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 1, enabled = true, description = "TC_009 (Collection Folder): Verify Adding Subfolder and Expand Structure")
	public void verifyExpandFolderstructure() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_009 (Collection Folder): Verify Adding Subfolder and Expand Structure");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Username");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String Collection_Name = PropertyReader.getProperty("ColleName");
			fnaHomePage.selectcollection(Collection_Name);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			String SubFolderName= fnaHomePage.Random_SubFoldername();	
			Log.assertThat(fnaHomePage.Adding_SubFolder_Expand_Structure(SubFolderName), "Adding Subfolder and Expand Stricture Successfull with valid credential", "Adding Subfolder and Expand Stricture UnSuccessful with valid credential", driver);
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_010 (Collection Folder): Verify revision upload file Count.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 2, enabled = true, description = "TC_010 (Collection Folder): Verify revision upload file Count.")
	public void verifyrevisionuploadfile() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_010 (Collection Folder): Verify revision upload file Count.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Username");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String Collection_Name = PropertyReader.getProperty("ColleName");
			fnaHomePage.selectcollection(Collection_Name);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			Log.assertThat(fnaHomePage.UploadFile_NewCopy(FolderPath), "Upload file with new copy Successfully", "Upload file with new copy UnSuccessfully", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_011 (Collection Folder): Verify  bread crumb available in the files.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 3, enabled = true, description = "TC_011 (Collection Folder): Verify bread crumb available in the files.")
	public void verifybreadcrumbavailablefiles() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_011 (Collection Folder): Verify bread crumb available in the files.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Username");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String Collection_Name = PropertyReader.getProperty("CollecBreadname");
			fnaHomePage.selectcollection(Collection_Name);	
			Log.assertThat(fnaHomePage.Bread_crumb_File(), "Bread Successfull", "Bread UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_012 (Collection Folder): Verify move folder from parent folder then download.
	  *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 4, enabled = true, description = "TC_012 (Collection Folder): Verify move folder from parent folder then download.")
	public void verify_Move_Folder() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_012 (Collection Folder): Verify move folder from parent folder then download.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			
			
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			String FolderName1= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder_Project(FolderName1), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			Log.assertThat(fnaHomePage.Move_Folder(FolderName), "Move folder Successfull", "Move folder UnSuccessful", driver);
            String usernamedir=System.getProperty("user.name");
            String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";
			
			//String DownloadPath = PropertyReader.getProperty("SysDownloadpath");
			Log.assertThat(fnaHomePage.Download_Folder(DownloadPath,FolderName1), " Download folder Successfull", "Download folder UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_013 (Collection Folder): Verify move folder from parent folder then link folder download
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 5, enabled = true, description = "TC_013 (Collection Folder): Verify move folder from parent folder then link folder download")
	public void verify_Move_Folder_LinkFolder() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_013 (Collection Folder): Verify move folder from parent folder then link folder download");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			String FolderName1= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder_Project(FolderName1), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			Log.assertThat(fnaHomePage.Move_Folder(FolderName), "Move folder Successfull", "Move folder UnSuccessful", driver);
            String usernamedir=System.getProperty("user.name");
            String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";        
			
			//String DownloadPath = PropertyReader.getProperty("SysDownloadpath");
			Log.assertThat(fnaHomePage.SendLink_Folder_Download(DownloadPath,FolderName), "Send link folder and Download Successfull", "Send link folder and Download UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/*
	*//** TC_014 (Collection Folder): Verify Expand Sub folder 30thlevel then move folder to parent root folder
	 *  Scripted By: Sekhar
	 * @throws Exception
	 *//*
	@Test(priority = 6, enabled = true, description = "TC_014 (Collection Folder): Verify Expand Sub folder 30thlevel then move folder to parent root folder")
	public void verifyExpandFolder30thlevel() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_014 (Collection Folder): Verify Expand Sub folder 30thlevel then move folder to parent root folder");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			//String SubFolderName= fnaHomePage.Random_SubFoldername();
			for(int i=1;i<=30;i++)
			{
				String SubFolderName= fnaHomePage.Random_SubFoldername();	
				fnaHomePage.Adding_SubFolder_Expand30thlevel(SubFolderName); 
			}			
			
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			Log.assertThat(fnaHomePage.Move_SubFolder30thlevel(), "30thlevel sub folder then move folder to parent root folder Successfull", "30thlevel sub folder then move folder to parent root folder UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	*/
	
	/** TC_015 (Collection Folder): Verify rename folder then Download folder.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 7, enabled = true, description = "TC_015 (Collection Folder): Verify rename folder then Download folder.")
	public void verify_Rename_Folder_Download() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_015 (Collection Folder): Verify rename folder then Download folder.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
            String usernamedir=System.getProperty("user.name");
            String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";         
			
			//String DownloadPath = PropertyReader.getProperty("SysDownloadpath");
			Log.assertThat(fnaHomePage.Download_Folder(DownloadPath,FolderName), "Download folder Successfull", "Download folder UnSuccessful", driver);
			String FolderName1= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Rename_Folder(FolderName1), "Adding rename folder Successfull", "Adding rename folder UnSuccessful", driver);
			Log.assertThat(fnaHomePage.Download_Folder(DownloadPath,FolderName1), "Download rename folder Successfull", "Download rename folder UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}	
	
	/** TC_016 (Collection Folder): Verify select multiple files then send link files and download.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 8, enabled = true, description = "TC_016 (Collection Folder): Verify select multiple files then send link files and download.")
	public void verifyMultipleFiles_Sendlinkfiles_Download() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_016 (Collection Folder): Verify select multiple files then send link files and download.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			
			//String FolderPath = PropertyReader.getProperty("MultipleFolderPath");
			
			File Path=new File(PropertyReader.getProperty("MultipleFolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			
			Log.assertThat(fnaHomePage.Upload_MultipleFiles(FolderPath), "Multiple files uploaded Successfully", "Multiple files uploaded UnSuccessfully", driver);
            String usernamedir=System.getProperty("user.name");
            String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";        
			
			//String DownloadPath = PropertyReader.getProperty("SysDownloadpath");
			Log.assertThat(fnaHomePage.Multiplefiles_sendlink_download(DownloadPath), "Download folder Successfully", "Download folder UnSuccessfully", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
			
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_017 (Collection Folder): Verify select multiple Photos then send link files and download.
	  *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 9, enabled = true, description = "TC_017 (Collection Folder): Verify select multiple Photos then send link files and download.")
	public void verifyMultiplePhotos_Sendlinkfiles_Download() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_017 (Collection Folder): Verify select multiple Photos then send link files and download");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String AlbumName= fnaHomePage.Random_Albumname();	
			Log.assertThat(fnaHomePage.Adding_Album(AlbumName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			
			//String PhotoPath = PropertyReader.getProperty("MultiplePhotosPath");
			File Path=new File(PropertyReader.getProperty("MultiplePhotosPath"));			
			String PhotoPath = Path.getAbsolutePath().toString();
			
			Log.assertThat(fnaHomePage.Upload_MultiplePhotos(PhotoPath), "Multiple photos uploaded Successfully", "Multiple photos uploaded UnSuccessfully", driver);
			
            String usernamedir=System.getProperty("user.name");
            String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";
         
			//String DownloadPath = PropertyReader.getProperty("SysDownloadpath");
			Log.assertThat(fnaHomePage.Multiplephoto_share_download(DownloadPath), "Download photo Successfully", "Download photo UnSuccessfully", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_018 (Collection Folder): Verify Auto update send link Download.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 10, enabled = true, description = "TC_018 (Collection Folder): Verify Auto update send link Download.")
	public void verifyautoupdate_Sendlink_download() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_018 (Collection Folder): Verify Auto update send link Download.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
            String usernamedir=System.getProperty("user.name");
            String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";      
			
			//String DownloadPath = PropertyReader.getProperty("SysDownloadpath");
			//String FilePath = PropertyReader.getProperty("FilePath");
            
            File Path1=new File(PropertyReader.getProperty("FilePath11"));			
			String FilePath = Path1.getAbsolutePath().toString();
			
			
			Log.assertThat(fnaHomePage.Autoupdate_sendlink_download(DownloadPath,FilePath), "Autoupdate send link Download Successfull", "Autoupdate send link Download UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_019 (Collection Folder): Verify Enable viewer with send link and file revision.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 11, enabled = true, description = "TC_019 (Collection Folder): Verify Enable viewer with send link and file revision.")
	public void verifyEnableviewer_Sendlink() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_019 (Collection Folder): Verify Enable viewer with send link and file revision.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			
			//String FilePath = PropertyReader.getProperty("FilePath");
			File Path1=new File(PropertyReader.getProperty("FilePath11"));			
			String FilePath = Path1.getAbsolutePath().toString();
			
			Log.assertThat(fnaHomePage.EnableViewer_sendlink_revision(FilePath), "Enable Viewer with send link  Successfull", "Enable Viewer with send link UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_020 (Collection Folder): Verify set alert two files then delete two files in message board.
	 *  Scripted By: Sekhar 
	 * @throws Exception
	 */
	@Test(priority = 12, enabled = true, description = "TC_020 (Collection Folder): Verify set alert two files then delete two files in message board.")
	public void verifysetalert_deletefiles_messageboard() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_020 (Collection Folder): Verify set alert two files then delete two files in message board.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			
			//String FolderPath = PropertyReader.getProperty("SetAlertMultipleFolderPath");
			File Path=new File(PropertyReader.getProperty("SetAlertMultipleFolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			Log.assertThat(fnaHomePage.Upload_MultipleFiles(FolderPath), "Upload Multiplefiles Successfull", "Upload Multiplefiles UnSuccessful", driver);
			Log.assertThat(fnaHomePage.SetAlert_DeleteFiles_MessageBoard(), "Set Alert for Two Files then delete files Successfull", "Set Alert for Two Files then delete files UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}		
	
	/** TC_021 (Collection Folder): Verify shared link files using enable viewer after delete file then check viewer or not.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 13, enabled = true, description = "TC_021 (Collection Folder): Verify shared link files using enable viewer after delete file then check viewer or not.")
	public void verifyLinkfiles_Deletefile_RestoreFile_Enableviewer() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_021 (Collection Folder): Verify shared link files using enable viewer after delete file then check viewer or not.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			Log.assertThat(fnaHomePage.EnableViewer_sendlinkfile_Deletefile_RestoreFile(CollectionName), "Enable Viewer send linkfile then delete file and check link viewer Successfull", "Enable Viewer send linkfile then delete file and check link viewer UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_022 (Collection Folder): Verify shared link folder then download after delete folder then check Downloading or not.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 14, enabled = true, description = "TC_022 (Collection Folder): Verify shared link folder then download after delete folder then check Downloading or not.")
	public void verifyLinkfolder_Deletefolder_RestoreFolder_Download() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_022 (Collection Folder): Verify shared link folder then download after delete folder then check Downloading or not.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
            String usernamedir=System.getProperty("user.name");
            String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";        
			
			//String DownloadPath = PropertyReader.getProperty("SysDownloadpath");
			Log.assertThat(fnaHomePage.Download_sendlinkfolder_Deletefolder_RestoreFolder(CollectionName,DownloadPath,FolderName), "Download send linkfolder then delete Folder and check link Download Successfull", "Download send linkfolder then delete Folder and check link Download UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}

	/** TC_023 (Collection Folder): Verify Checkout any file now checkin with new version file and comment.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 15, enabled = true, description = "TC_023 (Collection Folder): Verify Checkout any file now checkin with new version file and comment.")
	public void verifyCheckout_Checkin() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_023 (Collection Folder): Verify Checkout any file now checkin with new version file and comment.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			String RevisionName = PropertyReader.getProperty("Revisionname");
			String Comment = PropertyReader.getProperty("Comment");			
			//String FilePath = PropertyReader.getProperty("FolderPath");			
			File Path1=new File(PropertyReader.getProperty("FolderPath"));			
			String FilePath = Path1.getAbsolutePath().toString();			
			Log.assertThat(fnaHomePage.Checkout_Ckeckin_File(RevisionName,Comment,FilePath), "Check out files and Check in Successfull", "Check out files and Check in UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}

	/** TC_024 (Collection Folder): Verify warning message  move files using check out file.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 16, enabled = true, description = "TC_024 (Collection Folder): Verify warning message  move files using check out file.")
	public void verifyWarningmessage_Movefile_Checkout() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_024 (Collection Folder): Verify warning message  move files using check out file.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			Log.assertThat(fnaHomePage.Checkout_movefile(), "Nagative scenario for Move file Successfull", "Nagative scenario for Move file UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	/** TC_025 (Collection Folder): Verify shared link file with checkout and check in using enable viewer.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 17, enabled = true, description = "TC_025 (Collection Folder): Verify shared link file with checkout and checkin using enable viewer.")
	public void verifysharedlinkfile_checkout_checkin_enableviewer() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_025 (Collection Folder): Verify shared link file with checkout and checkin using enable viewer.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			
			//String FolderPath = PropertyReader.getProperty("FolderPath1");
			File Path=new File(PropertyReader.getProperty("FolderPath1"));			
			String FolderPath = Path.getAbsolutePath().toString();
			
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			String RevisionName = PropertyReader.getProperty("Revisionname");
			String Comment = PropertyReader.getProperty("Comment");
			
			//String FilePath = PropertyReader.getProperty("FolderPath1");
			File Path1=new File(PropertyReader.getProperty("FolderPath1"));			
			String FilePath = Path1.getAbsolutePath().toString();
			
			Log.assertThat(fnaHomePage.sharedlinkfile_Checkout_checkin_enableviewer(CollectionName,RevisionName,Comment,FilePath), "shared linkfiles checkout and checkin enable viewer Successfull", "shared linkfiles checkout and checkin enable viewer UnSuccessful", driver);
		
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_026 (Collection Folder): Verify Remove sub folder with file and restore the sub folder.
	  *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 18, enabled = false, description = "TC_026 (Collection Folder): Remove sub folder with file and restore the sub folder.")
	public void verifyRemove_subfolderwithfile_Restote() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_026 (Collection Folder): Verify Remove sub folder with file and restore the sub folder.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			String SubFolderName= fnaHomePage.Random_SubFoldername();	
			Log.assertThat(fnaHomePage.Adding_SubFolder(SubFolderName), "Adding Subfolder Successfull", "Adding Subfolder UnSuccessful", driver);
			
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			Log.assertThat(fnaHomePage.Remove_SubFolderwithfile_Restore(), "Remove sudfolder with file and restore subfolder Successfull", "Remove sudfolder with file and restore subfolder UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_027 (Collection Folder): Verify ViewFile in File level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 19, enabled = true, description = "TC_027 (Collection Folder): ViewFile in File level.")
	public void verifyViewFile_Filelevel() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_027 (Collection Folder): Verify ViewFile in File level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			Log.assertThat(fnaHomePage.ViewFile_Filelevel(), "File View Successfull", "File View  UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_028 (Collection Folder): Verify Copy files within project with create new copy option.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 20, enabled = true, description = "TC_028 (Collection Folder): Copy files within project with create new copy option.")
	public void verifyCopyFiles_Within_Project() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_028 (Collection Folder): Verify Copy files within project with create new copy option.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			driver.findElement(By.xpath("(//tbody/tr[2]/td[2]/table/tbody/tr[1]/td[4]/span)[1]")).click();//click on Project
			SkySiteUtils.waitTill(5000);
			String FolderName1= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName1), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			Log.assertThat(fnaHomePage.CopyFiles_WithinProject(), "Copy file within Project Successfull", "Copy file within Project UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_029 (Collection Folder): Verify Copy files to other project with create new copy option.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 21, enabled = true, description = "TC_029 (Collection Folder): Copy files to other project with create new copy option.")
	public void verifyCopyFiles_DifferentProject() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_029 (Collection Folder): Verify Copy files to other project with create new copy option");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();				
			String CollectionName1= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName1);	
			String FolderName1= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName1), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			String Collection_Name = PropertyReader.getProperty("Collecname");			
			fnaHomePage.selectcollection(Collection_Name);	
			String FolderName = PropertyReader.getProperty("SelFolder");			
			fnaHomePage.Select_Folder(FolderName);	
			Log.assertThat(fnaHomePage.CopyFiles_different_Project(CollectionName1,FolderName1), "Copy file different project Successfull", "Copy file different project UnSuccessful", driver);
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_030 (Collection Folder): Verify Delete file from Sub folder and rename the sub folder then restore the file.
	  *  Scripted By: Sekhar 
	 * @throws Exception
	 */
	@Test(priority = 22, enabled = true, description = "TC_030 (Collection Folder): Delete file from Sub folder and rename the sub folder then restore the file.")
	public void verifyDeletefile_Subfolder_RenameSubfolder_Restorefile() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_030 (Collection Folder): Verify Delete file from Sub folder and rename the sub folder then restore the file.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			String SubFolderName= fnaHomePage.Random_SubFoldername();	
			Log.assertThat(fnaHomePage.Adding_SubFolder(SubFolderName), "Adding Subfolder Successfull", "Adding Subfolder UnSuccessful", driver);
			
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			String SubFolderName1= fnaHomePage.Random_SubFoldername();				
			Log.assertThat(fnaHomePage.Deletefile_Subfolder_RenameSubfolder_Restorefile(SubFolderName1), "Delete file from subfolder and restore file in rename the subfolder Successfull", "Delete file from subfolder and restore file in rename the subfolder UnSuccessful", driver);
			
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_031 (Collection Folder): Verify Delete file from Sub folder then Move the Sub folder and restore the file.
	  *  Scripted By: Sekhar 
	 * @throws Exception
	 */
	@Test(priority = 23, enabled = true, description = "TC_031 (Collection Folder): Verify Delete file from Sub folder then Move the Sub folder and restore the file.")
	public void verifyDeletefile_Subfolder_MoveSubfolder_Restorefile() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_031 (Collection Folder): Verify Delete file from Sub folder then Move the Sub folder and restore the file.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			SkySiteUtils.waitTill(5000);	
			driver.findElement(By.xpath("(//tbody/tr[2]/td[2]/table/tbody/tr[1]/td[4]/span)[1]")).click();//click on Project
			SkySiteUtils.waitTill(5000);
			String FolderName1= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName1), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			String SubFolderName= fnaHomePage.Random_SubFoldername();	
			Log.assertThat(fnaHomePage.Adding_SubFolder1(SubFolderName), "Adding Subfolder Successfull", "Adding Subfolder UnSuccessful", driver);
			
			File Path=new File(PropertyReader.getProperty("FolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();	
			
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			Log.assertThat(fnaHomePage.Deletefile_Subfolder(), "Delete file from subfolder Successfull", "Delete file from subfolder UnSuccessful", driver);
			Log.assertThat(fnaHomePage.Move_SubFolder(SubFolderName), "Move Subfolder Successfull", "Move Subfolder UnSuccessful", driver);
			Log.assertThat(fnaHomePage.MoveSubfolder_Restorefile(), "Move Subfolder then restore file Successfull", "Move Subfolder then restore file UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	/** TC_032 (Collection Folder): Verify Rename folder after that remove folder and Pre Deletion Validation message  
	  *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 24, enabled = true, description = "TC_032 (Collection Folder): Verify Rename folder after that remove folder and Pre Deletion Validation message.")
	public void verifyRenamefolder_Removefolder_Predeletion() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_032 (Collection Folder): Verify Rename folder after that remove folder and Pre Deletion Validation message");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);				
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			String Rename= fnaHomePage.Random_Rename();
			Log.assertThat(fnaHomePage.RenameFolder_RemoveFolder(Rename), "Rename folder and Remove Folder Successfull", "Rename folder and Remove Folder UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}	
	
}
