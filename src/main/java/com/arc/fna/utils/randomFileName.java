package com.arc.fna.utils;
import java.math.BigInteger;
import java.security.SecureRandom;


public final class randomFileName 
{

	private SecureRandom random = new SecureRandom();
	
	public String nextFileName() 
	{
		    return new BigInteger(130, random).toString(32);
	}	
	
}

