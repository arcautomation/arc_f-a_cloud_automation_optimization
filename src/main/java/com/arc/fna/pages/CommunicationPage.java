package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arc.fna.utils.randomSharedName;
import com.arcautoframe.utils.Log;

public class CommunicationPage extends LoadableComponent<CommunicationPage>
{

	WebDriver driver;
	private boolean isPageLoaded;
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	@FindBy(xpath = "//*[@id='setting']")
	WebElement btnSetting;

	@Override
	protected void load() 
	{
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnSetting, 60);
	}

	@Override
	protected void isLoaded() throws Error 
	{
		if (!isPageLoaded)
		{
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public CommunicationPage(WebDriver driver) 
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}	
	/**
	 * Method written for Random(Subject)
	 *  Scripted By: Sekhar
	 * 
	 * @return
	 */

	public String Random_Subject()
	{

		String str = "Subject";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}
	
	
	 @FindBy(css = ".icon.icon-star.ico-lg.icon-communication")
     WebElement btniconcommunication;
	 
	 @FindBy(css = "#btnNewCommunication")
     WebElement btnNewCommunication;
	
	 @FindBy(xpath = "//*[@id='txtMessageBody_container']/table/tbody/tr[2]/td")
     WebElement txtMessageBody;
	
	 @FindBy(css = "#btnToReceipients")
     WebElement btnToReceipients;
	
	 @FindBy(css = "#btnAddInvitesAndClose")
     WebElement btnAddInvitesAndClose;
	 
	 @FindBy(css = "#txtSubject")
     WebElement txtSubject;
	 
	 @FindBy(css = "#tinymce")
     WebElement txttinymce;
	 
	 @FindBy(css = "#btnSaveCommunication")
     WebElement btnSaveCommunication;
	
	 @FindBy(css = "#btnResetSearch")
     WebElement btnResetSearch;
	 
	/** 
     * Method written for Add communication
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	
    public boolean Create_communication(String Contact,String Email,String SubjectName)
    {
         boolean result1=false;
    	driver.switchTo().defaultContent();
    	btniconcommunication.click();
    	Log.message("Communication button has been clicked"); 
    	SkySiteUtils.waitTill(5000);
     	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Swich to I Frame Success!!"); 
		SkySiteUtils.waitTill(5000);
		btnNewCommunication.click();//click add 
		Log.message("Add button has been clicked"); 
		SkySiteUtils.waitForElement(driver, txtMessageBody, 120);   
		btnToReceipients.click();//click on TO button
		Log.message("ToReceipients button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();		
		int File_Count=0;
		 int i = 0;
		 List<WebElement> allElements = driver.findElements(By.xpath("//*[@id='divGridRecp']/div[2]/table/tbody/tr/td[1]/img"));//Taking check box Count
		 for (WebElement Element : allElements)
		 { 
			 File_Count = File_Count+1; 	
		 }
		 Log.message("Check box Count is: "+File_Count);
		 //Getting Folder Names and machine
		 for(i = 1;i<=File_Count;i++)
		 {					
			 String fName=driver.findElement(By.xpath("(//*[@id='divGridRecp']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
			 Log.message("Contact Name is:"+fName);
			 Log.message("Contact user Name is:"+Contact);
			 SkySiteUtils.waitTill(2000);
			 String Act_MailID=driver.findElement(By.xpath("(//*[@id='divGridRecp']/div[2]/table/tbody/tr/td[4])["+i+"]")).getText();
			 Log.message("Act Email ID is:"+Act_MailID);
			 Log.message("Exp email is:"+Email);
			 SkySiteUtils.waitTill(3000);
			 
			 if((Act_MailID.equalsIgnoreCase(Email))&&(fName.contains(Contact)))
			 {	
				 result1=true;
				 Log.message("Expected Contact Selected Successfully");	
				 driver.findElement(By.xpath("(//*[@id='divGridRecp']/div[2]/table/tbody/tr/td[1]/img)["+i+"]")).click();//select Check box
				 SkySiteUtils.waitTill(5000);
				 break;
			 }
		 }
		 if(result1==true)
		 {			
			 SkySiteUtils.waitTill(3000);			
			 String Exp_ContactName=driver.findElement(By.xpath("(//*[@id='divGridRecp']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
			 Log.message("Exp Contact Name is:"+Exp_ContactName);			
		 }
		 else
		 {
			 Log.message("Expected Contact Selected UnSuccessfully");					
		 }
		 SkySiteUtils.waitTill(3000);	 
		 btnAddInvitesAndClose.click();//click on Save button
		 Log.message("save & close button has been clicked");
		 SkySiteUtils.waitTill(3000);
		 driver.switchTo().frame(driver.findElement(By.id("myFrame")));//Switch to my frame
		 SkySiteUtils.waitTill(3000);		
		 txtSubject.sendKeys(SubjectName); 
		 Log.message("subject name has been entered");
		 SkySiteUtils.waitTill(5000);
		 driver.switchTo().frame(driver.findElement(By.id("txtMessageBody_ifr")));//Switch to message frame
		 SkySiteUtils.waitTill(3000);
		 txttinymce.sendKeys(SubjectName);
		 Log.message("subject body has been entered");
		 SkySiteUtils.waitTill(5000);
		 driver.switchTo().defaultContent();
		 driver.switchTo().frame(driver.findElement(By.id("myFrame")));//Switch to my frame
		 SkySiteUtils.waitTill(5000);
		 btnSaveCommunication.click();
		 Log.message("save & send button has been clicked");
		 SkySiteUtils.waitTill(5000);
		 btnResetSearch.click();//click on Reset button
		 SkySiteUtils.waitTill(5000);
		 String Subject=driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]")).getText();
		 Log.message("Subject name is:"+Subject);
		 SkySiteUtils.waitTill(3000);
		 String Tocontact=driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[3]")).getText();
		 Log.message("To contact name is:"+Tocontact);
		 SkySiteUtils.waitTill(3000);
		 
		 if((Subject.contentEquals(SubjectName))&&(Contact.contentEquals(Tocontact)))
			 return true;
		 else
			 return false;
    }
    
    	
    @FindBy(css = "#rgtpnlMore")
    WebElement btnrgtpnlMore;
    
    @FindBy(xpath = "//*[@id='FileActionMenu']/li[1]/a")
    WebElement btndrpattachcom;
    
    @FindBy(xpath = "//input[@name='qqfile']")
    WebElement btnselectfile;
    
    @FindBy(xpath = "//button[@id='btnFileUpload']")
    WebElement btnFileUpload;
    
    @FindBy(xpath = "//*[@id='FileActionMenu']/li[2]/a")
    WebElement btndrpattachcoll;
    
    @FindBy(xpath = "//*[@id='divFilesGrid']/div[2]/table/tbody/tr[2]/td[1]/img")
    WebElement chkdivFilesGrid;
    //(//input[@class='btn btn-primary'])[2]
    
    @FindBy(xpath = "(//input[@class='btn btn-primary'])[2]")
    WebElement btnbtnprimary;
	/** 
    * Method written for Attachment from computer and collection
    * Scripted By: Sekhar     
	 * @throws IOException 	 
    * @throws AWTException 
    */
	
   public boolean Attchments_communication(String Contact,String Email,String SubjectName,String FolderPath) throws IOException 
   {
        boolean result1=false;
        boolean result2=false;
        boolean result3=false;
        driver.switchTo().defaultContent();
    	btniconcommunication.click();
    	Log.message("Communication button has been clicked"); 
    	SkySiteUtils.waitTill(5000);
     	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Swich to I Frame Success!!"); 
		SkySiteUtils.waitTill(5000);
		btnNewCommunication.click();//click add 
		Log.message("Add button has been clicked"); 
		SkySiteUtils.waitForElement(driver, txtMessageBody, 120);   
		btnToReceipients.click();//click on TO button
		Log.message("ToReceipients button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();		
		int File_Count=0;
		 int i = 0;
		 List<WebElement> allElements = driver.findElements(By.xpath("//*[@id='divGridRecp']/div[2]/table/tbody/tr/td[1]/img"));//Taking check box Count
		 for (WebElement Element : allElements)
		 { 
			 File_Count = File_Count+1; 	
		 }
		 Log.message("Check box Count is: "+File_Count);
		 //Getting Folder Names and machine
		 for(i = 1;i<=File_Count;i++)
		 {					
			 String fName=driver.findElement(By.xpath("(//*[@id='divGridRecp']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText().toString();
			 Log.message("Contact Name is:"+fName);
			 String Act_MailID=driver.findElement(By.xpath("(//*[@id='divGridRecp']/div[2]/table/tbody/tr/td[4])["+i+"]")).getText().toString();
			 Log.message("Act Email ID is:"+Act_MailID);
			 SkySiteUtils.waitTill(3000);			 
			 if((fName.contentEquals(Contact))&&(Act_MailID.contentEquals(Email)))
			 {	
				 result1=true;
				 Log.message("Expected Contact Selected Successfully");	
				 driver.findElement(By.xpath("(//*[@id='divGridRecp']/div[2]/table/tbody/tr/td[1]/img)["+i+"]")).click();//select Check box
				 SkySiteUtils.waitTill(5000);
				 break;
			 }
		 }
		 if(result1==true)
		 {			
			 SkySiteUtils.waitTill(3000);			
			 String Exp_ContactName=driver.findElement(By.xpath("(//*[@id='divGridRecp']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
			 Log.message("Exp Contact Name is:"+Exp_ContactName);			
		 }
		 else
		 {
			 Log.message("Expected Contact Selected UnSuccessfully");					
		 }
		 SkySiteUtils.waitTill(3000);	 
		 btnAddInvitesAndClose.click();//click on Save button
		 Log.message("save & close button has been clicked");
		 SkySiteUtils.waitTill(3000);
		 driver.switchTo().frame(driver.findElement(By.id("myFrame")));//Switch to my frame
		 SkySiteUtils.waitTill(3000);		
		 txtSubject.sendKeys(SubjectName); 
		 Log.message("subject name has been entered");
		 SkySiteUtils.waitTill(5000);
		 driver.switchTo().frame(driver.findElement(By.id("txtMessageBody_ifr")));//Switch to message frame
		 SkySiteUtils.waitTill(3000);
		 txttinymce.sendKeys(SubjectName);
		 Log.message("subject body has been entered");		
		 SkySiteUtils.waitTill(5000);
		 driver.switchTo().defaultContent();
		 driver.switchTo().frame(driver.findElement(By.id("myFrame")));//Switch to my frame
		 SkySiteUtils.waitTill(5000);		 
		 btnrgtpnlMore.click();
		 Log.message("Add button has been clicked");			
		 SkySiteUtils.waitTill(3000);
		 btndrpattachcom.click();
		 Log.message("Attach from computer button has been clicked");	
		 SkySiteUtils.waitTill(5000);
		 String parentHandle = driver.getWindowHandle(); //Getting parent window
		 Log.message(parentHandle);
		 for (String winHandle : driver.getWindowHandles())
		 {
			 driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		 }
		 SkySiteUtils.waitTill(8000);
		 driver.switchTo().defaultContent();
		 btnselectfile.click();
		 Log.message("Select files button has been clicked");				
		 SkySiteUtils.waitTill(5000);	
		 
		// Writing File names into a text file for using in AutoIT Script
		 BufferedWriter output;
		 randomFileName rn = new randomFileName();
		 //String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		 String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		 output = new BufferedWriter(new FileWriter(tmpFileName, true));
		 
		 String expFilename = null;
		 File[] files = new File(FolderPath).listFiles();
		 
		 for (File file : files) 
		 {
			 if (file.isFile()) 
			 {
				 expFilename = file.getName();// Getting File Names into a variable
				 Log.message("Expected File name is:" + expFilename);
				 output.append('"' + expFilename + '"');
				 output.append(" ");
				 SkySiteUtils.waitTill(5000);
			 }	
		 }
		 output.flush();
		 output.close();
		 
		 String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
		 // Executing .exe autoIt file
		 Runtime.getRuntime().exec(AutoItexe_Path + " " + FolderPath + " " + tmpFileName);
		 Log.message("AutoIT Script Executed!!");
		 SkySiteUtils.waitTill(30000);
		 
		 try 
		 {
			 File file = new File(tmpFileName);
			 if (file.delete()) 
			 {
				 Log.message(file.getName() + " is deleted!");
			 }
			 else 
			 {
				 Log.message("Delete operation is failed.");
			}
		 } 
		 catch (Exception e)
		 {
			 Log.message("Exception occured!!!" + e);
		 }
		 SkySiteUtils.waitTill(8000);
		 btnFileUpload.click();// Clicking on Upload button
		 Log.message("Upload button has been clicked");
		 SkySiteUtils.waitTill(30000);
		 driver.switchTo().window(parentHandle);// Switch back to folder page
		 SkySiteUtils.waitTill(5000);		
		 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		 SkySiteUtils.waitTill(5000);
		 if((driver.findElement(By.xpath("//*[@id='divAttachment']/div[2]/table/tbody/tr[2]/td[1]/a[2]")).isDisplayed())
				 &&(driver.findElement(By.xpath("//i[@class='icon icon-files']")).isDisplayed()))
		 {
			 result2=true;
			 Log.message("Attachment File From Computer Successfuuly");
		 }
		 else
		 {
			 result2=false;
			 Log.message("Attachment File From Computer UnSuccessfuuly");			
		 }
		 SkySiteUtils.waitTill(5000);
		 btnrgtpnlMore.click();//click on Add button
		 Log.message("add button has been clicked");						
		 SkySiteUtils.waitTill(2000);
		 btndrpattachcoll.click();//clickon Attach Fron your Project
		 Log.message("attach fron collection has been clicked");						
		 SkySiteUtils.waitTill(2000);
		 String parentHandle1 = driver.getWindowHandle(); //Getting parent window
		 Log.message(parentHandle1);
		 for (String winHandle : driver.getWindowHandles())
		 {
			 driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		 }
		 SkySiteUtils.waitTill(2000);
		 driver.switchTo().defaultContent();
		 chkdivFilesGrid.click();//click on Check box
		 Log.message("check box button has been clicked");						
		 SkySiteUtils.waitTill(2000);
		 btnbtnprimary.click();
		 Log.message("add & close button has been clicked");						
		 SkySiteUtils.waitTill(5000);							 
		 driver.switchTo().window(parentHandle1);//Switch back to folder page 	
		 SkySiteUtils.waitTill(5000);
		 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		 SkySiteUtils.waitTill(5000);
		 if((driver.findElement(By.xpath("//*[@id='divAttachment']/div[2]/table/tbody/tr[3]/td[1]/a[2]")).isDisplayed())
				 &&(driver.findElement(By.xpath("//i[@class='icon icon-collection']")).isDisplayed()))
		 {
			 result3=true;
			 Log.message("Attach From Project Successfully");
		 }
		 else
		 {
			 result3=false;
			 Log.message("Attach From Project UnSuccessfully");			 
		 }
		 SkySiteUtils.waitTill(5000);
		 btnSaveCommunication.click();
		 Log.message("save & send button has been clicked");
		 SkySiteUtils.waitTill(5000);
		 btnResetSearch.click();//click on Reset button
		 Log.message("Reset button has been clicked");
		 SkySiteUtils.waitTill(5000);
		 String Subject=driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]")).getText();
		 Log.message("Subject name is:"+Subject);
		 SkySiteUtils.waitTill(3000);
		 String Tocontact=driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[3]")).getText();
		 Log.message("To contact name is:"+Tocontact);
		 SkySiteUtils.waitTill(3000);		 
		 if((Subject.contentEquals(SubjectName))&&(Contact.contentEquals(Tocontact))
				 &&(result1==true)&&(result2==true)&&(result3==true))
			 return true;
		 else
			 return false;
   	}
	/** 
    * Method written to verify the communication
    * Scripted By: Trinanjwan   
    */
   
   @FindBy(xpath = "//button[@id='btnResetSearch']")
   WebElement resetbtncom;
   
   @FindBy(xpath = "//button[@data-id='ddlSearch']")
   WebElement selectiondrpdown;
   
   @FindBy(xpath = "//input[@id='txtSearchValue']")
   WebElement textSearch;
   
   @FindBy(xpath = "//button[@id='btnSearch']")
   WebElement buttonSearch;
   
   @FindBy(xpath = "//*[@id=\"divGrid\"]/div[2]/table/tbody/tr[2]/td[2]")
   WebElement tablefieldvalue;
   
   @FindBy(xpath = "//span[contains(text(), 'Subject')]")
   WebElement Subjectoption;
 
 

   
   public boolean verifycommunication(String SubjectCom)
   
   
   {
	   SkySiteUtils.waitTill(5000);
	   driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	   SkySiteUtils.waitTill(4000);
	   SkySiteUtils.waitForElement(driver, resetbtncom, 30);
	   Log.message("Reset button is visible now");
	   resetbtncom.click();
	   SkySiteUtils.waitTill(10000);
	   selectiondrpdown.click();
	   Log.message("Clicked on select dropdown"); 
	   Subjectoption.click();
	   Log.message("Subject option is selected now");
	   
	  
	   SkySiteUtils.waitTill(4000);
	   SkySiteUtils.waitForElement(driver, textSearch, 30);
	   Log.message("The search field is visibled now");
	   textSearch.sendKeys(SubjectCom);
	   Log.message("The value passed in search field: "+SubjectCom);
	   SkySiteUtils.waitTill(5000);
	   buttonSearch.click();
	   Log.message("Clicked on Search button");
	   SkySiteUtils.waitTill(8000);
	   
	   String actualvalue=tablefieldvalue.getText();
	   Log.message("Actual value is:"+actualvalue);
	   Log.message("Expected value is:"+SubjectCom);
	   if(actualvalue.contentEquals(SubjectCom))
		   return true;
	   else
		   return false;
	   
   }
 
}
   