package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arc.fna.utils.randomSharedName;
import com.arcautoframe.utils.Log;

public class AccountTeamPage extends LoadableComponent<AccountTeamPage>
{

	WebDriver driver;
	private boolean isPageLoaded;
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	@FindBy(xpath = "//*[@id='setting']")
	WebElement btnSetting;

	@Override
	protected void load()
	{
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnSetting, 60);
	}

	@Override
	protected void isLoaded() throws Error
	{
		if (!isPageLoaded) 
		{
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public AccountTeamPage(WebDriver driver) 
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	/**
	 * Method written for Team Name(Employee) 
	 * Scripted By: Sekhar
	 * @return
	 */

	public String Random_Teamname() 
	{

		String str = "Team";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}
	
	/**
	 * Method written for Edit Team Name(Employee) 
	 * Scripted By: Sekhar
	 * @return
	 */

	public String Random_Editname() 
	{

		String str = "EditTeam";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}
	
	 @FindBy(xpath = "//i[@class='icon icon-settings ico-lg']")
	 WebElement btnsettings;
	       
	 @FindBy(xpath = "//i[@class='icon icon-account-team icon-lg']")
	 WebElement btnaccountteam;
	 
	 @FindBy(xpath = ".//*[@id='txtProjectRole']")
	 WebElement txtProjectRole;
	 
	 @FindBy(xpath = "//input[@class='btn btn-primary']")
	 WebElement btnsave;
	   
	 @FindBy(xpath = "//input[@id='btnSaveProjAssociation']")
	 WebElement btnSaveProjAssociation;
	 
	 @FindBy(xpath = "//input[@id='btnCloseLockNSync']")
	 WebElement btnCloseLockNSync;
	 
	 @FindBy(xpath = "//i[@class='icon icon-collection ico-lg']")
	 WebElement btncollectionsicon;
	 
	 @FindBy(xpath = "//*[@id='btnResetSearch']")
	 WebElement btnResetSearch;
	 
	 @FindBy(xpath = "//i[@class='icon icon-star ico-lg icon-teams']")
	 WebElement btnTeams;
	 
	 /** 
	  * Method written for Add account team to multiple collections 
	  * Scripted By: Sekhar      
	  */
	
	public boolean Manageteam_Association_Accountteam(String Collectionname,String Collectionname1) 
	{	 	
	 	boolean result1=false;	
	 	boolean result2=false;
	 	boolean result3=false;
	 	boolean result4=false;	 
	 	driver.switchTo().defaultContent();		
	 	SkySiteUtils.waitTill(5000);
	 	btnsettings.click();
	 	Log.message("Setting button has been clicked");
	 	SkySiteUtils.waitTill(5000);				
	 	btnaccountteam.click();
		Log.message("accoun tteam button has been clicked");
	 	SkySiteUtils.waitTill(5000);
	 	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 	SkySiteUtils.waitTill(5000);
	 	String Exp_TeamName=driver.findElement(By.xpath("html/body/form/div[4]/section[2]/div/div[1]/div[2]/ul/li[1]/div/div[2]/a")).getText();
	 	Log.message("Team Name is:"+Exp_TeamName);
	 	SkySiteUtils.waitTill(5000);
	 	driver.findElement(By.xpath("//button[@id='btnLockNSynch']")).click();//click on manage team association
	 	Log.message(" manage team association button has been clicked");
	 	SkySiteUtils.waitTill(5000);		 		
	 	int check_Count=0;
	 	int i= 0;
	 	int k=0;
	 	List<WebElement> allElements1 = driver.findElements(By.xpath("//*[@id='divLockNSynchAddedProject']/div[2]/table/tbody/tr/td[1]/img"));//Taking check box Count
	 	for(WebElement Element : allElements1)
	 	{ 
	 		check_Count = check_Count+1; 		
	 	}		
	 	Log.message("Check box Count is: "+check_Count);
	 	for(i = 1;i<=check_Count;i++)				 
	 	{					 
	 		String Projectnum=driver.findElement(By.xpath("(//*[@id='divLockNSynchAddedProject']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText().toString();
	 		Log.message("Project Num is:"+Projectnum);
	 		if(Projectnum.trim().contains(Collectionname))
	 		{	
	 			result1=true;
	 			Log.message("Exp Project Num Successfully!!!!");	
	 			break;				
	 		}	
	 	}		 
	 	if(result1==true)
	 	{			
	 		SkySiteUtils.waitTill(3000);	
	 		driver.findElement(By.xpath("(//*[@id='divLockNSynchAddedProject']/div[2]/table/tbody/tr/td[1]/img)["+i+"]")).click();//click on check box
	 		for(k = 1;k<=check_Count;k++)				 
	 		{					 
	 			String Projectnum1=driver.findElement(By.xpath("(//*[@id='divLockNSynchAddedProject']/div[2]/table/tbody/tr/td[2])["+k+"]")).getText().toString();
	 			Log.message("Project Num is:"+Projectnum1);
	 			if(Projectnum1.trim().contains(Collectionname1))
	 			{	
	 				result2=true;
	 				Log.message("Exp Collection Num Successfully!!!!");	
	 				break;				
	 			}	
	 		}		 
	 		if(result2==true)
	 		{			
	 			SkySiteUtils.waitTill(3000);	
	 			driver.findElement(By.xpath("(//*[@id='divLockNSynchAddedProject']/div[2]/table/tbody/tr/td[1]/img)["+k+"]")).click();//click on check box
	 			SkySiteUtils.waitTill(5000);				
	 			btnSaveProjAssociation.click();
	 			Log.message("save  button has been clicked");	 		
	 			SkySiteUtils.waitTill(5000);
	 			btnCloseLockNSync.click();
	 			Log.message("close button has been clicked");	
	 			SkySiteUtils.waitTill(5000);	
	 			driver.switchTo().defaultContent();
	 			btncollectionsicon.click();
	 			Log.message("Collections button has been clicked");
	 			SkySiteUtils.waitTill(2000);
	 			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 			SkySiteUtils.waitTill(5000);	
	 			btnResetSearch.click();
	 			SkySiteUtils.waitTill(5000);	
	 			int prjCount_prjList=driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
	 			Log.message("prjCount_prjList: "+prjCount_prjList);
	 			int prj=0;
	 			for(prj = 1; prj <= prjCount_prjList; prj++) 
	 			{
	 							
	 				String prjName=driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)["+prj+"]")).getText();
	 				Log.message(prjName); 				
	 				if(prjName.equals(Collectionname))
	 				{	        	 
	 					result3=true;	 				
	 					driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)["+prj+"]")).click();
	 					SkySiteUtils.waitTill(10000);
	 					Log.message("Project Selection got Successfully!!!!!");
	 					break;
	 				}		 					
	 			}		
	 			SkySiteUtils.waitTill(5000);	
	 			driver.switchTo().defaultContent();
	 			btnTeams.click();
	 			Log.message("Teams button has been clicked");
	 			SkySiteUtils.waitTill(5000);	
	 			driver.switchTo().frame(driver.findElement(By.id("myFrame"))); //Back the Switch to Frame	
	 			SkySiteUtils.waitTill(2000);
	 			int Team_Size=driver.findElements(By.xpath("//i[@class='icon icon-owner-team ico-2x']")).size();
	 			Log.message("Team Size is: "+Team_Size);	 			
	 			int j=0;
	 			for(j = 1;j <= Team_Size;j++) 
	 			{	 							
	 				String TeamName=driver.findElement(By.xpath("(html/body/form/div[5]/section[2]/div/div[1]/div[2]/ul/li/div/div[2])["+j+"]")).getText();
	 				Log.message(TeamName);		    
	 				SkySiteUtils.waitTill(2000);
	 				if(TeamName.equals(Exp_TeamName))
	 				{	        	 
	 					result4=true;
	 					driver.findElement(By.xpath("(html/body/form/div[5]/section[2]/div/div[1]/div[2]/ul/li/div/div[2])["+j+"]")).click();
	 					Log.message("Exp Team has been clicked");
	 					SkySiteUtils.waitTill(10000);
	 					Log.message("Exp Collection Team got Successfully!!!!!");
	 					break;
	 				}		 								 
	 			}	
	 		}
	 		else
	 		{
	 			result2=false;
	 			Log.message("Exp Project num Failed!!!!");	
	 		}
	 	}
	 	else
	 	{
	 		result1=false;
	 		Log.message("Exp Project num Failed!!!!");					 
	 	}			 
	 	
	 	if((result1==true)&&(result2==true)&&(result3==true)&&(result4==true))
	 	{
	 		Log.message("Manage team Association with Project in Account team successfully!!!!!!");	 	
	 		return true;
	 	}
	 	else
	 	{	
	 		Log.message("Manage team Association with Project in Account team Failed!!!!!!");
	 		return false;
	 	}		 
	 }
	
	 @FindBy(xpath = "//*[@id='tbActTeam']/span/a")
	 WebElement btnActTeam;
	 
	 ////input[@id='btnSave']
	 
	 @FindBy(xpath = "//div/div[6]/div/div[3]/div/div/table/tbody/tr/td[1]/input")
	 WebElement btnhr;
	 
	 @FindBy(xpath = "//div/div[6]/div/div[3]/div/div/table/tbody/tr/td[2]/input")
	 WebElement btnlead;
	 
	 @FindBy(xpath = "//input[@id='btnSave']")
	 WebElement btnSave;
	 
	
	/** 
	  * Method written for Add Multiple account team to collection 
	  * Scripted By: Sekhar   
	  */	
	
	public boolean AddMultiple_Account_team_Collection(String CollectionName) 
	{		
		boolean result1=false;	
		boolean result2=false;		
		driver.switchTo().defaultContent();		
		SkySiteUtils.waitTill(5000);
		btncollectionsicon.click();
		Log.message("Collections button has been clicked");	
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));		
		btnResetSearch.click();
		Log.message("reset button has been clicked");		
		SkySiteUtils.waitTill(5000);	
		int prjCount_prjList=driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("prjCount_prjList: "+prjCount_prjList);
		//Loop start '2' as projects list start from tr[2]
		int i=0;
		for(i = 1; i <= prjCount_prjList; i++) 
		{				
			String prjName=driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)["+i+"]")).getText();
			Log.message(prjName);				
			if(prjName.equals(CollectionName))
			{	        	 
				result1=true;
				//Click on collection name where it equal with specified collection name
				driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[11]/a)["+i+"]")).click();
				SkySiteUtils.waitTill(5000);
				break;	         
			}	      
		}
		String parentHandle = driver.getWindowHandle(); 
		Log.message(parentHandle);		 			
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		}	
		SkySiteUtils.waitTill(6000);
		driver.switchTo().defaultContent();				
		btnActTeam.click();  
		Log.message("account team button has been clicked");		
		SkySiteUtils.waitTill(5000);
		btnhr.click();
		Log.message("HR check box has been clicked");		
		SkySiteUtils.waitTill(2000);
		btnlead.click();
		Log.message("LEAD check box has been clicked");
		SkySiteUtils.waitTill(3000);
		btnSave.click();
		Log.message("Save button has been clicked");	
		SkySiteUtils.waitTill(5000);
		driver.switchTo().window(parentHandle);//Switch back to page after upload
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));	
		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)["+i+"]")).click();
		SkySiteUtils.waitTill(5000);	
		driver.switchTo().defaultContent();		 
		btnTeams.click();
		Log.message("teams button has been clicked");		
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));	
		if(driver.findElement(By.xpath("html/body/form/div[5]/section[2]/div/div[1]/div[2]/ul/li[2]/div/div[2]")).isDisplayed()
				&&(driver.findElement(By.xpath("html/body/form/div[5]/section[2]/div/div[1]/div[2]/ul/li[3]/div/div[2]")).isDisplayed()))
		{	
			result2=true;
			Log.message("Exp Teams display Successfully!!!!");			 
		}
		else
		{
			result2=false;
			Log.message("Exp Teams display Failed!!!!");		 
		}				
		if((result1==true)&&(result2==true))
		{
			Log.message("Account team in Project level successfully!!!!!!");
			return true;				 
		}
		else
		{
			Log.message("Account team in Project level Failed!!!!!!");
			return false;			
		}		
	}	
	
	/** 
	  * Method written for addition of team to project directly from account team tab
	  * Scripted By: Sekhar      
	  */
	
	public boolean addition_team_Collection_Accountteam(String Collectionname) 
	{	 	
	 	boolean result1=false;	
	 	boolean result2=false;
	 	boolean result3=false;
	 	boolean result4=false;	 
	 	driver.switchTo().defaultContent();		
	 	SkySiteUtils.waitTill(5000);
	 	btnsettings.click();
	 	Log.message("Setting button has been clicked");
	 	SkySiteUtils.waitTill(5000);				
	 	btnaccountteam.click();
		Log.message("accoun tteam button has been clicked");
	 	SkySiteUtils.waitTill(5000);
	 	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 	SkySiteUtils.waitTill(5000);
	 	String Exp_TeamName=driver.findElement(By.xpath("html/body/form/div[4]/section[2]/div/div[1]/div[2]/ul/li[1]/div/div[2]/a")).getText();
	 	Log.message("Team Name is:"+Exp_TeamName);
	 	SkySiteUtils.waitTill(5000);
	 	driver.findElement(By.xpath("//button[@id='btnLockNSynch']")).click();//click on manage team association
	 	Log.message(" manage team association button has been clicked");
	 	SkySiteUtils.waitTill(5000);		 		
	 	int check_Count=0;
	 	int i= 0;
	 
	 	List<WebElement> allElements1 = driver.findElements(By.xpath("//*[@id='divLockNSynchAddedProject']/div[2]/table/tbody/tr/td[1]/img"));//Taking check box Count
	 	for(WebElement Element : allElements1)
	 	{ 
	 		check_Count = check_Count+1; 		
	 	}		
	 	Log.message("Check box Count is: "+check_Count);
	 	for(i = 1;i<=check_Count;i++)				 
	 	{					 
	 		String Projectnum=driver.findElement(By.xpath("(//*[@id='divLockNSynchAddedProject']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText().toString();
	 		Log.message("Project Num is:"+Projectnum);
	 		if(Projectnum.trim().contains(Collectionname))
	 		{	
	 			result1=true;
	 			Log.message("Exp Project Num Successfully!!!!");	
	 			break;				
	 		}	
	 	}		 
	 	if(result1==true)
	 	{			
	 		SkySiteUtils.waitTill(3000);	
	 		driver.findElement(By.xpath("(//*[@id='divLockNSynchAddedProject']/div[2]/table/tbody/tr/td[1]/img)["+i+"]")).click();//click on check box
	 		SkySiteUtils.waitTill(5000);				
	 		btnSaveProjAssociation.click();
	 		Log.message("save button has been clicked");	 		
	 		SkySiteUtils.waitTill(5000);
	 		btnCloseLockNSync.click();
	 		Log.message("close button has been clicked");	
	 		SkySiteUtils.waitTill(5000);	
	 		driver.switchTo().defaultContent();
	 		btncollectionsicon.click();
	 		Log.message("Collections button has been clicked");
	 		SkySiteUtils.waitTill(2000);
	 		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 		SkySiteUtils.waitTill(5000);	
	 		btnResetSearch.click();
	 		SkySiteUtils.waitTill(5000);	
	 		int prjCount_prjList=driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
	 		Log.message("prjCount_prjList: "+prjCount_prjList);
	 		int prj=0;
	 		for(prj = 1; prj <= prjCount_prjList; prj++) 
	 		{	 			
	 			String prjName=driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)["+prj+"]")).getText();
	 			Log.message(prjName); 				
	 			if(prjName.equals(Collectionname))
	 			{	        	 
	 				result2=true;	 				
	 				driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)["+prj+"]")).click();
	 				SkySiteUtils.waitTill(10000);
	 				Log.message("Project Selection got Successfully!!!!!");
	 				break;
	 			}		 					
	 		}		
	 		SkySiteUtils.waitTill(5000);	
	 		driver.switchTo().defaultContent();
	 		btnTeams.click();
	 		Log.message("Teams button has been clicked");
	 		SkySiteUtils.waitTill(5000);	
	 		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); //Back the Switch to Frame	
	 		SkySiteUtils.waitTill(2000);
	 		int Team_Size=driver.findElements(By.xpath("//i[@class='icon icon-owner-team ico-2x']")).size();
	 		Log.message("Team Size is: "+Team_Size);	 			
	 		int j=0;
	 		for(j = 1;j <= Team_Size;j++) 
	 		{	 							
	 			String TeamName=driver.findElement(By.xpath("(html/body/form/div[5]/section[2]/div/div[1]/div[2]/ul/li/div/div[2])["+j+"]")).getText();
	 			Log.message("Act Team name is:"+TeamName);		    
	 			SkySiteUtils.waitTill(2000);
	 			if(TeamName.equals(Exp_TeamName))
	 			{	        	 
	 				result3=true;
	 				driver.findElement(By.xpath("(html/body/form/div[5]/section[2]/div/div[1]/div[2]/ul/li/div/div[2])["+j+"]")).click();
	 				Log.message("Exp Team has been clicked");
	 				SkySiteUtils.waitTill(10000);
	 				Log.message("Exp Collection Team got Successfully!!!!!");
	 				break;
	 			}	 								 
	 		}			
	 	}
	 	else
	 	{
	 		result1=false;
	 		Log.message("Exp Project num Failed!!!!");					 
	 	}	 	
	 	if((result1==true)&&(result2==true)&&(result3==true))
	 	{
	 		Log.message("addition of team to project directly from account team successfully");	 	
	 		return true;
	 	}
	 	else
	 	{	
	 		Log.message("addition of team to project directly from account team Failed");
	 		return false;
	 	}		 
	 }
	
	 @FindBy(xpath = "//*[@id='btnAddNewTeam']")
	 WebElement btnAddNewTeam;
	 
	 @FindBy(xpath = "//*[@id='txtTeamName']")
	 WebElement txtTeamName;
	 
	 @FindBy(xpath = ".//*[@id='Button1']")
	 WebElement btnAddteammember;
	 
	 @FindBy(xpath = "//*[@id='btnAddClose']")
	 WebElement btnAddClose;
	 
	 @FindBy(xpath = "//*[@id='divTeamGrid']/div[2]/table/tbody/tr[2]/td[2]")
	 WebElement btndivTeamGrid;
	 
	 @FindBy(xpath = "//*[@id='btnSaveTeamData']")
	 WebElement btnSaveTeamData;
	
	/** 
	  * Method written for Edit team in account team level.
	  * Scripted By: Sekhar      
	  */
	
	public boolean Add_Edit_Accountteam(String TeamName,String ContactName,String EditTeam) 
	{	 	
	 	boolean result1=false;		
	 	boolean result2=false;
	 	boolean result3=false;
	 	boolean result4=false;
	 	driver.switchTo().defaultContent();		
	 	SkySiteUtils.waitTill(5000);
	 	btnsettings.click();
	 	Log.message("Setting button has been clicked");
	 	SkySiteUtils.waitTill(5000);				
	 	btnaccountteam.click();
	 	Log.message("accoun tteam button has been clicked");
	 	SkySiteUtils.waitTill(5000);
	 	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 	SkySiteUtils.waitTill(5000);
	 	btnAddNewTeam.click();
	 	Log.message("Add button has been clicked");
	 	SkySiteUtils.waitTill(3000);
	 	txtTeamName.sendKeys(TeamName);
		Log.message("Team name has been entered");
		btnAddteammember.click();
		Log.message("Add team member from address book button has been clicked");
		SkySiteUtils.waitTill(3000);
		int Contactlist=driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
 		Log.message("Contact list is:"+Contactlist);
 		int i=0;
 		for(i=1;i<= Contactlist;i++) 
 		{	 			
 			String Contact_name=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
 			Log.message(Contact_name); 
 			SkySiteUtils.waitTill(3000);
 			if(Contact_name.contains(ContactName))
 			{	        	 
 				result1=true;	 				
 				driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img)["+i+"]")).click();
 				SkySiteUtils.waitTill(8000);
 				Log.message("Contact Selection got Successfully!!!!!");
 				break;
 			}		 					
 		}		
 		SkySiteUtils.waitTill(5000);	
 		btnAddClose.click();
 		Log.message("Add&close button has been clicked");
 		SkySiteUtils.waitTill(3000);
 		if(btndivTeamGrid.isDisplayed())
 		{
 			result2=true;	
 			Log.message("Contact adding successfully");
 		}
 		else
 		{
 			result2=false;	
 			Log.message("Contact adding got failed"); 		
 		}
 		SkySiteUtils.waitTill(3000);
 		btnSaveTeamData.click();
 		Log.message("save&close button has been clicked");
 		SkySiteUtils.waitTill(5000); 		
 		int teamicon_count=driver.findElements(By.xpath("html/body/form/div[4]/section[2]/div/div[1]/div[2]/ul/li/div/div[3]/ul/li[1]/a")).size();
 		Log.message("team icon count is:"+teamicon_count);
 		int j=0;
 		for(j=1;j<= teamicon_count;j++) 
 		{	 			
 			String Team_name=driver.findElement(By.xpath("(html/body/form/div[4]/section[2]/div/div[1]/div[2]/ul/li/div/div[2]/a)["+j+"]")).getText();
 			Log.message(Team_name); 				
 			if(Team_name.contains(TeamName))
 			{	        	 
 				result3=true;	 				
 				driver.findElement(By.xpath("(html/body/form/div[4]/section[2]/div/div[1]/div[2]/ul/li/div/div[3]/ul/li[1]/a)["+j+"]")).click();
 				SkySiteUtils.waitTill(5000);
 				Log.message("Contact Selection got Successfully!!!!!");
 				break;
 			}		 					
 		}	
 		SkySiteUtils.waitTill(3000); 
 		txtTeamName.clear();
		Log.message("Team name has been cleared");
		SkySiteUtils.waitTill(3000); 		
		txtTeamName.sendKeys(EditTeam);
		Log.message("Edit Team name has been entered");
		SkySiteUtils.waitTill(3000); 
		btnSaveTeamData.click();
		Log.message("save&close button has been clicked");
		SkySiteUtils.waitTill(5000);		
		int teamicon_count1=driver.findElements(By.xpath("html/body/form/div[4]/section[2]/div/div[1]/div[2]/ul/li/div/div[3]/ul/li[1]/a")).size();
 		Log.message("team icon count is:"+teamicon_count1);
 		int k=0;
 		for(k=1;k<= teamicon_count1;k++) 
 		{	 			
 			String Team_name1=driver.findElement(By.xpath("(html/body/form/div[4]/section[2]/div/div[1]/div[2]/ul/li/div/div[2]/a)["+k+"]")).getText();
 			Log.message(Team_name1); 				
 			if(Team_name1.contains(EditTeam))
 			{	        	 
 				result4=true;	 				
 				Log.message("Contact Selection got Successfully!!!!!");
 				break;
 			}		 					
 		}	
 		SkySiteUtils.waitTill(3000); 
 		if((result1==true)&&(result2==true)&&(result3==true)&&(result4==true))
 			return true;
 		else
 			return false;
	}
	
	@FindBy(xpath = ".//*[@id='button-1']")
	WebElement btnyes;
	
	@FindBy(xpath = "//span[@class='noty_text']")
	WebElement notifytext;
	
	/** 
	  * Method written for Delete team in account team level.
	  * Scripted By: Sekhar      
	  */
	
	public boolean Add_Delete_Accountteam(String TeamName,String ContactName) 
	{	 	
	 	boolean result1=false;		
	 	boolean result2=false;
	 	boolean result3=false;
	 
	 	driver.switchTo().defaultContent();		
	 	SkySiteUtils.waitTill(5000);
	 	btnsettings.click();
	 	Log.message("Setting button has been clicked");
	 	SkySiteUtils.waitTill(5000);				
	 	btnaccountteam.click();
	 	Log.message("accoun tteam button has been clicked");
	 	SkySiteUtils.waitTill(5000);
	 	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 	SkySiteUtils.waitTill(5000);
	 	btnAddNewTeam.click();
	 	Log.message("Add button has been clicked");
	 	SkySiteUtils.waitTill(3000);
	 	txtTeamName.sendKeys(TeamName);
		Log.message("Team name has been entered");
		btnAddteammember.click();
		Log.message("Add team member from address book button has been clicked");
		SkySiteUtils.waitTill(3000);
		int Contactlist=driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("Contact list is:"+Contactlist);
		int i=0;
		for(i=1;i<= Contactlist;i++) 
		{	 			
			String Contact_name=driver.findElement(By.xpath("(.//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
			Log.message(Contact_name); 
			SkySiteUtils.waitTill(3000);
			if(Contact_name.contains(ContactName))
			{	        	 
				result1=true;	 				
				driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img)["+i+"]")).click();
				SkySiteUtils.waitTill(8000);
				Log.message("Contact Selection got Successfully!!!!!");
				break;
			}		 					
		}		
		SkySiteUtils.waitTill(5000);	
		btnAddClose.click();
		Log.message("Add&close button has been clicked");
		SkySiteUtils.waitTill(3000);
		if(btndivTeamGrid.isDisplayed())
		{
			result2=true;	
			Log.message("Contact adding successfully");
		}
		else
		{
			result2=false;	
			Log.message("Contact adding got failed"); 		
		}
		SkySiteUtils.waitTill(3000);
		btnSaveTeamData.click();
		Log.message("save&close button has been clicked");
		SkySiteUtils.waitTill(8000); 		
		int teamicon_count=driver.findElements(By.xpath("html/body/form/div[4]/section[2]/div/div[1]/div[2]/ul/li/div/div[3]/ul/li[2]/a")).size();
		Log.message("team icon count is:"+teamicon_count);
		int j=0;
		for(j=1;j<= teamicon_count;j++) 
		{	 			
			String Team_name=driver.findElement(By.xpath("(html/body/form/div[4]/section[2]/div/div[1]/div[2]/ul/li/div/div[2]/a)["+j+"]")).getText();
			Log.message(Team_name);
			SkySiteUtils.waitTill(2000);
			if(Team_name.contains(TeamName))
			{	        	 
				result3=true;					
				driver.findElement(By.xpath("(html/body/form/div[4]/section[2]/div/div[1]/div[2]/ul/li/div/div[3]/ul/li[2]/a)["+j+"]")).click();
				Log.message("Exp Download icon Selection got Successfully!!!!!");			
				break;
			}		 					
		}	
		SkySiteUtils.waitTill(5000); 		
		driver.switchTo().defaultContent();
		btnyes.click();
		Log.message("Yes button has been clicked");		
		SkySiteUtils.waitTill(3000); 
		//Account Team is successfully deleted.
		SkySiteUtils.waitForElement(driver, notifytext, 60);
		//String Notify = notifytext.getText();
		
		if((result1==true)&&(result2==true)&&(result3==true)&&(notifytext.isDisplayed()))
			return true;
		else
			return false;
	}
	
}
