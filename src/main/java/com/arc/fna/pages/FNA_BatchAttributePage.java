package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arcautoframe.utils.Log;
import com.gargoylesoftware.htmlunit.javascript.host.Set;

public class FNA_BatchAttributePage extends LoadableComponent<FNA_BatchAttributePage> {
	 WebDriver driver;
	    private  boolean isPageLoaded;
	@Override
	protected void load() {
		// TODO Auto-generated method stub
		isPageLoaded=true;
	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		if (!isPageLoaded)
		{
			Assert.fail();
		}
	}
	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 */
	public FNA_BatchAttributePage(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	//@FindBy(css = "#Button1")
  //@FindBy(css = "#rgtpnlMore")
   // WebElement moreoption;
	// @FindBy(css="#rgtpnlMore")
	  @CacheLookup
	  @FindBy(xpath=".//*[@id='rgtpnlMore']")
	  WebElement moreoption;
	
	 @FindBy(xpath="//*[contains(text(),'Modify attributes')]")	 
	  WebElement modifyattribute;

	 @FindBy (xpath = "//*[@id='rgtpnlMore']")
     WebElement moreLocal ;

     @FindBy(xpath = "//ul[@id='FileActionMenu']/li/a[contains( text(),'Delete')]")
     WebElement deletefilesoptionLocal;


     @FindBy(css = "#Button1")
    WebElement moreoptionfordelete;


     @FindBy(css = "#liDeleteFile1")
     WebElement deletefilesoption;

	 
	 
	/**Method to select a file and open modify attirbute  window
	 * Scripted by tarak
	 */
	public void selectFileandModifyAttribute(String Foldername) 
	{
		 	
			driver.switchTo().defaultContent();
	 		Log.message("Switched to default content");
	 		SkySiteUtils.waitTill(5000);
	 		
	 		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 		Log.message("Switched to frame");
	 		SkySiteUtils.waitTill(4000);
	 	
	 		int Count_no = driver.findElements(By.xpath("//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span")).size();
	 		Log.message("the number folder present are " + Count_no);
	 		SkySiteUtils.waitTill(5000);
	 		int k = 0;
	 		for (k = 1; k <= Count_no; k++) 
	 		{

	 			String foldernamepresent = driver.findElement(By.xpath("(//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["+k+"]")).getText();
	 			if (foldernamepresent.contains(Foldername)) {
	 				Log.message("Required folder is present at " + k + " position in the tree");
	 				driver.findElement(By.xpath("(//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["+ k + "]")).click();
	 				Log.message("Folder is selected");
	 				SkySiteUtils.waitTill(5000);
	 				break;
	 			}
	 		}
	 	
	 		int Itemlist = driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("the number files present are " + Itemlist);
			SkySiteUtils.waitTill(5000);
			
			int j = 0;
			for (j = 1; j <= Itemlist; j++) 
			{
					driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[1])["+j+"]")).click();
					Log.message("File is selected ");
					SkySiteUtils.waitTill(4000);
					break;
					
		     }
			
			if (moreLocal.isDisplayed()) 
			{
                moreLocal.click();
                Log.message("Clicked on Local more");
                SkySiteUtils.waitTill(5000);
                
			}
			else 
			{
                moreoptionfordelete.click();
                Log.message("Clicked on remote more");
                SkySiteUtils.waitTill(5000);
			}

			SkySiteUtils.waitTill(4000);
			SkySiteUtils.waitForElement(driver, modifyattribute, 50);			
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", modifyattribute);
			Log.message("modify attribute is clicked");
            SkySiteUtils.waitTill(6000);
			
		}
	
	
	
	/**Method to generate file name
	 * Scripted By:Tarak
	 * @return
	 */
	  public String Random_filename()
	    {
	           
	           String str="Filename";
	           Random r = new Random( System.currentTimeMillis() );
	           String abc = str+String.valueOf((10000 + r.nextInt(20000)));
	           return abc;
	           
	    }
	  /**Method to generate Title 
		 * Scripted By:Tarak
		 * @return
		 */
		  public String Random_title()
		    {
		           
		           String str="Title";
		           Random r = new Random( System.currentTimeMillis() );
		           String abc = str+String.valueOf((10000 + r.nextInt(20000)));
		           return abc;
		           
		    }
		  /**Method to generate description 
			 * Scripted By:Tarak
			 * @return
			 */
			  public String Random_description()
			    {
			           
			           String str="Description";
			           Random r = new Random( System.currentTimeMillis() );
			           String abc = str+String.valueOf((10000 + r.nextInt(20000)));
			           return abc;
			           
			    }
/**Method to generate Project No 
 * Scripted By:Tarak
	 * @return
 */
  public String Random_Projectno()
	  {
				          
				   String str="ProjectNo";
				   Random r = new Random( System.currentTimeMillis() );
				   String abc = str+String.valueOf((10000 + r.nextInt(20000)));
				   return abc;
				           
	  }
  /**Method to generate Project No 
   * Scripted By:Tarak
  	 * @return
   */
    public String Random_UpdateProjectno()
  	  {
  				          
  				   String str="UpdatedProjectNo";
  				   Random r = new Random( System.currentTimeMillis() );
  				   String abc = str+String.valueOf((10000 + r.nextInt(20000)));
  				   return abc;
  				           
  	  }
    /**Method to generate Project No 
     * Scripted By:Tarak
    	 * @return
     */
      public String Random_UpdateVendor()
    	  {
    				          
    				   String str="UpdatedVendorno";
    				   Random r = new Random( System.currentTimeMillis() );
    				   String abc = str+String.valueOf((10000 + r.nextInt(20000)));
    				   return abc;
    				           
    	  }
  /**Method to generate Project Name 
   * Scripted By:Tarak
  	 * @return
   */
    public String Random_Projectname()
  	  {
  				          
  				   String str="ProjectName";
  				   Random r = new Random( System.currentTimeMillis() );
  				   String abc = str+String.valueOf((10000 + r.nextInt(20000)));
  				   return abc;
  				           
  	  }
    /**Method to generate Invoice
     * Scripted By:Tarak
    	 * @return
     */
      public String Random_Invoice()
    	  {
    				          
    				   String str="Invoice";
    				   Random r = new Random( System.currentTimeMillis() );
    				   String abc = str+String.valueOf((10000 + r.nextInt(20000)));
    				   return abc;
    				           
    	  }
       

	@FindBy(css="#txt_fileName")
	@CacheLookup
	WebElement filename;
	@FindBy(css="#txt_title")
	@CacheLookup
	WebElement Title;
	@FindBy(css="#txt_desc")
	@CacheLookup
	WebElement Description;
	@FindBy(xpath="//input[@value='Update & close']")
	@CacheLookup
	WebElement updateclosebtn;
	
	/**Method to modify general attributes for file
	 * Scripted by Tarak
	 */
	public boolean modifyGeneralAttributes(String filerename, String title, String description) 
	{
		SkySiteUtils.waitTill(3000);
		filename.clear();
		Log.message("Filename is cleared");
		filename.sendKeys(filerename);
		Log.message("Rename for file entered is " +filerename);
		
		SkySiteUtils.waitTill(4000);
		Title.clear();
		Log.message("Title box is cleared");
		Title.sendKeys(title);
		Log.message("Title entered for file is " +title);
		
		SkySiteUtils.waitTill(4000);
		Description.clear();
		Log.message("Description is cleared");
		Description.sendKeys(description);
		Log.message("File description is " +description);
		
		SkySiteUtils.waitTill(4000);
		updateclosebtn.click();
		Log.message("update button is clicked");
		SkySiteUtils.waitTill(4000);
		
		int filesCount=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("No of file present are " +filesCount);
		
		int i=0;
		String filename= null;
		String Titleoffile= null;
		String descriptionofile= null;
		
		for(i=1; i<=filesCount; i++)
		{
			
			filename=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[2]/a[3]")).getText();
			SkySiteUtils.waitTill(2500);
			
			Titleoffile=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[5]")).getText();
			SkySiteUtils.waitTill(2500);
			
			descriptionofile=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[6]")).getText();
			SkySiteUtils.waitTill(2500);
			
		}
		
		SkySiteUtils.waitTill(5000);	
		if(filename.contains(filerename)  &&  Titleoffile.contains(title)  &&  descriptionofile.contains(description))	
		{
			Log.message("File general properties are updated");
			return true;
			
		}	
		else 
		{
			Log.message("File general properties are not updated ");		
			return false;
		}
			
	}
	
	@FindBy(css="#tbProjectKeyDate>a")
	@CacheLookup
	WebElement CoustomPropertiestab;
	@FindBy(xpath="//input[@placeholder='Project number']")
	@CacheLookup
	WebElement Projectnofield;
	@FindBy(xpath=".//*[@id='divCustomFiledData']/div/ul/li[4]/div/div[2]/div/div[2]/a/i")
	@CacheLookup
	WebElement Addicon;
	@FindBy(xpath=".//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr[4]/td[1]/img")
	@CacheLookup
	WebElement selectVendoricon;
	@FindBy(xpath=".//*[@id='lookup_combo_10177904']/div/input[1]")
	@CacheLookup
	WebElement VendorInputBox;
	//for production
	//@FindBy(xpath="//*[@tabindex='3']")
	//@FindBy(xpath="//*[@data-id='10174639']")
	//staging
	//@FindBy(xpath="//*[@data-id='10183928']")
	//for production
	@FindBy(xpath="//*[@data-id='10176181']")
	WebElement vendorinputboxupdate;
	//for production
	@FindBy(xpath="//*[@data-id='10176181']")
	//staging new
	//@FindBy(xpath="//*[@data-id='10183928']")
	//Staging old
		//@FindBy(xpath="//*[@data-id='10178919']")
	//*[@data-id='10183928']
	WebElement vendorinputboxformodify;
	
	@FindBy(xpath="//*[@id='divCustomFiledData']/div/ul/li[3]/div/div[2]/div/div[2]/a/i")
	WebElement plusBtnAddVendor;
	
	@FindBy(xpath=".//*[@id='spnCustomLookUpName']")
	WebElement selectVendorPopOver;
	
	@FindBy(xpath=".//*[@id='txtSerachValue']")
	WebElement selectVendorSearchTxtBox;
	
	@FindBy(xpath=".//*[@id='btnSearch']")
	WebElement selectVendorSearchButton;
	
	@FindBy(xpath=".//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr[2]/td[1]/img")
	WebElement selectSearchedVendor;
	
	
	/**Method to modify custom properties:Vendor & Project No
	 * @throws AWTException 
	 * Scripted by : Tarak
	 */
	public boolean customPropertiesVendorAndPNo(String projectno) throws AWTException
	{
		String vendorname=PropertyReader.getProperty("Vendor");
		SkySiteUtils.waitTill(4000);
		CoustomPropertiestab.click();
		Log.message("Custom Properties tab clicked");
		SkySiteUtils.waitTill(4000);
		
		SkySiteUtils.waitForElement(driver,Projectnofield,40);
		Projectnofield.click();
		Log.message("Project no field is clicked");
		SkySiteUtils.waitTill(3000);	
		Projectnofield.sendKeys(projectno);
		Log.message("Project number entered is " +projectno);
		SkySiteUtils.waitTill(3000);
		
		SkySiteUtils.waitForElement(driver, plusBtnAddVendor, 30);
		plusBtnAddVendor.click();
		Log.message("CLicked on the plus button to add vendor");
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, selectVendorPopOver, 30);
		Log.message("Select vendor popover opened");

		driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup")));
		Log.message("Switched frame");
		SkySiteUtils.waitForElement(driver, selectVendorSearchTxtBox, 60);
		selectVendorSearchTxtBox.clear();
		selectVendorSearchTxtBox.sendKeys(vendorname);
		Log.message("Vendor name entered for search is " +vendorname);
		
		SkySiteUtils.waitForElement(driver, selectVendorSearchButton, 30);
		selectVendorSearchButton.click();
		Log.message("Clicked on Search button");
		SkySiteUtils.waitTill(6000);
		
		SkySiteUtils.waitForElement(driver, selectSearchedVendor, 30);
		selectSearchedVendor.click();
		Log.message("Vendor is added");
		SkySiteUtils.waitTill(8000);
		
		if(btnSaveAndClose.isDisplayed())
		{
			btnSaveAndClose.click();
			Log.message("CLicked on Save and Close button after selecting vendor");
			SkySiteUtils.waitTill(5000);
			
			driver.switchTo().defaultContent();
	 		Log.message("Switched to default content");
	 		SkySiteUtils.waitTill(5000);
	 		
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 		Log.message("Switched frame");
			SkySiteUtils.waitTill(5000);
			
		}			
		else
		{
			driver.switchTo().defaultContent();
	 		Log.message("Switched to default content");
	 		SkySiteUtils.waitTill(5000);
	 		
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 		Log.message("Switched frame");
			SkySiteUtils.waitTill(5000);
			
		}
		
		
		SkySiteUtils.waitForElement(driver, updateclosebtn, 30);
 		updateclosebtn.click();
		Log.message("update button is clicked");
		SkySiteUtils.waitTill(4000);

		int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("No of file present are " +numberoffiles);
		
		String Projectnopresent=null;
		String vendorpresent=null;
		
		for(int i=1;i<numberoffiles;i++) 
		{
			Projectnopresent=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[11]")).getText();
			SkySiteUtils.waitTill(2500);
			Log.message("Updated Project no is:- "+Projectnopresent);
			
			vendorpresent=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[10]")).getText();
			SkySiteUtils.waitTill(2500);	
			Log.message("Updated Vendor is:- "+vendorpresent);
			
		}
		
		if(projectno.equals(Projectnopresent) && vendorpresent.contains(vendorname))	
		{
			
		    Log.message("File is present after searching with project number and vendor ");
		   return true;
			
		}
		else {
			
			
			Log.message("File is not present after search with project number and vendor");
			return false;
		}
	}
	
	
	
	@FindBy(css = "#lftpnlMore")
	@CacheLookup
	WebElement Moreoption;
	@FindBy(xpath=".//*[@onclick='javascript:RemoveFolder();']")
	@CacheLookup
	WebElement Removefolderoption;
	@FindBy(xpath = "//*[@id='button-1']")

	WebElement btnyes;
	/**Method to delete folder after execution
	 * 	Scripted by :Tarak
	 */
		public void deleteFolderFromCollection() {
				driver.switchTo().defaultContent();
				Log.message("Switching to default content");
				SkySiteUtils.waitTill(4000);
				driver.switchTo().frame(driver.findElement(By.id("myFrame")));
				Log.message("SWitched to frame");
				SkySiteUtils.waitTill(4000);
				// SkySiteUtils.waitForElement(driver, Moreoption, 50);

				Moreoption.click();
				SkySiteUtils.waitTill(4000);
				Log.message("More option is clicked");
				SkySiteUtils.waitTill(4000);
				Removefolderoption.click();
				Log.message("Remove folder button is clicked");
				SkySiteUtils.waitTill(10000);
				driver.switchTo().defaultContent();
				SkySiteUtils.waitTill(6000);
				btnyes.click();
				Log.message("yes button has been clicked.");
				
				SkySiteUtils.waitTill(6000);
		        Log.message("Folder has been deleted");
		}
		@FindBy(xpath=".//*[@id='lookup_combo_10176981']/div/input[1]")
		WebElement DocumentTypebox;
		@FindBy(xpath=".//*[@id='10176982']")
		WebElement invoicebox;
		@FindBy(xpath=".//*[@id='10177905']")
		WebElement Projectname;
		@FindBy(xpath=".//*[@id='10177907_true']")
		WebElement aprovalYes;
		@FindBy(xpath=".//*[@class='cflookup_combo']")
		WebElement documenttypesearchbox1;
		@FindBy(xpath="//*[@placeholder='Invoice #']")
		WebElement invoicebxoforupsate;
		@FindBy(xpath="//*[@placeholder='Project name']")
		WebElement projectnameboxupdate;
		/**Method to modify document type,invoice and project name
		 * scripted by Tarak
		 * @throws AWTException 
		 */
		public boolean modifyDocumentTypeInvoiceAndPName(String projectname,String invoice) throws AWTException 
		{
			
			String documenttype=PropertyReader.getProperty("Documenttype");
			SkySiteUtils.waitTill(3000);
			SkySiteUtils.waitForElement(driver, CoustomPropertiestab, 30);
			CoustomPropertiestab.click();
			Log.message("Custom Properties tab is clicked");
			SkySiteUtils.waitTill(4000);
			
			SkySiteUtils.waitForElement(driver, documenttypesearchbox1, 30);
			documenttypesearchbox1.click();
			Log.message("Document type box is clicked");
			SkySiteUtils.waitTill(4000);
			
	        Actions act=new Actions(driver);
	        act.sendKeys(documenttype).build().perform();
			Log.message("Document type entered is " +documenttype);
			SkySiteUtils.waitTill(3000);
			Robot rb= new Robot();
			rb.keyPress(KeyEvent.VK_ENTER);
			rb.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(4000);
			
			SkySiteUtils.waitForElement(driver, invoicebxoforupsate, 30);
			invoicebxoforupsate.click();
			Log.message("invoice box is clicked");
			invoicebxoforupsate.sendKeys(invoice);
			Log.message("Invoice is entered " +invoice);
			SkySiteUtils.waitTill(4000);
			
			SkySiteUtils.waitForElement(driver, projectnameboxupdate, 30);
			projectnameboxupdate.click();
			Log.message("Project name box is clicked");
			projectnameboxupdate.sendKeys(projectname);
			Log.message("Project name entered is " +projectname);
			SkySiteUtils.waitTill(4000);
			
			SkySiteUtils.waitForElement(driver, updateclosebtn, 30);
			updateclosebtn.click();
			Log.message("update button is clicked");
			SkySiteUtils.waitTill(4000);
	
			int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("No of file present are " +numberoffiles);
			
			String Documentypeintable = null;
			String invoiceintable = null;
			String projectnameintable =null;
			
			for(int i=1; i<numberoffiles; i++) 
			{
				Documentypeintable=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[8]")).getText();
				SkySiteUtils.waitTill(2500);
				Log.message("Updated Document Type is:-" +Documentypeintable);
				
				invoiceintable=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[9]")).getText();
				SkySiteUtils.waitTill(2500);
				Log.message("Updated Invoice is:-" +invoiceintable);
				
				projectnameintable=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[12]")).getText();
				SkySiteUtils.waitTill(2500);
				Log.message("Updated Project Name is:-" +projectnameintable);
				
			}
			
			if(Documentypeintable.contains(documenttype) && invoiceintable.contains(invoice) && projectnameintable.contains(projectname))	
			{
				SkySiteUtils.waitTill(2500);
				Log.message("Document & Invoice & project name is updated ");
			    return true;
				
			}
			else 
			{
				
				Log.message("Document & Invoice & project name is not  updated ");
				
				return false;
			}
			
	}
		
		
		
		@FindBy(xpath=".//*[@id='div_otherInfo']/div/div/div[3]/input[2]")
		@CacheLookup
		WebElement closebtn;
			//@FindBy(id="Button6")
		
		@FindBy(xpath=".//*[@id='btnAdvFileSearch']")
		WebElement advancedsearch;
		
		 @FindBy(xpath="//*[contains(text(),'Reset')]")
		// @FindBy(xpath=".//*[@id='divAdvSearchActionForCallingPage']/button[2]")
		WebElement resetbtn;
		@FindBy(xpath="//*[contains(text(),'With Document Type')]")
		
		WebElement checkbox;
		@FindBy(xpath="//input[@class='dhxcombo_input']")
		@CacheLookup
		WebElement documenttypesearchbox;  
		@FindBy(id="btnAdvSearch")
		WebElement Searchbtn;
		
		@FindBy(xpath=".//*[@id='div_otherInfo']/div/div/div[1]/button")
		WebElement btnCross;
		
		
		 /**  Method to search with document type
		 * Scriptedby:Tarak
		 * @throws AWTException 
		 */
		public boolean DocumentTypeSearch() throws AWTException 
		{
			
			SkySiteUtils.waitForElement(driver, btnCross,50);			
			btnCross.click();
			Log.message("Popover cross button is clicked to close modify attributes popover ");
			SkySiteUtils.waitTill(3000);
			
			SkySiteUtils.waitForElement(driver, advancedsearch,50);
			WebElement element = driver.findElement(By.xpath("//*[@id='btnAdvFileSearch']"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		    Log.message("Advanced search option is clicked");

			SkySiteUtils.waitForElement(driver, resetbtn, 50);
			resetbtn.click();
			Log.message("Reset button is clicked");
			
			Actions actclick=new Actions(driver);
		    SkySiteUtils.waitForElement(driver, checkbox, 40);
			actclick.moveToElement(checkbox).click(checkbox).build().perform();
			Log.message("checkbox is clicked");
			
			String documenttypesearch=PropertyReader.getProperty("DocumentTypeSearch");
		    SkySiteUtils.waitForElement(driver,documenttypesearchbox, 40);
		    documenttypesearchbox.sendKeys(documenttypesearch);
		    Log.message("Document type searched " +documenttypesearch);
		    SkySiteUtils.waitTill(3000);
		    
			Robot rb= new Robot();
			rb.keyPress(KeyEvent.VK_ENTER);
			rb.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(8000);
			Searchbtn.click();
			Log.message("search button is clicked");
			SkySiteUtils.waitTill(4000);
			
			SkySiteUtils.waitForElement(driver,advancedsearch, 40);
			int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("No of file present are " +numberoffiles);
			
			String documenttypepresent=null;
			for(int i=1;i<=numberoffiles;i++) 
			{
				documenttypepresent = driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[8]")).getText();
				Log.message("Document type is:- " +documenttypepresent);
			}
			
			if(documenttypesearch.equals(documenttypepresent) && numberoffiles==1)	
			{
			
			    Log.message("Search for document type is validated ");
				return true;
				
			}
			else 
			{
				Log.message("document type searched is not present");
				return false;
				
			}	
			
			
	}
		
		
		@FindBy(xpath="//*[contains(text(),'With Invoice #')]")
		@CacheLookup
		WebElement checkboxforinvoice;
		@FindBy(xpath=".//*[@placeholder='Invoice #']")
		@CacheLookup
		WebElement invoicesearchbox;
		@FindBy(xpath="//select[@id='sddlcond_Search_10177040']")
		@CacheLookup
		WebElement criteriadropdown;
		
		
		/**Method to search with invoice
		 * scripted by :Tarak
		 * @throws AWTException 
		 */
		public boolean searchInvoice() throws AWTException 
		{
			
			SkySiteUtils.waitForElement(driver, btnCross,50);			
			btnCross.click();
			Log.message("Popover cross button is clicked to close modify attributes popover ");
			SkySiteUtils.waitTill(3000);
			
			SkySiteUtils.waitForElement(driver, advancedsearch,50);
			WebElement element = driver.findElement(By.xpath("//*[@id='btnAdvFileSearch']"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		    Log.message("Advanced search option is clicked");
		            
	        SkySiteUtils.waitForElement(driver, resetbtn, 50);
			resetbtn.click();
			Log.message("Reset button is clicked");
			
			Actions actclick=new Actions(driver);
		    SkySiteUtils.waitForElement(driver, checkboxforinvoice, 40);
			actclick.moveToElement(checkboxforinvoice).click(checkboxforinvoice).build().perform();
			Log.message("Checkbox is clicked");
			SkySiteUtils.waitTill(5000);
			
            SkySiteUtils.waitTill(3000);
			String invoicesearch=PropertyReader.getProperty("Invoice");
		    SkySiteUtils.waitForElement(driver,invoicesearchbox, 40);
		    invoicesearchbox.sendKeys(invoicesearch);
		    Log.message("invoice searched is " +invoicesearch);
		    SkySiteUtils.waitTill(3000);
			
		    Robot rb= new Robot();
			rb.keyPress(KeyEvent.VK_ENTER);
			rb.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(5000);
			Searchbtn.click();
			Log.message("search button is clicked");
			SkySiteUtils.waitTill(4000);
			
			SkySiteUtils.waitForElement(driver,advancedsearch, 40);
			int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("No of file present are " +numberoffiles);
			
			String invoicepresent=null;
			for(int i =1;i<=numberoffiles;i++) 
			{
				invoicepresent=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[9]")).getText();
				Log.message("Invoice for the searched file is:- " +invoicepresent);
				
			}
			
			if(invoicepresent.contains(invoicesearch) && numberoffiles==1)	
			{
			
			    Log.message("Search with invoice is validated ");
				return true;
				
			}
			
			else 
			{
				Log.message("search with invoice  is not validated");
				return false;
			}
			
	}
		
		@FindBy(xpath="//input[contains(text(),'With project number')]")
		//@CacheLookup
		WebElement checkboxforprojectnumber;
		@FindBy(xpath="//input[@placeholder='project number']")
		@CacheLookup
		WebElement projectsearchbox;
		
		/**Method to search with Project Number
		 * scripted by:Tarak
		 * @throws AWTException 
		 */
		public boolean searchWithProjectNumber() throws AWTException 
		{
			
			SkySiteUtils.waitForElement(driver, btnCross,50);			
			btnCross.click();
			Log.message("Popover cross button is clicked to close modify attributes popover ");
			SkySiteUtils.waitTill(3000);
			
			SkySiteUtils.waitForElement(driver, advancedsearch,50);
			WebElement element = driver.findElement(By.xpath("//*[@id='btnAdvFileSearch']"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		    Log.message("Advanced search option is clicked");
		            
	        SkySiteUtils.waitForElement(driver, resetbtn, 50);
			resetbtn.click();
			Log.message("Reset button is clicked");
			SkySiteUtils.waitTill(2000);
			
			String Projectnosearch=PropertyReader.getProperty("Project_Number");
		    WebElement Projectnobox=driver.findElement(By.xpath("//input[@placeholder='Project number']"));
		    Projectnobox.click();
		    Log.message("project search box is clicked");
		    SkySiteUtils.waitTill(4000);
		    Projectnobox.sendKeys(Projectnosearch);
		    Log.message("Project number entered is " +Projectnosearch);
		    SkySiteUtils.waitTill(5000);
		    
		    SkySiteUtils.waitForElement(driver, Searchbtn, 30);
		    Searchbtn.click();
			Log.message("search button is clicked");
			SkySiteUtils.waitTill(4000);
			
			SkySiteUtils.waitForElement(driver,advancedsearch, 40);
			int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("No of file present are " +numberoffiles);
			
			String ProjectNoInList=null;
			for(int i=1; i<=numberoffiles; i++) 
			{
					
				ProjectNoInList =driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[11]")).getText();
				Log.message("Project Number for searched file is:- "+ProjectNoInList);	
			
	        }
			
			if(ProjectNoInList.contains(Projectnosearch) && numberoffiles==1)	
			{
				
				    Log.message("Search with Project number is validated !!!!!");
					return true;
					
			}
			else 
			{
					Log.message("Search with Project number  is not validated");
					return false;
			}	
			
		}
		
		
		
		@FindBy(xpath="//input[contains(text(),'With vendor')]")
		@CacheLookup
		WebElement checkboxforvendor;
		@FindBy(xpath =".//*[@id='lookup_combo_Search_10177908']/div/input[1]")
		@CacheLookup
		WebElement vendorsearchbox;
		@FindBy(xpath="//*[contains(text(),'With vendor')]")
		WebElement checkboxVendor;
		
		@FindBy(xpath=".//*[@id='divCustomFieldSearchOption']/div/ul/li[3]/div/div[3]/div/div[2]/a/i")
		WebElement plusBtnVendor;
		
		@FindBy(xpath=".//*[@id='divCustomFieldLookupGrid']/div[2]/table/tbody/tr[2]/td[1]/img")
		WebElement checkboxSearchedVendor;
		
		@FindBy(xpath=".//*[@id='btnSaveNClose']")
		WebElement btnSaveAndClose;
		
	
		/**Search with Vendor
		 * 
		 */
		public boolean searchWithVendor() throws AWTException 
		{

			SkySiteUtils.waitForElement(driver, btnCross,50);			
			btnCross.click();
			Log.message("Popover cross button is clicked to close modify attributes popover ");
			SkySiteUtils.waitTill(3000);
			
			SkySiteUtils.waitForElement(driver, advancedsearch,50);
			WebElement element = driver.findElement(By.xpath("//*[@id='btnAdvFileSearch']"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		    Log.message("Advanced search option is clicked");
		            
	        SkySiteUtils.waitForElement(driver, resetbtn, 50);
			resetbtn.click();
			Log.message("Reset button is clicked");
			SkySiteUtils.waitTill(5000);
			
			Actions actclick=new Actions(driver);
		    SkySiteUtils.waitForElement(driver, checkboxVendor, 40);
			actclick.moveToElement(checkboxVendor).click(checkboxVendor).build().perform();
			Log.message("checkbox is clicked");
			
			String Vendorforsearch=PropertyReader.getProperty("VendorforSearch");
			SkySiteUtils.waitForElement(driver, plusBtnVendor, 30);
			plusBtnVendor.click();
			Log.message("Clicked on plus button to select vendor");
			SkySiteUtils.waitTill(8000);
			
			driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup_Search")));
			Log.message("Switched frame");		
			SkySiteUtils.waitTill(6000);
			
			SkySiteUtils.waitForElement(driver, selectVendorSearchTxtBox, 40);
			selectVendorSearchTxtBox.clear();
			selectVendorSearchTxtBox.sendKeys(Vendorforsearch);
			Log.message("Vendor name entered for search is:- "+Vendorforsearch);
			
			SkySiteUtils.waitForElement(driver, selectVendorSearchButton, 40);
			selectVendorSearchButton.click();
			Log.message("Clicked on search button to find required Vendor");
			SkySiteUtils.waitTill(6000);
			
			SkySiteUtils.waitForElement(driver, checkboxSearchedVendor, 40);
			checkboxSearchedVendor.click();
			Log.message("Selected searched vendor");
			SkySiteUtils.waitTill(8000);
			
			if(btnSaveAndClose.isDisplayed())
			{
				btnSaveAndClose.click();
				Log.message("CLicked on Save and Close button after selecting vendor");
				SkySiteUtils.waitTill(5000);
				
				driver.switchTo().defaultContent();
		 		Log.message("Switched to default content");
		 		SkySiteUtils.waitTill(5000);
		 		
				driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		 		Log.message("Switched to frame");
		 		SkySiteUtils.waitTill(5000);
			}
			else
			{
				driver.switchTo().defaultContent();
		 		Log.message("Switched to default content");
		 		SkySiteUtils.waitTill(5000);
		 		
				driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		 		Log.message("Switched to frame");
		 		SkySiteUtils.waitTill(5000);
			}
			
			SkySiteUtils.waitForElement(driver, Searchbtn, 30);
			Searchbtn.click();
			Log.message("search button is clicked");
			SkySiteUtils.waitTill(5000);
			
			SkySiteUtils.waitForElement(driver,advancedsearch, 40);
			int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("No of file present are " +numberoffiles);
			
			String VendorInList = null;
			for(int i=1;i<=numberoffiles;i++) 
			{
					
				VendorInList =driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[10]")).getText();
				Log.message("Vendor of searched file is:- " +VendorInList);	
				
			}
			
			if(VendorInList.contains(Vendorforsearch) && numberoffiles==1)	
			{
				
				    Log.message("Search with Vendor is validated !!!!!");
					return true;
					
			}
			else 
			{
					Log.message("Search with Vendor  is not validated");
					return false;
					
			}	
		
		}
		
		
		
		
		/**Method to search for Projectname
		 * Scripted by:Tarak
		 * @throws AWTException 
		 */
	public boolean searchWithProjectName() throws AWTException 
	{
		
		SkySiteUtils.waitForElement(driver, btnCross,50);			
		btnCross.click();
		Log.message("Popover cross button is clicked to close modify attributes popover ");
		SkySiteUtils.waitTill(3000);
		
		SkySiteUtils.waitForElement(driver, advancedsearch,50);
		WebElement element = driver.findElement(By.xpath("//*[@id='btnAdvFileSearch']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
	    Log.message("Advanced search option is clicked");
	            
        SkySiteUtils.waitForElement(driver, resetbtn, 50);
		resetbtn.click();
		Log.message("Reset button is clicked");
		SkySiteUtils.waitTill(5000);
		
	    WebElement projectnamebox=driver.findElement(By.xpath("//input[@placeholder='Project name']"));
	    SkySiteUtils.waitTill(3000);
		String Projectnameforsearch=PropertyReader.getProperty("Project_Name");
	    SkySiteUtils.waitForElement(driver,projectnamebox, 40);
	    projectnamebox.click();
	    Log.message("Project name box is clicked");
	    projectnamebox.sendKeys(Projectnameforsearch);
	    Log.message("Project name searched is " +Projectnameforsearch);
	    SkySiteUtils.waitTill(5000);
	    
	    SkySiteUtils.waitForElement(driver, Searchbtn, 30);
	    Searchbtn.click();
		Log.message("search button is clicked");
		SkySiteUtils.waitTill(4000);
		
		SkySiteUtils.waitForElement(driver,advancedsearch, 40);
		int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("No of file present are " +numberoffiles);
		
		String searchedFileProjectName=null;
		for(int i=1;i<=numberoffiles;i++) 
		{
				
			searchedFileProjectName = driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[12]")).getText();
			Log.message("Project Name of searched file is:- " +searchedFileProjectName);	
		
		}
			
		if(Projectnameforsearch.contains(searchedFileProjectName) && numberoffiles==1)	
		{
			
			    Log.message("Search with Project name is validated !!!!!");
				return true;
				
		}
		else 
		{
				Log.message("Search with Project name  is not validated");
				return false;
				
		}	
		
	}
	
	
	
	
	@FindBy(xpath=".//*[@id='txtContentSearch']")
	@CacheLookup
	WebElement keywordcontentsearch;
	/**Method for search with Keyword
	 * 
	 */
	public boolean searchWithKeyword() 
	{
		SkySiteUtils.waitForElement(driver, btnCross,50);			
		btnCross.click();
		Log.message("Popover cross button is clicked to close modify attributes popover ");
		SkySiteUtils.waitTill(3000);
		
		SkySiteUtils.waitForElement(driver, advancedsearch,50);
		WebElement element = driver.findElement(By.xpath("//*[@id='btnAdvFileSearch']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
	    Log.message("Advanced search option is clicked");
	            
        SkySiteUtils.waitForElement(driver, resetbtn, 50);
		resetbtn.click();
		Log.message("Reset button is clicked");
		SkySiteUtils.waitTill(5000);
		
		String keywordforsearch=PropertyReader.getProperty("Keywordforsearch");
		keywordcontentsearch.sendKeys(keywordforsearch);
		Log.message("Keyword searched is " +keywordforsearch);
		 SkySiteUtils.waitTill(5000);
		 Searchbtn.click();
	    Log.message("search button is clicked");
	    SkySiteUtils.waitTill(4000);
	    
		SkySiteUtils.waitForElement(driver,advancedsearch, 40);
		int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("No of file present are " +numberoffiles);
		
		String filenamesearched=PropertyReader.getProperty("Filenameforkeywordsearch");
		Log.message("File name to be searched is " +filenamesearched);
		
		String fileinlist=null;
		for(int i =1;i<=numberoffiles;i++) 
		{
				
			fileinlist =driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[2]/a[3]")).getText();
			Log.message("Name if the searched file is:- "+fileinlist);	
			
		}
			
		if(fileinlist.contains(filenamesearched) && numberoffiles==1)	
		{
			
			    Log.message("Search with keyword is validated !!!!!");
				return true;
				
		}
		else 
		{
				Log.message("Search with  keyword  is not validated");
				return false;
		}	
	
   }
	
	
	
	
	@FindBy(css = "#btnUploadFile")

	WebElement btnUploadFile;

//	@FindBy(xpath = ".//*[@id='fine-uploader']/div/div[2]/div/div[2]/input")
	@FindBy(xpath = ".//*[@id='btnSelectFiles']")
	WebElement btnselctFile;
	
	@FindBy(css="#btnFileAttrLink")
	WebElement addattributelink;
	@FindBy(xpath=".//*[@id='btnSaveNClose']")
	WebElement savebtn;
	@FindBy(xpath = ".//*[@id='chkAllProjectdocs']")

	WebElement checkboxforfileselect;
	/**Method to upload file and add attribute
	 * 
	 */
	
	public void UploadFileAddAttribute(String FolderPath,String tempfilepath,String invoice) throws AWTException, IOException {
		// boolean result1=true;
		driver.switchTo().defaultContent();
		Log.message("Swtiched to default content");
		SkySiteUtils.waitTill(4000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		btnUploadFile.click(); // Adding on New Folder
		Log.message("upload button has been clicked");
		SkySiteUtils.waitTill(8000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			SkySiteUtils.waitTill(5000);// switch focus of WebDriver to the next found window handle (that's your newly
										// opened window)
		}
		SkySiteUtils.waitTill(10000);
		btnselctFile.click();
		Log.message("select files has been clicked");
		SkySiteUtils.waitTill(5000);
		//Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;			
  		randomFileName rn=new randomFileName();
  		String tmpFileName=tempfilepath+rn.nextFileName()+".txt";			
  		output = new BufferedWriter(new FileWriter(tmpFileName,true));	
  				
  		String expFilename=null;
  		File[] files = new File(FolderPath).listFiles();
  				
  		for(File file : files)
  		{
  			if(file.isFile()) 
  			{
  				expFilename=file.getName();//Getting File Names into a variable
  				Log.message("Expected File name is:"+expFilename);	
  				output.append('"' + expFilename + '"');
  				output.append(" ");
  				SkySiteUtils.waitTill(5000);	
  			}	
  		}	
  		output.flush();
  		output.close();	
  				
  		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
  		//Executing .exe autoIt file		 
  		Runtime.getRuntime().exec(AutoItexe_Path+" "+ FolderPath+" "+tmpFileName );			
  		Log.message("AutoIT Script Executed!!");				
  		SkySiteUtils.waitTill(20000);
  				
  		try
  		{
  			File file = new File(tmpFileName);
  			if(file.delete())
  			{
  				Log.message(file.getName() + " is deleted!");
  			}	
  			else
  			{
  				Log.message("Delete operation is failed.");
  			}
  		}
  		catch(Exception e)
  		{			
  			Log.message("Exception occured!!!"+e);
  		}			
  		SkySiteUtils.waitTill(8000);
  		addattributelink.click();
  		Log.message("Add attribute link clicked");
  		SkySiteUtils.waitTill(5000);
  		driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
  		Log.message("switch to frame");
  		checkboxforfileselect.click();
  		Log.message("checkbox clicked");
  		SkySiteUtils.waitTill(5000);
  	//	String filetoselect=PropertyReader.getProperty("Filenameforkeywordsearch");
  	//	String filetoselect=PropertyReader.getProperty("Filenametoaddattribute");
  	//	List<WebElement> filelist=driver.findElements(By.xpath("//*[@id='divAttributeGrid']/div[2]/table/tbody/tr/td"));
  	//	for (WebElement var:filelist)
  		///	if(var.getText().contains(filetoselect))
  		        //       var.click();
  		driver.findElement(By.xpath(".//*[@id='divAttributeGrid']/div[2]/table/tbody/tr[3]/td")).click();
  		Log.message("file is selected");
  		SkySiteUtils.waitTill(6000);

  		WebElement invoicebox12=driver.findElement(By.xpath("//input[@placeholder='Invoice #']"));
        Actions act4=new Actions(driver);
        act4.moveToElement(invoicebox12).click().build().perform();;
  		Log.message("Invoice box is clicked");
  		SkySiteUtils.waitTill(2000);
  		invoicebox12.sendKeys(invoice);
		Log.message("Invoice is entered " +invoice);
  		SkySiteUtils.waitTill(2000);
  		savebtn.click();
  		Log.message("save button is clicked");
  	
		SkySiteUtils.waitTill(30000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(8000);

	}
	/**Method to search with invoice for the updated file and invoice attribute
	 * scripted by :Tarak
	 * @throws AWTException 
	 */
	public boolean searchInvoiceonupdatedfile(String invoice) throws AWTException 
	{
		
		SkySiteUtils.waitForElement(driver, btnCross,50);			
		btnCross.click();
		Log.message("Popover cross button is clicked to close modify attributes popover ");
		SkySiteUtils.waitTill(3000);
		
		SkySiteUtils.waitForElement(driver, advancedsearch,50);
		WebElement element = driver.findElement(By.xpath("//*[@id='btnAdvFileSearch']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
	    Log.message("Advanced search option is clicked");
	            
        SkySiteUtils.waitForElement(driver, resetbtn, 50);
		resetbtn.click();
		Log.message("Reset button is clicked");
		SkySiteUtils.waitTill(5000);
			
	    SkySiteUtils.waitForElement(driver, checkboxforinvoice, 40);
	    Actions actclick=new Actions(driver);
		actclick.moveToElement(checkboxforinvoice).click(checkboxforinvoice).build().perform();
		Log.message("checkbox is clicked");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver,invoicesearchbox, 40);
	    invoicesearchbox.sendKeys(invoice);
	    Log.message("invoice searched is " +invoice);
	    SkySiteUtils.waitTill(3000);
	    
		Robot rb= new Robot();
		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(5000);
		Searchbtn.click();
		Log.message("search button is clicked");
		SkySiteUtils.waitTill(4000);
		
		SkySiteUtils.waitForElement(driver,advancedsearch, 40);
		int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("No of file present are " +numberoffiles);
		
		String invoicepresent=null;

		for(int i =1;i<=numberoffiles;i++) 
		{
			invoicepresent=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[9]")).getText();
			Log.message("Invoice of the searched file is:-"+invoicepresent);
		
		}
			
		if(invoicepresent.contains(invoice) && numberoffiles==1)	
		{
		
		    Log.message("Search with invoice is validated ");
			return true;
			
		}
		else 
		{
			Log.message("search with invoice  is not validated");
			return false;
		}	
		
}
	
	
	
	/**Method to upload file and add custom  attribute
	 * 
	 */
	
	public void UploadFileAddCustomAttribute(String FolderPath,String tempfilepath) throws AWTException, IOException 
	{
		// boolean result1=true;
		driver.switchTo().defaultContent();
		Log.message("Swtiched to default content");
		SkySiteUtils.waitTill(4000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		btnUploadFile.click(); // Adding on New Folder
		Log.message("upload button has been clicked");
		SkySiteUtils.waitTill(8000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle);
			SkySiteUtils.waitTill(5000);// switch focus of WebDriver to the next found window handle (that's your newly
										// opened window)
		}
		SkySiteUtils.waitTill(10000);
		btnselctFile.click();
		Log.message("select files has been clicked");
		SkySiteUtils.waitTill(10000);
		//Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;			
  		randomFileName rn=new randomFileName();
  		String tmpFileName=tempfilepath+rn.nextFileName()+".txt";			
  		output = new BufferedWriter(new FileWriter(tmpFileName,true));	
  				
  		String expFilename=null;
  		File[] files = new File(FolderPath).listFiles();
  				
  		for(File file : files)
  		{
  			if(file.isFile()) 
  			{
  				expFilename=file.getName();//Getting File Names into a variable
  				Log.message("Expected File name is:"+expFilename);	
  				output.append('"' + expFilename + '"');
  				output.append(" ");
  				SkySiteUtils.waitTill(5000);	
  			}	
  		}	
  		output.flush();
  		output.close();	
  				
  		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
  		//Executing .exe autoIt file		 
  		Runtime.getRuntime().exec(AutoItexe_Path+" "+ FolderPath+" "+tmpFileName );			
  		Log.message("AutoIT Script Executed!!");				
  		SkySiteUtils.waitTill(20000);
  				
  		try
  		{
  			File file = new File(tmpFileName);
  			if(file.delete())
  			{
  				Log.message(file.getName() + " is deleted!");
  			}	
  			else
  			{
  				Log.message("Delete operation is failed.");
  			}
  		}
  		catch(Exception e)
  		{			
  			Log.message("Exception occured!!!"+e);
  		}			
  		SkySiteUtils.waitTill(8000);
  		addattributelink.click();
  		Log.message("Add attribute link clicked");
  		SkySiteUtils.waitTill(5000);
  		driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
  		Log.message("switch to frame");
  		checkboxforfileselect.click();
  		Log.message("checkbox clicked");
  		SkySiteUtils.waitTill(6000);
  		
  		//String filetoselect=PropertyReader.getProperty("Filenameforkeywordsearch");
  		//String filetoselect=PropertyReader.getProperty("Filenametoaddattribute");
  		//List<WebElement> filelist=driver.findElements(By.xpath("//*[@id='divAttributeGrid']/div[2]/table/tbody/tr/td"));
  		//for (WebElement var:filelist)
  		//if(var.getText().contains(filetoselect))
        //var.click();
  		
		driver.findElement(By.xpath(".//*[@id='divAttributeGrid']/div[2]/table/tbody/tr[3]/td")).click();
  		Log.message("file is selected");
  		SkySiteUtils.waitTill(6000);

  	    //Log.message("file is selected");
  	    //SkySiteUtils.waitTill(2000);
        
  		
        //for Productions
  		//WebElement vendorsearchbox=driver.findElement(By.xpath("//*[@data-id='10176181']"));
        //staging new
  		//WebElement vendorsearchbox=driver.findElement(By.xpath(".//*[@data-id='10183928']"));
        
  		String vendordata=PropertyReader.getProperty("VendorforSearch");
     	SkySiteUtils.waitForElement(driver, plusButtonVendor, 30);
     	plusButtonVendor.click();
     	Log.message("Clicked on the plus button to select vendor");
     	SkySiteUtils.waitTill(5000);
     	
     	driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup")));
		Log.message("Switched frame");		
		SkySiteUtils.waitTill(6000);
		
		SkySiteUtils.waitForElement(driver, selectVendorSearchTxtBox, 40);
		selectVendorSearchTxtBox.clear();
		selectVendorSearchTxtBox.sendKeys(vendordata);
		Log.message("Vendor name entered for search is:- "+vendordata);
		
		SkySiteUtils.waitForElement(driver, selectVendorSearchButton, 40);
		selectVendorSearchButton.click();
		Log.message("Clicked on search button to find required Vendor");
		SkySiteUtils.waitTill(6000);
		
		SkySiteUtils.waitForElement(driver, checkboxSearchedVendor, 40);
		checkboxSearchedVendor.click();
		Log.message("Clicked on searched vendor checkbox");
		
		driver.switchTo().defaultContent();
 		Log.message("Switched to default content");
 		SkySiteUtils.waitTill(5000);
 		
		driver.switchTo().frame(driver.findElement(By.id("IframeSetBatchAttribute")));
 		Log.message("Switched to frame");
 		SkySiteUtils.waitTill(4000);
		   	
     	/*vendorsearchbox.click();
  		Log.message("Vendor box is clicked");
  		SkySiteUtils.waitTill(2000);
  		Actions act6=new Actions(driver);
  		act6.sendKeys(vendordata).build().perform();
  		//vendorsearchbox.sendKeys(vendordata);
		Log.message("Vendor  is entered " +vendordata);
  		SkySiteUtils.waitTill(2000);
  		Robot rb=new Robot();
  		rb.keyPress(KeyEvent.VK_ENTER);
  		rb.keyRelease(KeyEvent.VK_ENTER);*/
  		
  		savebtn.click();
  		Log.message("save button is clicked");
  	
		SkySiteUtils.waitTill(30000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(8000);

	}
	
	@FindBy(xpath=".//*[@id='divAttCustFieldsData']/div/ul/li[3]/div/div[2]/div/div[2]/a/i")
	WebElement plusButtonVendor;
	
  	/**Method search with All attribute using And operator
  	 * @throws AWTException 
  	 * 
  	 */
    public boolean searchWithAllAttributeAndOperator() throws AWTException 
    {
    	
    	SkySiteUtils.waitForElement(driver, btnCross,50);			
		btnCross.click();
		Log.message("Popover cross button is clicked to close modify attributes popover ");
		SkySiteUtils.waitTill(3000);
		
		SkySiteUtils.waitForElement(driver, advancedsearch,50);
		WebElement element = driver.findElement(By.xpath("//*[@id='btnAdvFileSearch']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
	    Log.message("Advanced search option is clicked");
	            
        SkySiteUtils.waitForElement(driver, resetbtn, 50);
		resetbtn.click();
		Log.message("Reset button is clicked");
		SkySiteUtils.waitTill(5000);
		
		Actions actclick=new Actions(driver);
		SkySiteUtils.waitForElement(driver, checkbox, 40);
	    actclick.moveToElement(checkbox).click(checkbox).build().perform();
		Log.message("checkbox is clicked");
		
		String documenttypesearch=PropertyReader.getProperty("DocumentTypeSearch");
		SkySiteUtils.waitForElement(driver,documenttypesearchbox, 40);
		documenttypesearchbox.sendKeys(documenttypesearch);
		Log.message("Document type searched " +documenttypesearch);
		SkySiteUtils.waitTill(3000);
	    
		Robot rb= new Robot();
		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(5000);
	    SkySiteUtils.waitForElement(driver, checkboxforinvoice, 40);
		actclick.moveToElement(checkboxforinvoice).click(checkboxforinvoice).build().perform();
		Log.message("checkbox is clicked");
		SkySiteUtils.waitTill(8000);
 
		String invoicesearch=PropertyReader.getProperty("Invoice");
	    SkySiteUtils.waitForElement(driver,invoicesearchbox, 40);
	    invoicesearchbox.sendKeys(invoicesearch);
	    Log.message("invoice searched is " +invoicesearch);
	    SkySiteUtils.waitTill(3000);
		Robot rb1= new Robot();
		rb1.keyPress(KeyEvent.VK_ENTER);
		rb1.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(5000);
		
		String Vendorforsearch=PropertyReader.getProperty("VendorforSearch");		
	    SkySiteUtils.waitForElement(driver, checkboxVendor, 40);
		actclick.moveToElement(checkboxVendor).click(checkboxVendor).build().perform();
		Log.message("checkbox is clicked");
		
		SkySiteUtils.waitForElement(driver, plusBtnVendor, 30);
		plusBtnVendor.click();
		Log.message("Clicked on plus button to select vendor");
		SkySiteUtils.waitTill(8000);
		
		driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup_Search")));
		Log.message("Switched frame");		
		SkySiteUtils.waitTill(6000);
		
		SkySiteUtils.waitForElement(driver, selectVendorSearchTxtBox, 40);
		selectVendorSearchTxtBox.clear();
		selectVendorSearchTxtBox.sendKeys(Vendorforsearch);
		Log.message("Vendor name entered for search is:- "+Vendorforsearch);
		
		SkySiteUtils.waitForElement(driver, selectVendorSearchButton, 40);
		selectVendorSearchButton.click();
		Log.message("Clicked on search button to find required Vendor");
		SkySiteUtils.waitTill(6000);
		
		SkySiteUtils.waitForElement(driver, checkboxSearchedVendor, 40);
		checkboxSearchedVendor.click();
		Log.message("Clicked on searched vendor checkbox");
		
		SkySiteUtils.waitForElement(driver, btnSaveAndClose, 40);
		btnSaveAndClose.click();
		Log.message("CLicked on Save and Close button after selecting vendor");
		SkySiteUtils.waitTill(5000);
		
		driver.switchTo().defaultContent();
 		Log.message("Switched to default content");
 		SkySiteUtils.waitTill(5000);
 		
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
 		Log.message("Switched to frame");
 		SkySiteUtils.waitTill(4000);
			   
		Robot rb2= new Robot();
		rb2.keyPress(KeyEvent.VK_DOWN);
		rb2.keyRelease(KeyEvent.VK_DOWN);
		rb2.keyPress(KeyEvent.VK_ENTER);
		rb2.keyRelease(KeyEvent.VK_ENTER);
		Log.message("Enter is pressed");
		SkySiteUtils.waitTill(5000);
		
		String Projectnosearch=PropertyReader.getProperty("Project_Number");
	    WebElement Projectnobox=driver.findElement(By.xpath("//input[@placeholder='Project number']"));
	    Actions act4=new Actions(driver);
	    act4.moveToElement(Projectnobox).click(Projectnobox).build().perform();
	    Log.message("poject search box is clicked");
	    SkySiteUtils.waitTill(4000);
	    Projectnobox.sendKeys(Projectnosearch);
	    Log.message("Project number entered is " +Projectnosearch);
	    SkySiteUtils.waitTill(5000);
	    
	    WebElement projectnamebox=driver.findElement(By.xpath("//input[@placeholder='Project name']"));
	    SkySiteUtils.waitTill(3000);
		String Projectnameforsearch=PropertyReader.getProperty("Project_Name");
	    SkySiteUtils.waitForElement(driver,projectnamebox, 40);
	    projectnamebox.click();
	    Log.message("Project name box is clicked");
	    projectnamebox.sendKeys(Projectnameforsearch);
	    Log.message("Project name searched is " +Projectnameforsearch);
	    SkySiteUtils.waitTill(5000);
	    
	    SkySiteUtils.waitForElement(driver, Searchbtn, 30);
		Searchbtn.click();
		Log.message("search button is clicked");
		SkySiteUtils.waitTill(4000);
		
		SkySiteUtils.waitForElement(driver,advancedsearch, 40);
		String filenamesearched=PropertyReader.getProperty("filesearchwithallattribute");
		
		int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("No of file present are " +numberoffiles);
		
		String fileinlist =null;
		for(int i =1; i<=numberoffiles;i++) 
		{
			fileinlist =driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[2]/a[3]")).getText();
			Log.message("Searched file is: "+fileinlist);
						
		}
		
		SkySiteUtils.waitTill(5000);	
		if(fileinlist.contains(filenamesearched) && numberoffiles==1)	
		{
				
		    Log.message("Search with all attribute is validated!!!!!");
			return true;
			
		}
		else 
		{
			Log.message("Search with all attribute is not  validated");
			return false;
		}	
	
   }
    
    
    
    /**Method to update attribute after adding
     * @throws AWTException 
     * 
     */
    public boolean modifyAddedAttributes(String projectno,String vendorname) throws AWTException 
    {   	
    	
		SkySiteUtils.waitTill(4000);
		CoustomPropertiestab.click();
		Log.message("Custome Properties tab clicked");
		SkySiteUtils.waitTill(4000);
		
		SkySiteUtils.waitForElement(driver,Projectnofield,40);
		Projectnofield.click();
		Log.message("Project no field is clicked");
		SkySiteUtils.waitTill(3000);
		
		Projectnofield.clear();
		Log.message("Project no field is cleared");
		Projectnofield.sendKeys(projectno);
		Log.message("Project number entered is " +projectno);
		SkySiteUtils.waitTill(3000);
		
		SkySiteUtils.waitForElement(driver, plusBtnAddVendor, 30);
		plusBtnAddVendor.click();
		Log.message("CLicked on the plus button to add vendor");
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, selectVendorPopOver, 30);
		Log.message("Select vendor popover opened");

		driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup")));
		Log.message("Switched frame");
		SkySiteUtils.waitForElement(driver, selectVendorSearchTxtBox, 60);
		selectVendorSearchTxtBox.clear();
		selectVendorSearchTxtBox.sendKeys(vendorname);
		Log.message("Vendor name entered for dearch is " +vendorname);
		
		SkySiteUtils.waitForElement(driver, selectVendorSearchButton, 30);
		selectVendorSearchButton.click();
		Log.message("Clicked on Search button");
		SkySiteUtils.waitTill(6000);
		
		SkySiteUtils.waitForElement(driver, selectSearchedVendor, 30);
		selectSearchedVendor.click();
		Log.message("Vendor is added");
		SkySiteUtils.waitTill(8000);
		
		if(btnSaveAndClose.isDisplayed())
		{
			btnSaveAndClose.click();
			Log.message("CLicked on Save and Close button after selecting vendor");
			SkySiteUtils.waitTill(5000);
			
			driver.switchTo().defaultContent();
	 		Log.message("Switched to default content");
	 		SkySiteUtils.waitTill(5000);
	 		
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 		Log.message("Switched frame");
			SkySiteUtils.waitTill(5000);
			
		}			
		else
		{
			driver.switchTo().defaultContent();
	 		Log.message("Switched to default content");
	 		SkySiteUtils.waitTill(5000);
	 		
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 		Log.message("Switched frame");
			SkySiteUtils.waitTill(5000);
			
		}
		
		SkySiteUtils.waitForElement(driver, updateclosebtn, 30);
 		//updateclosebtn.click();
 		WebElement element = driver.findElement(By.xpath("//input[@value='Update & close']"));
 		JavascriptExecutor executor = (JavascriptExecutor)driver;
 		executor.executeScript("arguments[0].click();", element);
		Log.message("update button is clicked");
		SkySiteUtils.waitTill(4000);
	
		int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
		Log.message("No of file present are " +numberoffiles);
		
		String Projectnopresent=null;
		String vendorpresent=null;
		
		for(int i =1;i<numberoffiles;i++) 
		{
			SkySiteUtils.waitTill(4000);
			Projectnopresent=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[11]")).getText();
			Log.message("Edited Project No is:- "+Projectnopresent);
			SkySiteUtils.waitTill(2500);
			 
			vendorpresent=driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[10]")).getText();
			Log.message("Edited Vendor is:- "+vendorpresent);	
			SkySiteUtils.waitTill(2500);
			
		}
		
		if(Projectnopresent.contains(projectno) && vendorpresent.contains(vendorname))	
		{
		
		   Log.message("File is present after searching with project number and vendor ");
		   return true;
			
		}
		else 
		{		
			Log.message("File is not present after search with project number and vendor");
			return false;
		}
		
	}	
    
    
    
        @FindBy(xpath="//input[@placeholder='Project number']")
        WebElement projectsearchbox2;
    	
      
        @FindBy(xpath="//*[text()='Reset']")
        WebElement btnResetAdvSearch;
        
    	/**Method to search the update value 
    	 * @throws AWTException 
    	 * 
    	 */
		public boolean searchUpdatedValue(String updatedprojectno,String vendor) throws AWTException 
		{

			SkySiteUtils.waitForElement(driver, btnCross,50);			
			btnCross.click();
			Log.message("Popover cross button is clicked to close modify attributes popover ");
			SkySiteUtils.waitTill(8000);
			
			SkySiteUtils.waitForElement(driver, advancedsearch,80);		
			WebElement element = driver.findElement(By.xpath("//*[@id='btnAdvFileSearch']"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		    Log.message("Advanced search option is clicked");
		    SkySiteUtils.waitTill(8000);
		            
	        SkySiteUtils.waitForElement(driver, resetbtn, 50);
			resetbtn.click();
			Log.message("Reset button is clicked");
			SkySiteUtils.waitTill(4000);
			
			Actions actclick=new Actions(driver);
		    SkySiteUtils.waitForElement(driver, checkboxVendor, 40);
			actclick.moveToElement(checkboxVendor).click(checkboxVendor).build().perform();
			Log.message("checkbox is clicked");
			
			SkySiteUtils.waitForElement(driver, plusBtnVendor, 30);
			plusBtnVendor.click();
			Log.message("Clicked on plus button to select vendor");
			SkySiteUtils.waitTill(8000);
			
			driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup_Search")));
			Log.message("Switched frame");		
			SkySiteUtils.waitTill(6000);
			
			SkySiteUtils.waitForElement(driver, selectVendorSearchTxtBox, 40);
			selectVendorSearchTxtBox.clear();
			selectVendorSearchTxtBox.sendKeys(vendor);
			Log.message("Vendor name entered for search is:- "+vendor);
			
			SkySiteUtils.waitForElement(driver, selectVendorSearchButton, 40);
			selectVendorSearchButton.click();
			Log.message("Clicked on search button to find required Vendor");
			SkySiteUtils.waitTill(6000);
			
			SkySiteUtils.waitForElement(driver, checkboxSearchedVendor, 40);
			checkboxSearchedVendor.click();
			Log.message("Selected searched vendor");
			SkySiteUtils.waitTill(8000);
			
			if(btnSaveAndClose.isDisplayed())
			{
				btnSaveAndClose.click();
				Log.message("CLicked on Save and Close button after selecting vendor");
				SkySiteUtils.waitTill(5000);
				
				driver.switchTo().defaultContent();
		 		Log.message("Switched to default content");
		 		SkySiteUtils.waitTill(5000);
		 		
				driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		 		Log.message("Switched to frame");
		 		SkySiteUtils.waitTill(5000);
			}
			else
			{
				driver.switchTo().defaultContent();
		 		Log.message("Switched to default content");
		 		SkySiteUtils.waitTill(5000);
		 		
				driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		 		Log.message("Switched to frame");
		 		SkySiteUtils.waitTill(5000);
			}
			
	 		WebElement Projectnobox=driver.findElement(By.xpath("//li[@id='trContentSearchBreak']//li[4]//div[3]//input"));
	 		Projectnobox.click();
	 		Log.message("project number search box is clicked");
	 		Projectnobox.clear();
	 		Projectnobox.sendKeys(updatedprojectno);
	 		Log.message("Project number entered is " +updatedprojectno);
		    SkySiteUtils.waitTill(5000);

			SkySiteUtils.waitForElement(driver, Searchbtn, 30);
			Searchbtn.click();
			Log.message("search button is clicked");
			SkySiteUtils.waitTill(4000);
			
			SkySiteUtils.waitForElement(driver,advancedsearch, 40);
			int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("No of file present are " +numberoffiles);
			String filenamesearched=PropertyReader.getProperty("Searchfilename");
			
			String fileinlist =null;
			for(int i =1;i<=numberoffiles;i++) 
			{
					
				fileinlist =driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[2]/a[3]")).getText();
				Log.message("Searched file is:- "+fileinlist);	

			}
			
			if(fileinlist.contains(filenamesearched) && numberoffiles==1)	
			{
				
				    Log.message("Search with Project number and vendor is validated !!!!!");
					return true;
					
			}
			else 
			{
					Log.message("Search with Project number and vendor is not validated");
					return false;
			}
			
		}
		
				
		/**Search with Vendor
		 * 
		 */
		public boolean searchWithVendorwhenaddingattribute() throws AWTException 
		{

			SkySiteUtils.waitForElement(driver, btnCross,50);			
			btnCross.click();
			Log.message("Popover cross button is clicked to close modify attributes popover ");
			SkySiteUtils.waitTill(3000);
			
			SkySiteUtils.waitForElement(driver, advancedsearch,50);
			WebElement element = driver.findElement(By.xpath("//*[@id='btnAdvFileSearch']"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		    Log.message("Advanced search option is clicked");
		            
	        SkySiteUtils.waitForElement(driver, resetbtn, 50);
			resetbtn.click();
			Log.message("Reset button is clicked");
			SkySiteUtils.waitTill(5000);
					
			Actions actclick=new Actions(driver);
		    SkySiteUtils.waitForElement(driver, checkboxVendor, 40);
			actclick.moveToElement(checkboxVendor).click(checkboxVendor).build().perform();
			Log.message("checkbox is clicked");
			
			String Vendorforsearch=PropertyReader.getProperty("VendorforSearch");
			SkySiteUtils.waitForElement(driver, plusBtnVendor, 30);
			plusBtnVendor.click();
			Log.message("Clicked on plus button to select vendor");
			SkySiteUtils.waitTill(8000);
			
			driver.switchTo().frame(driver.findElement(By.id("ifrmCustomFieldLookup_Search")));
			Log.message("Switched frame");		
			SkySiteUtils.waitTill(6000);
			
			SkySiteUtils.waitForElement(driver, selectVendorSearchTxtBox, 40);
			selectVendorSearchTxtBox.clear();
			selectVendorSearchTxtBox.sendKeys(Vendorforsearch);
			Log.message("Vendor name entered for search is:- "+Vendorforsearch);
			
			SkySiteUtils.waitForElement(driver, selectVendorSearchButton, 40);
			selectVendorSearchButton.click();
			Log.message("Clicked on search button to find required Vendor");
			SkySiteUtils.waitTill(6000);
			
			SkySiteUtils.waitForElement(driver, checkboxSearchedVendor, 40);
			checkboxSearchedVendor.click();
			Log.message("Clicked on searched vendor checkbox");
			
			SkySiteUtils.waitForElement(driver, btnSaveAndClose, 40);
			btnSaveAndClose.click();
			Log.message("CLicked on Save and Close button after selecting vendor");
			SkySiteUtils.waitTill(5000);
			
			driver.switchTo().defaultContent();
	 		Log.message("Switched to default content");
	 		SkySiteUtils.waitTill(5000);
	 		
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 		Log.message("Switched to frame");
	 		SkySiteUtils.waitTill(4000);
			
			SkySiteUtils.waitForElement(driver, Searchbtn, 30);
			Searchbtn.click();
			Log.message("search button is clicked");
			SkySiteUtils.waitTill(4000);
			
			String vendorname =null;
			int numberoffiles=driver.findElements(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
			Log.message("No of file present are " +numberoffiles);
			SkySiteUtils.waitTill(6000);
			
			for(int i =1;i<=numberoffiles;i++) 
			{
					
				vendorname =driver.findElement(By.xpath("html/body/form/div[3]/section[2]/div/div[2]/section/div/div/div[1]/div[2]/div[2]/table/tbody/tr["+i+"+"+1+"]/td[10]")).getText();
				Log.message("Searched file vendor name is:- "+vendorname);
				
				
			}
		
			if(vendorname.contains(Vendorforsearch) && numberoffiles==1)	
			{
				
				    Log.message("Search with Project number and vendor is validated !!!!!");
					return true;
					
			}
			else 
			{
					Log.message("Search with Project number and vendor is not validated");
					return false;
			}	
		
		}
		
}






    	
  

	
	

