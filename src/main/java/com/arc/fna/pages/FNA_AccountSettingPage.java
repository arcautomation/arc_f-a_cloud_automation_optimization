package com.arc.fna.pages;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arcautoframe.utils.Log;

public class FNA_AccountSettingPage extends LoadableComponent<FNA_AccountSettingPage> {
	WebDriver driver;
	private boolean isPageLoaded;
	
	@FindBy(xpath = "//*[text()='Change logo']")
	WebElement btnChangeLogo;
	
	@FindBy(xpath="//*[@id='btnGetCroppedImage']")
	WebElement savebtn;
	
	@FindBy(xpath="//*[@id='uploadlogo']")
	WebElement btnAddlogo;
	
	@Override
	protected void load() {
		// TODO Auto-generated method stub
		isPageLoaded = true;
		SkySiteUtils.waitTill(7000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitForElement(driver, btnAddlogo, 60);
		Log.message("Account setting page is loaded");
		driver.switchTo().defaultContent();
	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	public FNA_AccountSettingPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath="//a[text()='Branding']")
	WebElement brandingtab;
	@FindBy(xpath="//a[text()='General Settings']")
	WebElement Generaltab;
    /**Select branding tab of AccountSettings
     * 
     */
	public void selectBrandingTab() {
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(3000);
		//Log.message("Switching to  frame to add logo");
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Swtiched to frame");
		SkySiteUtils.waitForElement(driver, brandingtab,50);
		brandingtab.click();
		Log.message("Branding tab is clicked");
		
}
	 /**Select General tab of AccountSettings
     * 
     */
	public void selectGeneralTab() {
		driver.switchTo().defaultContent();

		//Log.message("Switching to  frame to add logo");
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Swtiched to frame");
		SkySiteUtils.waitForElement(driver, Generaltab,50);
		Generaltab.click();
		Log.message("General  tab is clicked");
		
}
	@FindBy(xpath="//a[text()='Sign in method']")
	WebElement SignIntab;
	/**Select sign in tab of account settings
     * 
     */
	public void selectSignInTab() {
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(3000);
		//Log.message("Switching to  frame to add logo");
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Swtiched to frame");
		SkySiteUtils.waitForElement(driver, SignIntab,50);
		SignIntab.click();
		Log.message("Sign in tab is clicked");
		
}
	
	@FindBy(xpath="//*[text()='Password Settings']")
	WebElement passwordsettingtab;
	/**method to select password setting tab
	 * 
	 
	 */
	public void selectPasswordSettingtab() {
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(3000);
		//Log.message("Switching to  frame to add logo");
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Swtiched to frame");
		SkySiteUtils.waitForElement(driver, passwordsettingtab,50);
		passwordsettingtab.click();
		Log.message("password setting tab is clicked");
	}
	/**
	 * Method to add logo in account setting Scripted by : Tarak
	 * 
	 * @throws IOException
	 */
	public boolean addLogo(String tempfilepath, String folderpath) throws IOException 
	{

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(3000);
		
		Log.message("Switching to  frame to add logo");
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitForElementLoadTime(driver, btnAddlogo, 30);
		btnAddlogo.click();
		Log.message("Clicked on Add Logo button");
		SkySiteUtils.waitTill(5000);
		
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = tempfilepath + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		
		String expFilename = null;
		File[] files = new File(folderpath).listFiles();

		for (File file : files) 
		{
			if (file.isFile()) 
			{
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(5000);
				
			}
		}
		output.flush();
		output.close();

		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
		// Executing .exe autoIt file
		Runtime.getRuntime().exec(AutoItexe_Path + " " + folderpath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(30000);
		
		try 
		{
			File file = new File(tmpFileName);
			if (file.delete()) 
			{
				Log.message(file.getName() + " is deleted!");
				
			} 
			else 
			{
				Log.message("Delete operation is failed.");
				
			}
		} 
		
		catch (Exception e) 
		{
			Log.message("Exception occured!!!" + e);
			
		}
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, savebtn, 30);
		savebtn.click();
		Log.message("Save Cropped Logo button is clicked");
		SkySiteUtils.waitTill(5000);
		
		if (driver.findElement(By.xpath(".//*[@id='imgClientLogo']")).isDisplayed() == true) 
		{
			Log.message("Addition of logo is done successfully");
			return true;

		} 
		else 
		{
			return false;
		}

	}

	@FindBy(xpath = ".//*[@id='logodeletebutton']/i")
	WebElement deletelogo;

	@FindBy(xpath = ".//*[@id='button-1']")
	WebElement Yesbtn;
	@FindBy(xpath = ".//*[@id='button-0']")
	WebElement Nobtn;

	/**
	 * Method to delete logo
	 * @return 
	 * 
	 */
	public boolean deleteLogo() 
	{
		
		SkySiteUtils.waitForElementLoadTime(driver, deletelogo, 30);
		deletelogo.click();
		Log.message("Delete logo button is clicked");
		
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, Yesbtn, 40);
		Yesbtn.click();
		Log.message("Yes button is clicked");
		SkySiteUtils.waitTill(5000);
		
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitForElement(driver, btnAddlogo, 60);
		
		if(btnAddlogo.isDisplayed())
		{
			return true;			
		}
		else
		{
			return false;		
		}
				
	}

	/**
	 * Method to delete logo and verify deletion
	 * 
	 */
	public boolean deleteLogoandverify() {
		deletelogo.click();
		Log.message("delete logo button is clicked");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, Yesbtn, 40);
		Yesbtn.click();
		Log.message("Yes button is clicked");
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		if (!(driver.findElement(By.xpath(".//*[@id='imgClientLogo']")).isDisplayed())) {
			Log.message("Added logo is deleted successfully");
			return true;

		} else {

			return false;
		}

	}

	/**
	 * method to delete existing logo Scripted By:Tarak
	 * 
	 */
	public void delete_existinglogo() {
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(6000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(2000);
		List<WebElement> ele = driver.findElements(By.xpath("//*[@class='icon icon-close-round icon-lg']"));
		SkySiteUtils.waitTill(2000);
		// if(driver.findElement(By.xpath(".//*[@id='imgClientLogo']")).isDisplayed()==true)
		// {
		if (ele.size() > 0) {
			Log.message("Logo already existing");
			driver.switchTo().defaultContent();
			SkySiteUtils.waitTill(3000);
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			driver.findElement(By.xpath("//*[@class='icon icon-close-round icon-lg']")).click();
			Log.message("Delete button is clicked");
			SkySiteUtils.waitTill(5000);
			driver.switchTo().defaultContent();
			SkySiteUtils.waitForElement(driver, Yesbtn, 40);
			Yesbtn.click();
			Log.message("Yes button is clicked");
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			SkySiteUtils.waitForElement(driver, btnAddlogo, 60);
			Log.message("Logo is deleted succcesfully");
			// Log.message("Existing logo deleted sucessfully");

		}

		else if (ele.size() < 0) {
			Log.message("No logo exist");
		}

	}

	/**
	 * Method to change logo Scripted by :Tarak
	 * 
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	public boolean changeLogo(String tempfilepath, String folderpath, String filepath1,String filepathtemp) throws IOException, InterruptedException 
	{
		
		SkySiteUtils.waitForElement(driver, btnChangeLogo, 40);
		btnChangeLogo.click();
		Log.message("Change logo button is clicked ");	
		
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = tempfilepath + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, false));
		String expFilename = null;
		File[] files = new File(folderpath).listFiles();

		for (File file : files) 
		{
			if (file.isFile()) 
			{
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(5000);
			
			}
		}
		output.flush();
		output.close();

		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
		// Executing .exe autoIt file
		Runtime.getRuntime().exec(AutoItexe_Path + " " + folderpath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(30000);

		try 
		{
			File file = new File(tmpFileName);
			if (file.delete()) 
			{
				Log.message(file.getName() + " is deleted!");
			
			} 
			else 
			{
				Log.message("Delete operation is failed.");
				
			}
		} 
		
		catch (Exception e) 
		{
			Log.message("Exception occured!!!" + e);
			
		}
		
		SkySiteUtils.waitForElement(driver, savebtn, 40);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(10000);
		
		//SkySiteUtils.waitForElement(driver, btnChangeLogo, 40);

		String savedscreenshot1 = filepathtemp + "\\Finallogopic.png";
		String savedscreenshot2 = filepathtemp + "\\Changedlogoruntime.png";
		
		Log.message("Taking screenshot of changed logo");
		SkySiteUtils.waitTill(5000);
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));
		Log.message("Screenshot taken of changed logo");
		SkySiteUtils.waitTill(5000);
		
		Log.message("Checking the saved screenshot with temp screenshot");
		
		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
		
		//Thread.sleep(2000);
		
		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);
		
		SkySiteUtils.waitTill(5000);
		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) 
		{
			Log.message("image height width are different");
			return false;
			
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		
		for (int y = 0; y < height; y++) 
		{
			for (int x = 0; x < width; x++) 
			{
				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) 
				{
					Log.message("Image are different");
					return false;
					
				}
			}

		}

		return true;

	}

	/**
	 * Method to delete screenshot
	 * 
	 * @param Screenshotpath
	 * @return
	 */
	public void delteScreenShot(String Screenshotpath) {
		Log.message("Searching screenshot downloaded");
		File file = new File(Screenshotpath);
		File[] Filearray = file.listFiles();
		if (file.isDirectory()) {
			for (File Myfile : Filearray) {
				if (Myfile.getName().contains("Finallogopic.png")) {
					Myfile.delete();
					Log.message("File is deleted successfully");
				}
			}
		}

	}

	@FindBy(xpath = ".//*[@id='rdAlphanumeric']")
	WebElement alphanumericbutn;
	//@FindBy(css = "#btnSettingsSave")
	@FindBy(xpath=".//*[@id='Button5']")
	WebElement savesettingbtn;
    // @FindBy(css = "#btnSettingsSave")
	@FindBy(xpath=".//*[@id='Button7']")
    WebElement savesettingbtnpw;
	@FindBy(xpath = ".//*[@id='ProjectFiles']/a")
	WebElement documenttab;
	@FindBy(css = "#lftpnlMore")
	WebElement moreoption;
	@FindBy(css = "#btnFolderSort>a")
	WebElement foldersortingoption;

	/**
	 * Method to select foldersorting option and verify the same in document tab
	 * 
	 */
	public boolean selectFolderSortingOption() {
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		SkySiteUtils.waitForElement(driver, alphanumericbutn, 40);
		alphanumericbutn.click();
		Log.message("Alphanumericbutton is clicked");
		SkySiteUtils.waitForElement(driver, savesettingbtn, 40);
		savesettingbtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(2000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", documenttab);

		Log.message("document tab is clicked");
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitForElement(driver, moreoption, 40);
		moreoption.click();
		Log.message("more option is clicked");
		foldersortingoption.click();
		Log.message("folder sorting option is clikced");
		SkySiteUtils.waitTill(2000);
		if (driver.findElement(By.xpath(".//*[@id='radName']")).isSelected() == true) {
			Log.message("Alphnumeric option selected by default");
			return true;
		} else {

			Log.message("Alphabnumeric option is clicked");
			return false;

		}

	}

	@FindBy(xpath = ".//*[@id='btnFolderSortClose']")
	WebElement closefolderoption;

	/**
	 * Method to close folder option
	 * 
	 * 
	 */
	public FnaHomePage closeFolderOption() {
		SkySiteUtils.waitForElement(driver, closefolderoption, 40);
		closefolderoption.click();
		Log.message("closefolder option is clicked");
		driver.switchTo().defaultContent();
		return new FnaHomePage(driver).get();

	}

	@FindBy(css = "#rdManuallyOrganized")
	WebElement manuallyorganizesoption;

	/**
	 * Method to change folder sorting option to manually organized
	 * 
	 */
	public void setOptionToManuallyOrganized() {
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitForElement(driver, manuallyorganizesoption, 40);
		manuallyorganizesoption.click();
		Log.message("Manually organized button is clicked");
		SkySiteUtils.waitForElement(driver, savesettingbtn, 40);
		savesettingbtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(3000);
		if (manuallyorganizesoption.isSelected()) {
			Log.message("manually organized button is selected ");
		} else {

			Log.message("Manually organized button is not selected");
		}

	}

	@FindBy(css = "#rdDeletePermanently")
	WebElement deltepermanentlyoption;
	@FindBy(xpath = ".//*[@id='liRecycle']/a")
	WebElement Recyclebinoption;
	@FindBy(xpath = ".//*[@id='btnRecycleSetting']")
	WebElement settingbtn;

	/**
	 * Method to select recyclebin option in account setting and check it reflects
	 * in recycle bin tab
	 * 
	 */
	public boolean setRecyclebinOption() {
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		SkySiteUtils.waitForElement(driver, deltepermanentlyoption, 40);
		deltepermanentlyoption.click();
		Log.message("Delete Permanently option is clicked");
		SkySiteUtils.waitForElement(driver, savesettingbtn, 40);
		savesettingbtn.click();
		Log.message("save button is clicked");
		driver.switchTo().defaultContent();
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", Recyclebinoption);
		// Recyclebinoption.click();
		Log.message("Recyclebin option is clicked");
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitForElement(driver, settingbtn, 40);
		settingbtn.click();
		Log.message("Setting button is clicked");
		SkySiteUtils.waitTill(4000);
		if (driver.findElement(By.xpath(".//*[@id='rdDeletePermanently']")).isSelected() == true) {
			Log.message("Delete Permanent option is selected in recyclebin tab");

			return true;
		} else {
			Log.message("Option selected from setting tab is not reflected in recyclebin tab");
			return false;
		}

	}

	@FindBy(xpath = ".//*[@id='divTeamInfo']/div/div/div[3]/button")
	WebElement closebtn;

	/**
	 * Method to close folder option
	 * 
	 * 
	 */
	public FnaHomePage closeSettingTab() {
		SkySiteUtils.waitForElement(driver, closebtn, 40);
		closebtn.click();
		Log.message("closefolder option is clicked");
		driver.switchTo().defaultContent();
		return new FnaHomePage(driver).get();

	}

	@FindBy(xpath = ".//*[@id='rdMoveToRecycle']")
	WebElement MoveToRecyclebin;

	/**
	 * Method to select move to recyclebin option
	 * 
	 */

	public void setOptionMoveToRecyclebin() {
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitForElement(driver, MoveToRecyclebin, 40);
		MoveToRecyclebin.click();
		Log.message("Move to Recyclebin button is clicked");
		SkySiteUtils.waitForElement(driver, savesettingbtn, 40);
		savesettingbtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(3000);
		if (MoveToRecyclebin.isSelected()) {
			Log.message("Move to recyclebin option is selected ");
		} else {

			Log.message("Move to recyclebin  button is not selected");
		}

	}

	@FindBy(xpath = ".//*[@id='txtExpiryPeriod']")
	WebElement expiryperiod;
	//@FindBy(xpath = ".//*[@id='divFolderSort']/div[6]/div/div[2]/div/button")
	@FindBy(xpath=".//*[@id='Div2']/div[1]/div[4]/div/div[2]/div/button")
	WebElement dropdown;
	//@FindBy(xpath = ".//*[@id='divFolderSort']/div[6]/div/div[2]/div/div/ul/li[3]/a")
	@FindBy(xpath=".//*[@id='Div2']/div[1]/div[4]/div/div[2]/div/div/ul/li[3]/a")
	WebElement weekoption;
	//@FindBy(xpath = ".//*[@id='divFolderSort']/div[6]/div/div[2]/div/div/ul/li[4]/a")
	@FindBy(xpath=".//*[@id='Div2']/div[1]/div[4]/div/div[2]/div/div/ul/li[4]/a")
	WebElement dayoption;
	@FindBy(xpath = ".//*[@id='Collections']/a")
	WebElement collectiontab;

	/**
	 * Folder to set link expiration (in Days) for link files
	 * 
	 */
	public void setExpirationInDaysForFiles(String noofdays) {
		driver.switchTo().defaultContent();

		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		SkySiteUtils.waitForElement(driver, expiryperiod, 50);
		// String no="5";
		expiryperiod.click();
		Log.message("expiry period box clicked");
		expiryperiod.clear();
		Log.message("expiry period box cleared");

		expiryperiod.sendKeys(noofdays);
		Log.message("expiry period has been set to " + noofdays);
		SkySiteUtils.waitTill(2000);
		dropdown.click();
		Log.message("Drop down is clicked");
		SkySiteUtils.waitTill(3000);
		dayoption.click();
		Log.message("day option is selected");
		SkySiteUtils.waitForElement(driver, savesettingbtn, 40);
		savesettingbtn.click();
		Log.message("save button is clicked");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", collectiontab);
		// collectiontab.click();
		Log.message("Collection tab is clicked");

	}

	@FindBy(xpath = ".//*[@id='aspnetForm']/div[3]/section[2]/div/div[2]/nav/div[5]/div[1]/button[2]")
	WebElement linkfileoption;
	@FindBy(xpath = ".//*[@id='txtExpiryDate_hyperlink']")
	WebElement expirationdate;
	@FindBy(xpath="//*[@id='Button1']")
	WebElement moreoption12;
	@FindBy(xpath="//a[text()=' Link files']")
	WebElement linkmenu;
	@FindBy (xpath = "//*[@id='rgtpnlMore']")
    WebElement moreLocal ;
	@FindBy(xpath=".//*[@data-original-title='Link files']")
	WebElement btnLinkFiles;


    @FindBy(css = "#Button1")
   WebElement moreoptionfordelete;
    
	/**
	 * Open file link option and verify the set expiration date from settings
	 * 
	 */
	public boolean openLinkFileOptionVerifySetDayPeriod(String noofdays) {
		int outputtotaldate=0;
		// Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		// driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		// Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='ParentFolder']")).click();
		Log.message("Folder is selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[1]/img")).click();
		Log.message("File is selected");
		
		if (moreLocal.isDisplayed()) 
		{
            moreLocal.click();
            Log.message("Clicked on Local more");
            SkySiteUtils.waitTill(5000);
            
		}
		else 
		{
            moreoptionfordelete.click();
            Log.message("Clicked on remote more");
            SkySiteUtils.waitTill(5000);
		}
		
		SkySiteUtils.waitTill(6000);
		/*SkySiteUtils.waitForElement(driver, moreoption12,60);
		moreoption12.click();
		Log.message("more option is clicked");*/
		if(btnLinkFiles.isDisplayed())
		{
			btnLinkFiles.click();
			Log.message("Link file button is clicked");
		}
		else
		{
			linkmenu.click();
			Log.message("Link file option is clicked from more menu");
			
		}
		//SkySiteUtils.waitForElement(driver, linkmenu, 50);
		
		
		
		//SkySiteUtils.waitTill(3000);*/
     // SkySiteUtils.waitTill(4000);
   //  driver.findElement(By.xpath("//*[@data-original-title='Link files']")).click();
    //  Log.message("Link file option is clicked");
        SkySiteUtils.waitTill(5000);
		String date1 = expirationdate.getAttribute("value");
		Log.message("The date is " + date1);
		Log.message("split(String regex, int limit) with limit=3:");
		String array2[] = date1.split("/", 3);
		for (String temp : array2) {
			Log.message(temp);
		}
		Date d = new Date();
		Log.message("the current date is " + d.toString());
		String currenttimearray[] = d.toString().split(" ");
		for (String cdata : currenttimearray) {
			Log.message(cdata);
		}
		// as week is 2
		// String noofdays="14";
		int inttime1 = Integer.parseInt(currenttimearray[2]);
		String month=currenttimearray[1];
		int noofdaysint = Integer.parseInt(noofdays);
		if(inttime1>26 && (month.equals("Jan") || month.equals("Mar") || month.equals("May") || month.equals("July") || month.equals("Aug") || month.equals("Oct") || month.equals("Dec")) ) {
		switch (inttime1) {
		case 27:
			outputtotaldate = 1 ;
			break;
		case 28:
			outputtotaldate = 2 ;
			break;
		case 29:
			outputtotaldate = 3;
			break;
		case 30:
			outputtotaldate = 4;
			break;
		case 31:
			outputtotaldate = 5;
			break;
		 default:
			Log.message("Invalid month");

		}
		}
		else if(inttime1>26 && (month.equals("April") || month.equals("Feb") || month.equals("Jun") || month.equals("Sep") || month.equals("Aug") || month.equals("Nov") || month.equals("Dec")))  {
			switch (inttime1) {
			case 27:
				outputtotaldate = 2 ;
				break;
			case 28:
				outputtotaldate = 3 ;
				break;
			case 29:
				outputtotaldate = 4;
				break;
			case 30:
				outputtotaldate = 5;
				break;
			 default:
				Log.message("Invalid month");

			}
			
		}
		
		else {

             outputtotaldate = inttime1 + noofdaysint;
		}
		Log.message("Total expected date for expiry is " + outputtotaldate);
		int expirydaypresent = Integer.parseInt(array2[1]);
		Log.message("Expiry date is " + expirydaypresent);
      
		if (outputtotaldate == expirydaypresent) {

			Log.message("expiry date is reflected successfully");
			return true;
		}
		return false;
	}

	@FindBy(css = "#setting")
	@CacheLookup
	WebElement Settingsbtn;

	@FindBy(css = "#liAccountSettings > a:nth-child(1)")
	@CacheLookup
	WebElement AccountSettingsbtn;

	@FindBy(xpath = ".//*[@id='divCopyClipBoard_Hyperlink']/div/div/div[3]/input[4]")
	WebElement closebtnoflinkfile;
	@FindBy(xpath = ".//*[@id='btnSettingsSave']")
	WebElement savebtnforaccountsetting;

	/**
	 * Method to close the linked page
	 * 
	 */
	public void closeLinkFileWindow() {
		SkySiteUtils.waitForElement(driver, closebtnoflinkfile, 50);
		closebtnoflinkfile.click();
		Log.message("Close button is clicked");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		Log.message("Waiting for Settings button to display");
		SkySiteUtils.waitForElement(driver, Settingsbtn, 10);
		Log.message("Settings button is displayed");
		Settingsbtn.click();
		Log.message("Clicked on Settings button");
		SkySiteUtils.waitTill(3000);
		AccountSettingsbtn.click();
		Log.message("Clicked on Account Settings button from list");
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		WebElement expirydatebox = driver.findElement(By.xpath(".//*[@id='txtExpiryPeriod']"));
		SkySiteUtils.waitForElement(driver, expirydatebox, 60);
		expirydatebox.clear();
		Log.message("expiry date has been cleared");
		SkySiteUtils.waitTill(2000);
		savesettingbtn.click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitTill(4000);

	}

	@FindBy(xpath = ".//*[@id='divFolderShareLink']/div/div/div[3]/input[4]")
	WebElement closebtnforfolderhyperlink;

	
	/**
	 * Method to close the folder link window
	 * 
	 */
	public void closeLinkFolderWindow() {
		SkySiteUtils.waitForElement(driver, closebtnforfolderhyperlink, 50);
		closebtnforfolderhyperlink.click();
		Log.message("Close button is clicked");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		Log.message("Waiting for Settings button to display");
		SkySiteUtils.waitForElement(driver, Settingsbtn, 10);
		Log.message("Settings button is displayed");
		Settingsbtn.click();
		Log.message("Clicked on Settings button");
		SkySiteUtils.waitTill(3000);
		AccountSettingsbtn.click();
		Log.message("Clicked on Account Settings button from list");
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		WebElement expirydatebox = driver.findElement(By.xpath(".//*[@id='txtExpiryPeriod']"));
		SkySiteUtils.waitForElement(driver, expirydatebox, 60);
		expirydatebox.clear();
		Log.message("expiry date has been cleared");
		SkySiteUtils.waitForElement(driver, savesettingbtn, 40);
		savesettingbtn.click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitTill(4000);
	}

	@FindBy(xpath = "//*[@id='Collections']/a")

	WebElement clkcollection;
	@FindBy(xpath = "//*[@id='btnResetSearch']")

	WebElement resetcollection;
	@FindBy(xpath = "//*[@id='setting']")

	WebElement drpSetting;
	@FindBy(css = ".selectedTreeRow")

	WebElement selectedTreeRow;

	/**
	 * Method written for select collection Scripted By: Sekhar
	 * 
	 * @return
	 */
	public boolean selectcollection(String Collection_Name) {
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, drpSetting, 60);
		SkySiteUtils.waitTill(3000);
		clkcollection.click();// click on collection Selection
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		resetcollection.click();// click on reset button
		Log.message("reset button has been clicked.");
		SkySiteUtils.waitTill(5000);
		int prjCount_prjList = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img"))
				.size();
		Log.message("prjCount_prjList :" + prjCount_prjList);
		// Loop start '2' as projects list start from tr[2]

		int prj = 0;
		for (prj = 1; prj <= prjCount_prjList; prj++) {
			// x path as dynamically providing all rows(prj) tr[prj]
			String prjName = driver
					.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + prj + "]"))
					.getText();
			Log.message(prjName);
			SkySiteUtils.waitTill(2000);
			// Checking project name equal with each row of table
			if (prjName.equals(Collection_Name)) {
				Log.message("Select Collection button has been clicked!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		// Click on project name where it equal with specified project name
		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + prj + "]")).click();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, selectedTreeRow, 60);
		SkySiteUtils.waitTill(5000);
		String Collection = selectedTreeRow.getText();
		Log.message("Collection name is:" + Collection);
		SkySiteUtils.waitTill(2000);
		if (Collection.contains(Collection_Name)) {
			Log.message("Select Collection successfull");
			SkySiteUtils.waitTill(2000);
			return true;
		} else {
			Log.message("Select Collection Unsuccessfull");
			SkySiteUtils.waitTill(2000);
			return false;
		}

	}

	/**
	 * Folder to set link expiration (in Weeks) for folder link
	 * 
	 */
	public void setExpirationInDaysForFolderLink(String noofweeks) {
		driver.switchTo().defaultContent();

		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		SkySiteUtils.waitForElement(driver, expiryperiod, 50);
		// String no="5";
		expiryperiod.click();
		Log.message("expiry period box clicked");
		expiryperiod.clear();
		Log.message("expiry period box cleared");

		expiryperiod.sendKeys(noofweeks);
		Log.message("expiry period has been set to " + noofweeks);
		SkySiteUtils.waitTill(2000);
		dropdown.click();
		Log.message("Drop down is clicked");
		SkySiteUtils.waitTill(3000);
		weekoption.click();
		Log.message("week option is selected");
		SkySiteUtils.waitForElement(driver, savesettingbtn, 40);
		savesettingbtn.click();
		Log.message("save button is clicked");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", collectiontab);
		// collectiontab.click();
		Log.message("Collection tab is clicked");

	}

	@FindBy(xpath = ".//*[@id='navBtnLink']/div/ul/li[5]/a")
	WebElement folderlinkoption;

	/**
	 * Open file link option and verify the set expiration date from settings
	 * 
	 */
	public boolean openLinkFolderOptionVerifySetWeekPeriod(String noofweek) {
		int outputtotaldate=0;
		// Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		// driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		// Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='ParentFolder']")).click();
		Log.message("Folder is selected");

		WebElement moreoption = driver.findElement(By.xpath(".//*[@id='lftpnlMore']"));
		SkySiteUtils.waitForElement(driver, moreoption, 50);
		moreoption.click();
		Log.message("more option clicked");
		SkySiteUtils.waitForElement(driver, folderlinkoption, 50);
		folderlinkoption.click();
		Log.message("Folder link option is clicked");
		SkySiteUtils.waitTill(5000);

		// String date1=expirationdate.getAttribute("value");
		String date1 = driver.findElement(By.xpath("//*[@title='Set expiration date']")).getAttribute("value");
		Log.message("The date is " + date1);
		Log.message("split(String regex, int limit) with limit=3:");
		String array2[] = date1.split("/", 3);
		for (String temp : array2) {
			Log.message(temp);
		}
		Date d = new Date();
		Log.message("the current date is " + d.toString());
		String currenttimearray[] = d.toString().split(" ");
		for (String cdata : currenttimearray) {
			Log.message(cdata);
		}
		// as week is 1
		String noofdays = "7";
		int inttime1 = Integer.parseInt(currenttimearray[2]);
		String month=currenttimearray[1];
		int noofdaysint = Integer.parseInt(noofdays);
		if(inttime1>24 && (month.equals("Jan") || month.equals("Mar") || month.equals("May") || month.equals("July") || month.equals("Aug") || month.equals("Oct") || month.equals("Dec")) ) {
			switch (inttime1) {
			case 25:
				outputtotaldate = 1 ;
				break;
			case 26:
				outputtotaldate = 2 ;
				break;
			case 27:
				outputtotaldate = 3;
				break;
			case 28:
				outputtotaldate = 4;
				break;
			case 29:
				outputtotaldate = 5;
				break;
			case 30:
				outputtotaldate = 6;
				break;
			case 31:
				outputtotaldate = 7;
				break;
			 default:
				Log.message("Invalid month");

			}
			}
			else if(inttime1>24 && (month.equals("April") || month.equals("Feb") || month.equals("Jun") || month.equals("Sep") || month.equals("Aug") || month.equals("Nov") || month.equals("Dec")))  {
				switch (inttime1) {
				case 25:
					outputtotaldate = 2 ;
					break;
				case 26:
					outputtotaldate = 3;
					break;
				case 27:
					outputtotaldate = 4;
					break;
				case 28:
					outputtotaldate = 5;
					break;
				case 29:
					outputtotaldate = 6;
					break;
				case 30:
					outputtotaldate = 7;
					break;
				
				 default:
					Log.message("Invalid month");

				}
				
			}
			
			else {

	             outputtotaldate = inttime1 + noofdaysint;
			}
		
		Log.message("Total expected date for expiry is " + outputtotaldate);
		int expirydaypresent = Integer.parseInt(array2[1]);
		Log.message("Expiry date is " + expirydaypresent);

		if (outputtotaldate == expirydaypresent) {

			Log.message("expiry date is reflected successfully");
			return true;
		}
		return false;
	}

	@FindBy(xpath = ".//*[@id='Div2']/div[1]/div[4]/div/div[2]/div/div/ul/li[2]/a")
	WebElement monthoption;

	/**
	 * Folder to set link expiration (in Weeks) for folder link
	 * 
	 */
	public void setExpirationInMonthsForShareAlbum(String noofmonths) {
		driver.switchTo().defaultContent();

		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		SkySiteUtils.waitForElement(driver, expiryperiod, 50);
		// String no="5";
		expiryperiod.click();
		Log.message("expiry period box clicked");
		expiryperiod.clear();
		Log.message("expiry period box cleared");

		expiryperiod.sendKeys(noofmonths);
		Log.message("expiry period has been set to " + noofmonths);
		SkySiteUtils.waitTill(2000);
		dropdown.click();
		Log.message("Drop down is clicked");
		SkySiteUtils.waitTill(3000);
		monthoption.click();
		Log.message("month option is selected");
		SkySiteUtils.waitForElement(driver, savesettingbtn, 40);
		savesettingbtn.click();
		Log.message("save button is clicked");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", collectiontab);
		// collectiontab.click();
		Log.message("Collection tab is clicked");

	}

	@FindBy(xpath = ".//*[@id='Div1']/nav/div[2]/div/button")
	WebElement moreoptionforalbum;
	/***
	 * Method to navigate to album tab
	 * 
	 */
	@FindBy(css = "#ProjectMenu1_Album>a")
	@CacheLookup
	WebElement albumicon;

	public void clickAlbumIcon() {
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		albumicon.click();
		Log.message("Album icon is clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		driver.findElement(By.xpath(
				".//*[@id='treeboxbox_tree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span"))
				.click();
		Log.message("Album is selected");
		SkySiteUtils.waitForElement(driver, moreoptionforalbum, 50);
		moreoptionforalbum.click();
		Log.message("more option clicked");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='Div1']/nav/div[2]/div/ul/li[4]/a")).click();
		Log.message("Share album option is clicked");

	}

	/**
	 * Open share album option and verify the set expiration date from settings
	 * 
	 */
	public boolean openShareAlbumOptionVerifySetMonthPeriod(String noofmonth) 
	{
		
		int outputtotaldate =0;
		int currentmonth = 0;
	
		SkySiteUtils.waitTill(10000);
		String date1 = driver.findElement(By.xpath(".//*[@id='txtExpiryDate_hyperlink']")).getAttribute("value");
		Log.message("The date is " + date1);
		
		// getting the date present in application
		Log.message("split(String regex, int limit) with limit=3:");
		String array2[] = date1.split("/", 3);
		for (String temp : array2) 
		{
			Log.message(temp);
		}
		
		// getting current time
		Date d = new Date();
		Log.message("the current date is " + d.toString());
		String currenttimearray[] = d.toString().split(" ");
		for (String cdata : currenttimearray) 
		{
			Log.message(cdata);
		}
		
		// month is 3

		String month = currenttimearray[1];
		switch (month) 
		{
			case "Jan":
				currentmonth = 1;
			break;
			
			case "Feb":
				currentmonth = 2;
			break;
			
			case "Mar":
				currentmonth = 3;
			break;
			
			case "Apr":
				currentmonth = 4;
			break;
			
			case "May":
				currentmonth = 5;
			break;
			
			case "Jun":
				currentmonth = 6;
			break;
			
			case "Jul":
				currentmonth = 7;
			break;
			
			case "Aug":
				currentmonth = 8;
			break;
			
			case "Sep":
				currentmonth = 9;
			break;

			case "Oct":
				currentmonth = 10;
			break;
			
			case "Nov":
				currentmonth = 11;
			break;
			
			case "Dec":
				currentmonth = 12;
			break;

			default:
			Log.message("Invalid month");

		}

		int noofmonthssint = Integer.parseInt(noofmonth);
		Log.message("No of months entered in settings " + noofmonthssint);
		if(currentmonth==10) 
		{
			currentmonth=1;
			outputtotaldate = currentmonth + 0;
			Log.message("Total expected date to be present is" + outputtotaldate);
			
		}
		
		else if(currentmonth==11) 
		{
			currentmonth=2;
			outputtotaldate = currentmonth + 0;
			Log.message("Total expected date to be present is" + outputtotaldate);
		
		}
	   	
		else if(currentmonth==12) 
		{
			currentmonth=3;
			outputtotaldate = currentmonth + 0;
			Log.message("Total expected date to be present is" + outputtotaldate);
		}
		  
		else 
		{
			 outputtotaldate = currentmonth + noofmonthssint;
		     Log.message("Total expected date to be present is " + outputtotaldate);
			   
	   }
	
		int expirydaypresent = Integer.parseInt(array2[0]);
		Log.message("Expiry date is " + expirydaypresent);

		if (outputtotaldate == expirydaypresent) 
		{

			Log.message("expiry date is reflected successfully");
			return true;
		}
		
		return false;
		
	}

	
	
	/**
	 * method to close ShareAlbum Windo
	 * 
	 */
	@FindBy(xpath = ".//*[@id='divAlbumShareLink']/div/div/div[3]/button[4]")
	WebElement closebtnforsharealbum;

	public void closeShareAlbumWindow() {
		SkySiteUtils.waitForElement(driver, closebtnforsharealbum, 50);
		closebtnforsharealbum.click();
		Log.message("Close button is clicked");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		Log.message("Waiting for Settings button to display");
		SkySiteUtils.waitForElement(driver, Settingsbtn, 10);
		Log.message("Settings button is displayed");
		Settingsbtn.click();
		Log.message("Clicked on Settings button");
		SkySiteUtils.waitTill(3000);
		AccountSettingsbtn.click();
		Log.message("Clicked on Account Settings button from list");
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		WebElement expirydatebox = driver.findElement(By.xpath(".//*[@id='txtExpiryPeriod']"));
		SkySiteUtils.waitForElement(driver, expirydatebox, 60);
		expirydatebox.clear();
		Log.message("expiry date has been cleared");
		savesettingbtn.click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitTill(4000);

	}

	@FindBy(xpath = ".//*[@id='Div2']/div[1]/div[4]/div/div[2]/div/div/ul/li[1]/a")
	WebElement yearoption;
	@FindBy(xpath = ".//*[@id='ProjectFiles']/a")
	WebElement documenttab2;
	@FindBy(xpath = ".//*[@id='ProjectMenu1_ancFileMenuDownIcon']")
	WebElement dropdownfile;
	@FindBy(xpath = ".//*[@id='ProjectMenu1_liPackages']/a")
	WebElement packageoption;

	/**
	 * Folder to set link expiration (in Years) for package link
	 * 
	 */
	public void setExpirationInYearForPackage(String noofyears) {
		driver.switchTo().defaultContent();

		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		SkySiteUtils.waitForElement(driver, expiryperiod, 50);
		// String no="5";
		expiryperiod.click();
		Log.message("expiry period box clicked");
		expiryperiod.clear();
		Log.message("expiry period box cleared");

		expiryperiod.sendKeys(noofyears);
		Log.message("expiry period has been set to " + noofyears);
		SkySiteUtils.waitForElement(driver, dropdown, 50);
		dropdown.click();
		Log.message("Drop down is clicked");
		SkySiteUtils.waitTill(3000);
		yearoption.click();
		Log.message("Year option is selected");
		SkySiteUtils.waitForElement(driver, savesettingbtn, 40);
		savesettingbtn.click();
		Log.message("save button is clicked");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", documenttab2);
		// collectiontab.click();
		Log.message("Document tab is clicked");
		SkySiteUtils.waitForElement(driver, dropdownfile, 50);
		dropdownfile.click();
		Log.message("Dropdown for file is clicked");
		SkySiteUtils.waitForElement(driver, packageoption, 60);
		packageoption.click();
		Log.message("package option is clicked");

	}

	/**
	 * Open share album option and verify the set expiration date from settings
	 * 
	 */
	public boolean openSharePackageOptionVerifySetMonthYear(String noofyear) {
		int currentmonth = 0;
		// Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		/*
		 * driver.findElement(By.xpath("//*[text()='ParentFolder']")).click();
		 * Log.message("Folder is selected");
		 * 
		 * WebElement moreoption=driver.findElement(By.xpath(".//*[@id='lftpnlMore']"));
		 * SkySiteUtils.waitForElement(driver, moreoption, 50); moreoption.click();
		 * Log.message("more option clicked"); SkySiteUtils.waitForElement(driver,
		 * folderlinkoption,50); folderlinkoption.click();
		 * Log.message("Folder link option is clicked");
		 */

		driver.findElement(By.xpath(".//*[@id='divGridPackagesList']/div[2]/table/tbody/tr[2]/td[1]/img")).click();
		Log.message("First element is selected");
		driver.findElement(By.xpath(".//*[@id='btnLinkPackage']")).click();
		Log.message("Link option is clicked");

		// String date1=expirationdate.getAttribute("value");
		String date1 = driver.findElement(By.xpath(".//*[@id='txtExpiryDate_hyperlink']")).getAttribute("value");
		Log.message("The date is " + date1);
		// getting the date present in application
		Log.message("split(String regex, int limit) with limit=3:");
		String array2[] = date1.split("/", 3);
		for (String temp : array2) {
			Log.message(temp);
		}
		// getting current time
		Date d = new Date();
		Log.message("the current date is " + d.toString());
		String currenttimearray[] = d.toString().split(" ");
		for (String cdata : currenttimearray) {
			Log.message(cdata);
		}
		// month is 3

		String currentyear = currenttimearray[5];
		int cyear = Integer.parseInt(currentyear);
		int noofyearsint = Integer.parseInt(noofyear);
		Log.message("No of years entered in settings " + noofyearsint);
		Log.message("Current year is " + cyear);
		int outputtotaldate = cyear + noofyearsint;
		Log.message("Total expected year for expiry is " + outputtotaldate);
		int expirydaypresent = Integer.parseInt(array2[2]);
		Log.message("Expiry year present in link window is " + expirydaypresent);

		if (outputtotaldate == expirydaypresent) {

			Log.message("expiry year is reflected successfully");
			return true;
		}
		return false;
	}
    
	/**
	 * method to close package album
	 * 
	 */
	@FindBy(xpath = ".//*[@id='divCopyClipBoard_Hyperlink']/div/div/div[3]/button[4]")
	WebElement closebntforpackage;

	public void closepackagewindow() {
		SkySiteUtils.waitForElement(driver, closebntforpackage, 50);
		closebntforpackage.click();
		Log.message("Close button is clicked");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		Log.message("Waiting for Settings button to display");
		SkySiteUtils.waitForElement(driver, Settingsbtn, 10);
		Log.message("Settings button is displayed");
		Settingsbtn.click();
		Log.message("Clicked on Settings button");
		SkySiteUtils.waitTill(3000);
		AccountSettingsbtn.click();
		Log.message("Clicked on Account Settings button from list");
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		WebElement expirydatebox = driver.findElement(By.xpath(".//*[@id='txtExpiryPeriod']"));
		SkySiteUtils.waitForElement(driver, expirydatebox, 60);
		expirydatebox.clear();
		Log.message("expiry date has been cleared");
		SkySiteUtils.waitForElement(driver, savesettingbtn, 50);
		savesettingbtn.click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitTill(4000);

	}
	//@FindBy(xpath=".//*[@id='password-settings']/div[3]/div[1]/div/div/div/button")
	@FindBy(xpath=".//*[@id='password-settings']/div/div[1]/div/div/div/button")
	WebElement pexpiredropdown;
    @FindBy(xpath=".//*[@id='password-settings']/div/div[1]/div/div/div/ul/li[3]/a")
    WebElement monthoptionforpexpire;

	/**Method to set passwordExpiry(){
	 * 
	 * 
	
	 */
	public void setPasswordExpiry() {
		driver.switchTo().defaultContent();
		Log.message("Switching to default content");
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitForElement(driver, pexpiredropdown,50);
		pexpiredropdown.click();
		Log.message("dropdown is clicked");
		SkySiteUtils.waitForElement(driver, monthoptionforpexpire,50);
		monthoptionforpexpire.click();
		Log.message("90 day option is selected");
		SkySiteUtils.waitForElement(driver, savesettingbtn, 40);
		savesettingbtnpw.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
}
	 @FindBy(xpath=".//*[@id='password-settings']/div/div[1]/div/div/div/ul/li[1]/a")
	    WebElement neverexpirreoption;
	/**Verify set passwod expiry
	 * 
	 */
	public boolean verifySetExpiry() {
		SkySiteUtils.waitTill(5000);
		String passowrdexpirypresent=driver.findElement(By.xpath("//*[@placeholder='Never expires']")).getAttribute("value");
		Log.message("the expiry present is " +passowrdexpirypresent);
		if(passowrdexpirypresent.equals("90 days")) {
			
			  Log.message("Password expiry set successfull");
			  SkySiteUtils.waitTill(6000);
                 return true;
		}    
                	 return false;
		
		}
	/**Set to default password expiry
	 * 
	 */
	public void setDefaultExpiry() {
	
		driver.switchTo().defaultContent();
		Log.message("Switching to default content");
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		SkySiteUtils.waitTill(2000);
	//	driver.findElement(By.xpath(".//*[@id='aspnetForm']/div[8]/div/ul/li[2]/a")).click();
		//Log.message("Sign in button is clicked");
		this.selectPasswordSettingtab();
	      
		SkySiteUtils.waitForElement(driver, pexpiredropdown,50);
		pexpiredropdown.click();
		Log.message("dropdown is clicked");
		SkySiteUtils.waitForElement(driver, neverexpirreoption,50);
		neverexpirreoption.click();
		Log.message("never expire option is selected");
		SkySiteUtils.waitForElement(driver, savesettingbtn, 40);
		savesettingbtnpw.click();
		Log.message("save button is clicked");
		
}
}
