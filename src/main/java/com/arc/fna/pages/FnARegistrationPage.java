package com.arc.fna.pages;


import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.RandomEmployeeName;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arc.fna.utils.randomSharedName;
import com.arcautoframe.utils.Log;

public class FnARegistrationPage  extends LoadableComponent<FnARegistrationPage> {

	WebDriver driver;
	private boolean isPageLoaded;
	
	/**
	 * Identifying web elements using FindBy annotation.
	 */
	

	@FindBy(css="#txtFirstName")
	@CacheLookup
	WebElement FirstnameField;

	
	@FindBy(css="#txtLastName")
	@CacheLookup
	WebElement LastnameField;
	
	
	@FindBy(css="#txtEmail")
	@CacheLookup
	WebElement EmailField;
	
	
	@FindBy(css="#txtASState")
	@CacheLookup
	WebElement Statefield;
	
	
	@FindBy(css="#txtASPhone")
	@CacheLookup
	WebElement Phonenumber;
	
	@FindBy(css="#txtPassword")
	@CacheLookup
	WebElement Passwordfield;
	
	@FindBy(css="#txtReferralCode")
	@CacheLookup
	WebElement Referralcodefield;
	
	@FindBy(css="#aSubmit")
	@CacheLookup
	WebElement CreateAccountButton;
	
	@FindBy(css="#ddlASStateDrop")
	@CacheLookup
	WebElement Dropdownbutton;
	
	@FindBy(css="#ddlASState > li:nth-child(1) > a:nth-child(1)")
	@CacheLookup
	WebElement SelectAlabamalocation;
	
	@FindBy(css="div.form-group:nth-child(9) > a:nth-child(1)")
	@CacheLookup
	WebElement Registrationbtn;
	
	
	
	
	@Override
	protected void load() {
		isPageLoaded = true;
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		// TODO Auto-generated method stub
		
	}
	
	public FnARegistrationPage(WebDriver driver) {
		   this.driver = driver;
		   PageFactory.initElements(this.driver, this);
		 }
	
	
	/** 
	 * Method written for checking whether create account button is present?
	 * @return
	 * Created By trinanjwan
	 */
	public boolean presenceOfCreateAccountButton()
	{
		SkySiteUtils.waitForElement(driver, CreateAccountButton, 10);
		Log.message("The Create Account button to be appeared");
		if(CreateAccountButton.isDisplayed())
			return true;
			else
			return false;
	}
	
	
	/** 
	 * Method written for the registration with the valid credentials
	 * @return
	 * Created By trinanjwan
	 */
	
	
	public ProjectsMyProfPage RegistrationWithValidCredential(String Emailnew) throws AWTException
	
	{
			
		    Registrationbtn.click();
		    Log.message("Clicked on Register button");
		    SkySiteUtils.waitTill(5000);
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ESCAPE);
			String FName = PropertyReader.getProperty("FirstName");
			FirstnameField.clear();
			FirstnameField.sendKeys(FName);
			Log.message("First Name: " + FName + " " + "has been entered in FIRST NAME text box." );
			
			String LName = PropertyReader.getProperty("LastName");
			LastnameField.clear();
			LastnameField.sendKeys(LName);
			Log.message("Last Name: " + LName + " " + "has been entered in LAST NAME text box." );
			
			EmailField.clear();
			EmailField.sendKeys(Emailnew);
			Log.message("Email ID: " + Emailnew + " " + "has been entered in EMAIL text box." );
			
		
			String State = PropertyReader.getProperty("Statenamereg");
			Statefield.sendKeys(State);
			Log.message("State: " + State + " " + "has been entered in State text box." );
			
	
			SkySiteUtils.waitTill(5000);
			
			
			String Ph = PropertyReader.getProperty("PhoneNumber");
			Phonenumber.clear();
			Phonenumber.sendKeys(Ph);
			Log.message("Phone Number " + Ph + " " + "has been entered in PHONE NUMBER text box." );
				
			
			String Pword = PropertyReader.getProperty("Passwordreg");
			Passwordfield.sendKeys(Pword);
			Log.message("Password: " + Pword + " " + "has been entered in PASSWORD text box." );
			
			
			CreateAccountButton.click();
			Log.message("CREATE ACCOUNT button clicked" );
			return new ProjectsMyProfPage(driver).get();
				
			
}
	 
		/** Method to create Random Email for registration
		 * By Trinanjwan
		 * Modified by Trinanjwan | 20-Nov-2018
		 */
	
	
	
	 public String Random_Email()
	    {
	           
	           String str="Emailtest";
	           String str2="@yopmail.com";
	           Random r = new Random( System.currentTimeMillis() );
	           Random r1= new Random( System.currentTimeMillis());
	           String abc = str+String.valueOf((10000 + r.nextInt(20000)))+String.valueOf((20000 + r1.nextInt(30000)))+str2;
	           return abc;
	           
	    }

}
	