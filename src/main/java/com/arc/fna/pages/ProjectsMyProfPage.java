package com.arc.fna.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;

public class ProjectsMyProfPage  extends LoadableComponent<ProjectsMyProfPage> {

	WebDriver driver;
	private boolean isPageLoaded;
	
	/**
	 * Identifying web elements using FindBy annotation.
	 */
	

	
	@FindBy(css="#txtASFirstName")
	@CacheLookup
	WebElement FirstName;
	
	
	@FindBy(css="a.btn-primary:nth-child(2) > span:nth-child(2)")
	@CacheLookup
	WebElement sendfilebtn;
	
	
	@FindBy(css=".icon-module-change")
	@CacheLookup
	WebElement ModuleNavigation;
	
	@FindBy(css=".select-module > li:nth-child(1) > a:nth-child(1)")
	@CacheLookup
	WebElement Homebtn;
	
	@FindBy(css=".select-module > li:nth-child(2) > a:nth-child(1)")
	@CacheLookup
	WebElement FnAbtn;
	
	
	@FindBy(css="#txtASEmail")
	@CacheLookup
	WebElement Emailfield;
	
	@FindBy(css="#txtASCompany")
	@CacheLookup
	WebElement CompanyNamefield;
	
	@FindBy(css="#txtASPhone")
	@CacheLookup
	WebElement Phonenumberfield;
	
	
	@FindBy(css="#txtASCountry")
	@CacheLookup
	WebElement Countryfield;
	
	@FindBy(css="#txtASState")
	@CacheLookup
	WebElement Statefield;
	

	
	@Override
	protected void load() {
		isPageLoaded = true;
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		// TODO Auto-generated method stub
		
	}
	
	public ProjectsMyProfPage(WebDriver driver) {
		   this.driver = driver;
		   PageFactory.initElements(this.driver, this);
		 }

	/** 
	 * Method written for checking whether projects my profile page is landed properly
	 * @return
	 * Created By trinanjwan
	 */
	
	
	
	

	public boolean presenceOfPrjDashboardButton()
	{
		
		SkySiteUtils.waitTill(20000);
	
		if(sendfilebtn.isDisplayed())
		{	
			Log.message("Button is displayed");
			return true;
		}
			else
			return false;
	}
	

	/** 
	 * Method written for navigating to the common login page
	 * @return
	 * Created By trinanjwan
	 */


	public CommonLoginPage navigatingToCommonLoginPage()
	
	{
		SkySiteUtils.waitTill(20000);

		SkySiteUtils.waitForElement(driver, ModuleNavigation, 10);
		ModuleNavigation.click();
		Log.message("Clicked on module navigation button");
		SkySiteUtils.waitTill(5000);
		Homebtn.click();
		Log.message("Clicked on HOME button");
		return new CommonLoginPage(driver).get();
	
		
	}
	
	
	/** Method to Verify changing of Email ID getting changed in PROJECTS
     * Scripted By :Trinanjwan
		*
		*/
	
	public boolean verificationOfChangedEmail(String RandomEmail)
	
	{
		SkySiteUtils.waitTill(10000);
		String actualEmail=Emailfield.getAttribute("value");
		Log.message("The actual email is:"+actualEmail);
		Log.message("The expected email is:"+RandomEmail);
		if(actualEmail.contentEquals(RandomEmail))
			return true;
		else
			return false;
	}
	
	/** Method to Verify changing of Company Name getting changed in PROJECTS
     * Scripted By :Trinanjwan
		*
		*/
	
	public boolean verificationOfChangedcompany(String RandomCompany)
	
	{
		SkySiteUtils.waitTill(10000);
		String actualcomp=CompanyNamefield.getAttribute("value");
		Log.message("The actual company is:"+actualcomp);
		Log.message("The expected company is:"+RandomCompany);
		if(actualcomp.contentEquals(RandomCompany))
			return true;
		else
			return false;
	}
	
	
	/** Method to Verify changing of Phone number getting changed in PROJECTS
     * Scripted By :Trinanjwan
		*
		*/
	
	public boolean verificationOfChangedphonenumber()
	
	{
		SkySiteUtils.waitTill(10000);
		String actualph=Phonenumberfield.getAttribute("value");
		Log.message("The actual ph no. is:"+actualph);
		String newph=PropertyReader.getProperty("Phonenumbchange");
		Log.message("The expected company is:"+newph);
		if(actualph.contentEquals(newph))
			return true;
		else
			return false;
	}
	
	/** Method to navigate to FnA Homepage from PROJECTS module
     * Scripted By :Trinanjwan
		*
		*/
	
	public FnaHomePage navigatingToFnAHome()
	
	{
	
		SkySiteUtils.waitTill(20000);

		SkySiteUtils.waitForElement(driver, ModuleNavigation, 10);
		ModuleNavigation.click();
		Log.message("Clicked on module navigation button");
		SkySiteUtils.waitTill(5000);
		FnAbtn.click();
		Log.message("Clicked on F&A button to navigate to F&A module");
		return new FnaHomePage(driver).get();
		
		
		
	}

	

	/** Method to Verify changing of Country and State getting changed in PROJECTS
	 * Scripted By :Trinanjwan
	*
	*/

	public boolean verificationOfChangedStateandCountry()

	{
		
		SkySiteUtils.waitForElement(driver, FirstName, 20);
		Log.message("Page landed successfully");
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,5000)");
		Log.message("Scroll down below");
		
		String actualcountry=Countryfield.getAttribute("value");
        Log.message("Actual Country is "+actualcountry);

         
        String actualstate=Statefield.getAttribute("value");
        Log.message("Actual State is "+actualstate);
		
        String expectedcountry=PropertyReader.getProperty("CountryChange");
        Log.message("Expected Country is:"+expectedcountry);
        
        String expectedstate=PropertyReader.getProperty("StateChange");
        Log.message("Expected State is:"+expectedstate);
		
		if(expectedcountry.contentEquals(actualcountry)&&expectedstate.contentEquals(actualstate))
			return true;
		else
			return false;
	}
	
	
	
	
	
	/**
	 * Method for handling the new feature pop over window Scripted Trinanjwan
	 */

	public void handlingNewFeaturepopover() {
		SkySiteUtils.waitTill(6000);
		Log.message("Pop over is not present");
		/**	
		SkySiteUtils.waitTill(6000);
		int Feedback_Alert = driver
				.findElements(By.cssSelector(
						".btnWhatsNewFeatureAccepted"))
				.size();
		Log.message("Checking feedback alert is there or not : " + Feedback_Alert);
		SkySiteUtils.waitTill(2000);
		if (Feedback_Alert > 0) {
			driver.findElement(By.cssSelector(
					".btnWhatsNewFeatureAccepted")).click();
			Log.message("Clicked on 'Got it' link!!!");
		}
	 */
	
	}

	
	
	
	
	/**
	 * Method for handling sample video pop over window for SKYSITE Projects | Scripted Trinanjwan
	 */

	public void handlingsamplevideoPROJECTS() {
		

		SkySiteUtils.waitTill(30000);
		int Feedback_Alert = driver
				.findElements(By.cssSelector("#closeWelcomeVideo"))
				.size();
		Log.message("Checking video pop over is there or not : " + Feedback_Alert);
		SkySiteUtils.waitTill(10000);
		if (Feedback_Alert > 0) {
			driver.findElement(By.cssSelector("#closeWelcomeVideo")).click();
			Log.message("Clicked on 'X' button!!!");
		}

	
	}
	
	/**
	 * Method written to verify the check/un-check status of Complex Password and 2 step authentication for SKYSITE Projects | Scripted Trinanjwan
	 */
	
	@FindBy(css="img.img-circle")
	@CacheLookup
	WebElement Profileicon;
	
	@FindBy(css=".sldmenu-Settings > a:nth-child(1)")
	@CacheLookup
	WebElement Settingsmenu;
	
	@FindBy(css="#account > div:nth-child(1) > div:nth-child(1) > h3:nth-child(1)")
	@CacheLookup
	WebElement AccountInfosection;
	
	@FindBy(xpath="//a[text()='Password Settings']")
	@CacheLookup
	WebElement AccountSettingsTAB;

	

	public boolean verifyCheckUncheckofComplexPassword()
	
	{
		
		SkySiteUtils.waitForElement(driver, Profileicon, 40);
		Log.message("Profile icon is visible now");
		Profileicon.click();
		Log.message("Clicked on the Profile icon");
		SkySiteUtils.waitForElement(driver, Settingsmenu, 40);
		Log.message("Setting option is visible now");
		Settingsmenu.click();
		Log.message("Clicked on the Settings option");
		SkySiteUtils.waitForElement(driver, AccountInfosection, 40);
		Log.message("Landed in the Account info section");
		SkySiteUtils.waitForElement(driver, AccountSettingsTAB, 40);
		Log.message("Account Settings TAB is displayed now");
		Actions action1=new Actions(driver);
		action1.moveToElement(AccountSettingsTAB).click(AccountSettingsTAB).build().perform();
		
	//	AccountSettingsTAB.click();
		Log.message("Clicked on Account Settings TAB to navigate to settings section");
		SkySiteUtils.waitTill(10000);
		
		WebElement isChecked1;
		WebElement isChecked2;
		
		isChecked1 = driver.findElement(By.cssSelector("div.form-group:nth-child(2) > div:nth-child(1) > div:nth-child(1) > label:nth-child(1) > input:nth-child(1)"));
		isChecked2 = driver.findElement(By.cssSelector("div.checkbox:nth-child(2) > label:nth-child(1) > input:nth-child(1)"));
		if (isChecked1.isSelected() && isChecked2.isSelected() )
			return true;
		else
			return false;
			
	}

	
	/**
	 * Method written to enable 2 way authentication in PROJECTS module | Scripted Trinanjwan
	 */
	
	
	@FindBy(css="div.checkbox:nth-child(2) > label:nth-child(1) > input:nth-child(1)")
	@CacheLookup
	WebElement TwoStepCheckbox;
	
	
	@FindBy(css=".stnd-sign-in-container > div:nth-child(2) > div:nth-child(1) > button:nth-child(1)")
	@CacheLookup
	WebElement Savebutton;
	
	
	@FindBy(css="#button-0")
	@CacheLookup
	WebElement ResetPasswordbutton;
	
	//Element added to click on profile icon
	
	@FindBy(xpath="//span[text()='Profile']")
	@CacheLookup
	WebElement profileiconimg;
	
	
	@FindBy(xpath="//a[text()='Password Settings']")
	@CacheLookup
	WebElement PasswordsettingsTAB;
	
	
	
	
	
	
	public FnaAccountSettingsPage enablingtwowayinPROJECTS()
	
	{
		
		SkySiteUtils.waitForElement(driver, Profileicon, 25);
		Log.message("Profile icon is visible now");
		profileiconimg.click();
		Log.message("Clicked on the Profile icon");
		SkySiteUtils.waitForElement(driver, Settingsmenu, 20);
		Log.message("Setting option is visible now");
		Settingsmenu.click();
		Log.message("Clicked on the Settings option");
		SkySiteUtils.waitForElement(driver, AccountInfosection, 20);
		Log.message("Landed in the Account info section");
		SkySiteUtils.waitForElement(driver, PasswordsettingsTAB, 20);
		Log.message("Password Settings TAB is displayed now");
		Actions action1=new Actions(driver);
		action1.moveToElement(PasswordsettingsTAB).click(PasswordsettingsTAB).build().perform();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, TwoStepCheckbox, 15);
		Log.message("The 2 step enabling check box is displayed now");
		TwoStepCheckbox.click();
		Log.message("Clicked on checkbox to enable 2 step authentication");
		SkySiteUtils.waitForElement(driver, Savebutton, 15);
		Savebutton.click();
		Log.message("Clicked on Save button");
		SkySiteUtils.waitForElement(driver, ResetPasswordbutton, 15);
		Log.message("Reset password button is displayed now");
		ResetPasswordbutton.click();
		Log.message("Clicked on Reset Password button");
		return new FnaAccountSettingsPage(driver).get();
	
	
	
	}
	
	
	/**
	 * Method written to verify security questions/answers in PROJECTS module | Scripted Trinanjwan
	 */
	
	
	@FindBy(css="img.img-circle")
	@CacheLookup
	WebElement PrjMyProfile;


	@FindBy(css=".sldmenu-Myprofile > a:nth-child(1)")
	@CacheLookup
	WebElement PrjMyProfilenav;
	
	@FindBy(xpath="//input[@id='txtSecurityAnswer1']")
	@CacheLookup
	WebElement Answer1;
	
	@FindBy(xpath="//input[@id='txtSecurityAnswer2']")
	@CacheLookup
	WebElement Answer2;
	
	@FindBy(xpath="//input[@id='txtSecurityAnswer3']")
	@CacheLookup
	WebElement Answer3;
	
	
	public boolean verifysecurityquestions()
	
	{
		
		SkySiteUtils.waitTill(20000);
        PrjMyProfile.click();
        Log.message("Clicked on My Profile icon");
        SkySiteUtils.waitTill(2000);
        PrjMyProfilenav.click();
        Log.message("Clicked on My Profile to navigate to PROJECTS");
        
        
        SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, FirstName, 20);
		Log.message("My profile page is landed successfully");
        
        String expectedanswer1=PropertyReader.getProperty("Answer1tri");
        SkySiteUtils.waitForElement(driver, Answer1, 20);       
        String actualanswer1=Answer1.getAttribute("value");
        Log.message("Expected Answer1:"+expectedanswer1);
        Log.message("Actual Answer1:"+actualanswer1);
        
        String expectedanswer2=PropertyReader.getProperty("Answer2tri");
        SkySiteUtils.waitForElement(driver, Answer2, 20);
        String actualanswer2=Answer2.getAttribute("value");
        Log.message("Expected Answer2:"+expectedanswer2);
        Log.message("Actual Answer2:"+actualanswer2);
        
        	        
        String expectedanswer3=PropertyReader.getProperty("Answer3tri");
        SkySiteUtils.waitForElement(driver, Answer3, 20);
        String actualanswer3=Answer3.getAttribute("value");
        Log.message("Expected Answer3:"+expectedanswer3);
        Log.message("Actual Answer3:"+actualanswer3);
        
        if(expectedanswer1.contentEquals(actualanswer1) && expectedanswer2.contentEquals(actualanswer2) &&
        		expectedanswer3.contentEquals(actualanswer3))
        	return true;
        else
        	return false;
        
        
	}
	
	
	
	@FindBy(xpath="//a[text()='Trial expires in 13 days. ']")
	@CacheLookup
	WebElement trialsection;
	
	
	
	/**
	 * Method written to verify trial period of PROJECTS | Scripted Trinanjwan
	 */
	
	public boolean verificationoftrial()
	
	{
		
		SkySiteUtils.waitForElement(driver, trialsection, 40);
		String actual=trialsection.getText();
		Log.message("The actual trial is:"+actual);
		String expected="Trial expires in 13 days. Buy now";
		Log.message("The expected is:"+expected);
		if(actual.contentEquals(expected))
			return true;
		else
			return false;
				
	}	
	
	}	
	
	
	