package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arc.fna.utils.randomSharedName;
import com.arcautoframe.utils.Log;

public class FNASanity_USA_Page extends LoadableComponent<FNASanity_USA_Page>
{

	WebDriver driver;
	private boolean isPageLoaded;
	
	FnaHomePage fnaHomePage;
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	@FindBy(xpath = "//*[@id='setting']")
	WebElement btnSetting;

	@Override
	protected void load()
	{
		isPageLoaded = true;
		//SkySiteUtils.waitForElement(driver, btnSetting, 60);
	}

	@Override
	protected void isLoaded() throws Error 
	{
		if (!isPageLoaded) 
		{
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public FNASanity_USA_Page(WebDriver driver) 
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	@FindBy(xpath = "//*[@id='setting']")
	WebElement drpSetting;
	
	@FindBy(xpath = "//*[@id='Collections']/a")
	WebElement clkcollection;

	@FindBy(xpath = "//*[@id='btnResetSearch']")
	WebElement resetcollection;
	
	/**
	 * Method written for select collection
	 *  Scripted By: Sekhar
	 * 
	 * @return
	 */
	public boolean selectcollection(String Collection_Name) 
	{
		SkySiteUtils.waitTill(5000);
		//driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, drpSetting, 60);
		SkySiteUtils.waitTill(3000);
		clkcollection.click();// click on collection Selection
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		resetcollection.click();// click on reset button
		Log.message("reset button has been clicked.");
		SkySiteUtils.waitTill(5000);
		int prjCount_prjList = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img"))
				.size();
		Log.message("prjCount_prjList :" + prjCount_prjList);
		// Loop start '2' as projects list start from tr[2]
		int prj = 0;
		for (prj = 1; prj <= prjCount_prjList; prj++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			String prjName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + prj + "]")).getText();
			Log.message(prjName);
			SkySiteUtils.waitTill(2000);
			// Checking project name equal with each row of table
			if (prjName.equals(Collection_Name)) 
			{
				Log.message("Select Collection button has been clicked!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		// Click on project name where it equal with specified project name
		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + prj + "]")).click();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, selectedTreeRow, 60);
		SkySiteUtils.waitTill(5000);
		String Collection = selectedTreeRow.getText();
		Log.message("Collection name is:" + Collection);
		SkySiteUtils.waitTill(2000);
		if (Collection.contains(Collection_Name)) 
		{
			Log.message("Select Collection successfull");
			SkySiteUtils.waitTill(2000);
			return true;
		} 
		else
		{
			Log.message("Select Collection Unsuccessfull");
			SkySiteUtils.waitTill(2000);
			return false;
		}

	}
	@FindBy(css = "#btnAddFolder")
	WebElement btnAddFolder;
	
	/**
	 * Method written for Adding folder Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */
	public boolean Adding_Folder(String FolderName) throws AWTException 
	{
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		// Switch to Frame
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Swich to I Frame Success!!");
		SkySiteUtils.waitTill(5000);
		btnAddFolder.click(); // Adding on New Folder
		Log.message("Add Folder has been clicked");
		SkySiteUtils.waitTill(5000);
		txtFolderName.sendKeys(FolderName);
		Log.message("Folder name has been entered");
		SkySiteUtils.waitTill(3000);
		btnNewFolderSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(5000);
		String Folder_name = slttreefolder.getText();
		Log.message("Act Folder Name is:" + Folder_name);
		if (Folder_name.contains(FolderName)) 
		{
			Log.message("Adding the folder validation successfull ");
			SkySiteUtils.waitTill(2000);
			return true;
		} 
		else 
		{
			Log.message("Adding the folder validation Unsuccessfull ");
			SkySiteUtils.waitTill(2000);
			return false;
		}
	}	

	/**
	 * Method written for Upload file Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean UploadFile(String FolderPath) throws AWTException, IOException
	{
		// boolean result1=true;
		SkySiteUtils.waitTill(3000);
		btnUploadFile.click(); // Adding on New Folder
		Log.message("upload button has been clicked");
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(10000);
		btnselctFile.click();
		Log.message("select files has been clicked");
		SkySiteUtils.waitTill(5000);
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		//String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));

		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();

		for (File file : files) 
		{
			if (file.isFile())
			{
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(5000);
			}
		}
		output.flush();
		output.close();

		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
		// Executing .exe autoIt file
		Runtime.getRuntime().exec(AutoItexe_Path + " " + FolderPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(30000);

		try 
		{
			File file = new File(tmpFileName);
			if (file.delete()) 
			{
				Log.message(file.getName() + " is deleted!");
			}
			else
			{
				Log.message("Delete operation is failed.");
			}
		}
		catch (Exception e)
		{
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath(".//*[@id='multipartUploadBtn']")).click();//modify by sekhar
		Log.message("Upload button has been clicked");
		SkySiteUtils.waitTill(10000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(3000);
		if (driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).isDisplayed())
			return true;
		else
			return false;
	}

	@FindBy(css = "#btnUploadFile")
	WebElement btnUploadFile;
	
	@FindBy(css = "#btnSelectFiles")
	WebElement btnselctFile;
	
	/**
	 * Method written for Multiple files upload 
	 * Scripted By: Sekhar
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean Multiple_UploadFile(String FolderPath) throws AWTException, IOException
	{		
		SkySiteUtils.waitTill(3000);
		//driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		//SkySiteUtils.waitTill(5000);
		btnUploadFile.click(); // Adding on New Folder
		Log.message("upload button has been clicked");
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		}
		SkySiteUtils.waitTill(15000);
		btnselctFile.click();
		Log.message("select files has been clicked");
		SkySiteUtils.waitTill(5000);
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		//String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));

		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();

		for (File file : files) 
		{
			if (file.isFile())
			{
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(5000);
			}
		}
		output.flush();
		output.close();

		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
		// Executing .exe autoIt file
		Runtime.getRuntime().exec(AutoItexe_Path + " " + FolderPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(30000);

		try 
		{
			File file = new File(tmpFileName);
			if (file.delete()) 
			{
				Log.message(file.getName() + " is deleted!");
			}
			else
			{
				Log.message("Delete operation is failed.");
			}
		}
		catch (Exception e)
		{
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(15000);		
		String BeforeUpload_Count = driver.findElement(By.xpath("//*[@id='lblFileCount']")).getText().toString();// Getting label counts
		Log.message("Before upload count is: " + BeforeUpload_Count);// Getting Lite user counts
		SkySiteUtils.waitTill(5000);
		String[] y = BeforeUpload_Count.split(" ");
		String AvlUpload_Count = y[0];
		int CountBefore = Integer.parseInt(AvlUpload_Count);
		Log.message("Avl before upload file count is: " + CountBefore);
		SkySiteUtils.waitTill(10000);		
		driver.findElement(By.xpath(".//*[@id='multipartUploadBtn']")).click();
		Log.message("Upload button has been clicked");
		SkySiteUtils.waitTill(15000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(2000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);				
		String After_pagiUpload_Count = driver.findElement(By.xpath(".//*[@id='divPaginationBelow']/div[2]/div/div")).getText().toString();// Getting label counts
		Log.message("After upload count is: " + After_pagiUpload_Count);// Getting Lite user counts
		SkySiteUtils.waitTill(3000);
		String[] x = After_pagiUpload_Count.split(" ");
		String AvlPagiUpload_Count = x[3];
		int PagiCount = Integer.parseInt(AvlPagiUpload_Count);
		Log.message("Avl upload file pagination count is: " + PagiCount);// Getting Lite user counts
		SkySiteUtils.waitTill(5000);		
		if(CountBefore==PagiCount)
			return true;
		else
			return false;
	}
	
	@FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")
	WebElement Filename;
	
	@FindBy(xpath = ".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[1]/i")
	WebElement icondownload;

	/**
	 * Method written for Download file from menu.
	 *  Scripted By: Sekhar
	 * @throws AWTException
	 */
	
	public boolean DownloadFile_FromMenu(String DownloadPath) throws AWTException
	{			
		SkySiteUtils.waitTill(5000);		
		// Calling delete files from download folder script
		driver.switchTo().defaultContent();
		fnaHomePage = new FnaHomePage(driver).get();
		fnaHomePage.Delete_Files_From_Folder(DownloadPath);
		SkySiteUtils.waitTill(5000);		
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); 
		SkySiteUtils.waitTill(5000);
		String filename=Filename.getText();
		Log.message("File Name is:"+filename);
		SkySiteUtils.waitTill(3000);
		icondownload.click();
		Log.message("Download icon button has been clicked");
		SkySiteUtils.waitTill(8000);
		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);

		if (browserName.contains("firefox")) 
		{
			// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();

			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}
		SkySiteUtils.waitTill(25000);
		// After checking whether download folder or not
		String ActualFoldername = null;

		File[] files = new File(DownloadPath).listFiles();

		for (File file : files)
		{
			if (file.isFile()) 
			{
				ActualFoldername = file.getName();// Getting Folder Name into a variable
				Log.message("Actual File name is:" + ActualFoldername);
				SkySiteUtils.waitTill(1000);
				Long ActualFolderSize = file.length();
				Log.message("Actual File size is:" + ActualFolderSize);
				SkySiteUtils.waitTill(5000);
				if (ActualFolderSize != 0) 
				{
					Log.message("Downloaded file is available in downloads!!!");
				} 
				else
				{
					Log.message("Downloaded file is NOT available in downloads!!!");
				}
			}
		}
		if (ActualFoldername.contains(filename))
			return true;
		else
			return false;
	}
	
	@FindBy(xpath = "(//i[@class='icon icon-view-files'])[2]")
	WebElement iconviewfiles;	

	@FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[3]/td[2]/a[3]")
	WebElement TIFFilename;
	/**
	 * Method written for ViewFile.
	 *  Scripted By: Sekhar
	 * @throws AWTException
	 */
	
	public boolean ViewFile_Filelevel() throws AWTException
	{	
		boolean result1=false;
		boolean result2=false;
		SkySiteUtils.waitTill(5000);		
		Filename.click();
		Log.message("pdf file button has been clicked");
		SkySiteUtils.waitTill(10000);
		String parentHandle = driver.getWindowHandle(); // Getting parent window
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(25000);
		String View_FileName = driver.findElement(By.xpath("//span[@class='revision-view-event']")).getText();
		Log.message("View File Name is:" + View_FileName);
		SkySiteUtils.waitTill(5000);
		if ((driver.findElement(By.xpath("//span[@class='revision-view-event']")).isDisplayed() 
				&& (driver.findElement(By.xpath("//*[@id='imageViewer']/div[2]/div[1]/div/div[2]/img[1]")).isDisplayed())))
		{
			result1=true;
			Log.message("PDF File View Successfully ");			
		} 
		else 
		{
			result1=false;
			Log.message("PDF File View UnSuccessfully ");			
		}
		driver.close();
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); 
		SkySiteUtils.waitTill(15000);
		TIFFilename.click();
		Log.message("TIF file button has been clicked");
		SkySiteUtils.waitTill(10000);
		String parentHandle1 = driver.getWindowHandle(); // Getting parent window
		Log.message(parentHandle1);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(30000);
		String View_FileName1 = driver.findElement(By.xpath("//span[@class='revision-view-event']")).getText();
		Log.message("View File Name is:" + View_FileName1);
		SkySiteUtils.waitTill(5000);
		if ((driver.findElement(By.xpath("//span[@class='revision-view-event']")).isDisplayed() && (driver
				.findElement(By.xpath("//*[@id='imageViewer']/div[2]/div[1]/div/div[2]/img[1]")).isDisplayed())))
		{
			result2=true;
			Log.message("TIF File View Successfully ");			
		} 
		else 
		{
			result2=false;
			Log.message("TIF File View UnSuccessfully ");			
		}
		if((result1=true)&&(result2=true))
			return true;
		else
			return false;			
	}
	
	/**
	 * Method written for select folder 
	 * Scripted By: Sekhar
	 * @return
	 * @throws AWTException
	 */
	public boolean Select_Folder(String FolderName) throws AWTException
	{
		boolean result1 = false;
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		// Counting available folders
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		driver.findElement(By.xpath("(//span[@class='standartTreeRow' and @style='padding-left: 5px; padding-right: 5px;'])[2]")).click();
		SkySiteUtils.waitTill(3000);
		int Folders_Count = 0;
		int i = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("(//span[@style='padding-left: 5px; padding-right: 5px;'])"));
		for (WebElement Element : allElements) 
		{
			Folders_Count = Folders_Count + 1;
		}
		Log.message("Folder Count: " + Folders_Count);
		// Getting Folder Names and machine
		for (i = 2; i <= Folders_Count; i++) 
		{
			String Exp_FolderName = driver.findElement(By.xpath("(//span[@style='padding-left: 5px; padding-right: 5px;'])[" + i + "]")).getText();
			Log.message("Exp Folder Name: " + Exp_FolderName);
			if (Exp_FolderName.contentEquals(FolderName))
			{
				result1 = true;
				Log.message("Exp Folder is Available");
				break;
			}
		}
		if (result1 == true)
		{
			// select the Folder
			driver.findElement(By.xpath("(//span[@style='padding-left: 5px; padding-right: 5px;'])[" + i + "]")).click();// Select Folder
			Log.message("selected Folder has been clicked ");
			SkySiteUtils.waitTill(10000);
		} 
		else
		{
			result1 = false;
			Log.message("Folder is Not Available.....");
		}
		// Final Condition
		if ((result1 == true))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Method written for Random Name 
	 * Scripted By: Sekhar
	 * @return
	 */

	public String Random_Title() 
	{

		String str = "Title";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}
	
	/**
	 * Method written for Random Name 
	 * Scripted By: Sekhar
	 * @return
	 */

	public String Random_Description() 
	{

		String str = "Description";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}
	
	/**
	 * Method written for Random Name 
	 * Scripted By: Sekhar
	 * @return
	 */

	public String Random_SearchTags() 
	{

		String str = "SearchTags";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}
	
	/**
	 * Method written for Random Name 
	 * Scripted By: Sekhar
	 * @return
	 */

	public String Random_Rename() 
	{

		String str = "Rename";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}


	@FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[2]/i")
	WebElement btniconupdate;
	
	//@FindBy(xpath = "//*[@id='div_otherInfo']/div/div/div[3]/input[1]")
	@FindBy(xpath = "//*[@id='btnupdateclose']")
	WebElement btnupdate;
	
	@FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")
	WebElement filename;
	
	@FindBy(xpath = "//input[@id='txt_title']")
	WebElement txttitle;
	
	@FindBy(xpath = "//input[@id='txt_desc']")
	WebElement txtdesc;
	
	@FindBy(xpath = "//input[@id='txt_tags']")
	WebElement txttags;
	
	@FindBy(xpath = "//input[@id='txt_fileName']")
	WebElement txtfileName;
	
	
	/**
	 * Method written for EditFile.
	 *  Scripted By: Sekhar
	 * @throws AWTException
	 */
	
	public boolean EditFile_Filelevel(String Title,String Description,String SearchTags,String Rename) throws AWTException
	{			
		SkySiteUtils.waitTill(5000);
		//driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		//SkySiteUtils.waitTill(3000);
		btniconupdate.click();
		Log.message("Update icon from menu button has been clicked");
		SkySiteUtils.waitTill(10000);
		txttitle.clear();
		Log.message("Title name has been cleared");
		txttitle.sendKeys(Title);
		Log.message("Title name has been entered");
		SkySiteUtils.waitTill(2000);
		txtdesc.clear();
		Log.message("Description name has been cleared");
		txtdesc.sendKeys(Description);
		Log.message("Description name has been entered");
		SkySiteUtils.waitTill(2000);
		txttags.clear();
		Log.message("SearchTags name has been cleared");
		txttags.sendKeys(SearchTags);
		Log.message("SearchTags name has been entered");
		SkySiteUtils.waitTill(3000);				
		txtfileName.clear();
		Log.message("Rename has been cleared");
		txtfileName.sendKeys(Rename);
		Log.message("Rename has been entered:"+Rename);
		SkySiteUtils.waitTill(3000);
		btnupdate.click();
		Log.message("Update button has been clicked");
		SkySiteUtils.waitTill(10000);	
		String fName=filename.getText().toString();
		Log.message("File rename is:"+fName);
		SkySiteUtils.waitTill(5000);	
		 if(fName.contains(Rename))
			 return true;
		 else 
			 return false;	
	}
	
	@FindBy(xpath = "//*[@id='txtSearchValue']")
	WebElement txtSearchValue;
	
	@FindBy(xpath = "(//button[@id='btnSearch'])[1]")
	WebElement btnSearch;
	
	@FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")
	WebElement getfilename;	
	
	/**
	 * Method written for File grid search
	 *  Scripted By: Sekhar
	 * @throws AWTException
	 */
	
	public boolean File_Grid_Search(String Rename) throws AWTException
	{	
		SkySiteUtils.waitTill(3000);
		txtSearchValue.sendKeys(Rename);
		Log.message("File rename has been entered"+Rename);			
		SkySiteUtils.waitTill(3000);		
		String ExpName = Rename+".pdf";
		Log.message("Exp Name is:"+ExpName);
		SkySiteUtils.waitTill(2000);
		btnSearch.click();
		Log.message("search button has been clicked");
		SkySiteUtils.waitTill(5000);
		String Filename= getfilename.getText().toString();
		Log.message("file name is:"+Filename);		
		SkySiteUtils.waitTill(5000);
		if(ExpName.equals(Filename))
			return true;
		else
			return false;
	}
	
	@FindBy(xpath = "//*[@id='liGlobalSearch']/a/i")
	WebElement btnsearchicon;
	
	@FindBy(xpath = "//*[@id='idSearchcontroleDropDown']/li[1]/div/div[2]/div[1]/div")
	WebElement btnSearchcontroleDropDown;
	
	@FindBy(xpath = "//button[@id='searchiconbuttonWithOutAttributes']")
	WebElement searchiconbutton;
	
	
	/**
	 * Method written for module search
	 *  Scripted By: Sekhar
	 * @throws AWTException
	 */
	
	public boolean Module_Searchwithfilename(String Rename,String Collectionname) throws AWTException
	{	
		boolean result1=false;
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		btnsearchicon.click();  
		Log.message("search icon button has been clicked ");
		SkySiteUtils.waitTill(2000);
		btnSearchcontroleDropDown.click();
		Log.message("DropDown icon button has been clicked ");
		SkySiteUtils.waitTill(5000); 		
		if(!driver.findElement(By.id("SearchsettingSelectall")).isSelected())
		{
			driver.findElement(By.id("SearchsettingSelectall")).click();
		}       
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.id("SearchsettingSelectall")).click();
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.id("ChkSearchSettingFile")).click();        
		if ( !driver.findElement(By.id("ChkSearchSettingFile")).isSelected())
		{
			driver.findElement(By.id("ChkSearchSettingFile")).click();
		} 
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//input[@id='txtSearchKey']")).clear();
		SkySiteUtils.waitTill(30000);
		driver.findElement(By.xpath("//input[@id='txtSearchKey']")).sendKeys(Rename);
		Log.message("File name has ben entered");
		SkySiteUtils.waitTill(30000);		
		searchiconbutton.click();
		Log.message("Global search button has ben clicked");
		SkySiteUtils.waitTill(15000); 		
		int File_Count=0;
		int i = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//i[@class='icon icon-download ico-lg']"));//Taking Download Count
		for (WebElement Element : allElements)
		{ 
			File_Count = File_Count+1; 	
		}
		Log.message("File Download Count is: "+File_Count);
		//Getting Folder Names and machine
		for(i = 1;i<=File_Count;i++)
		{					
			String fName=driver.findElement(By.xpath("(//*[@id='div_SearchContent']/li/div/div[1]/h4/a)["+i+"]")).getText().toString();
			Log.message("File Name is:"+fName);
			String Exp_ProjectaName=driver.findElement(By.xpath("(//*[@id='div_SearchContent']/li/div/div[1]/h5)["+i+"]")).getText();
			Log.message("Exp Project Name is:"+Exp_ProjectaName);
			if((fName.contains(Rename))&&(Exp_ProjectaName.contains(Collectionname)))
			{	
				result1=true;
				Log.message("Module search with file name Successfully");	
				break;
			}
		}
		if(result1==true)
			return true;
		else
			return false;
	}
	
	/**
	 * Method written for Global search
	 *  Scripted By: Sekhar
	 * @throws AWTException
	 */
	
	public boolean Global_Searchwithfilename(String Rename,String Collectionname) throws AWTException
	{	
		boolean result1=false;
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		btnsearchicon.click();  
		Log.message("search icon button has been clicked ");
		SkySiteUtils.waitTill(2000);
		btnSearchcontroleDropDown.click();
		Log.message("DropDown icon button has been clicked ");
		SkySiteUtils.waitTill(5000); 		
		driver.findElement(By.id("SearchsettingSelectall")).click();        
		if(!driver.findElement(By.id("SearchsettingSelectall")).isSelected())
		{
			driver.findElement(By.id("SearchsettingSelectall")).click();
		}		
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//input[@id='txtSearchKey']")).clear();
		SkySiteUtils.waitTill(30000);
		driver.findElement(By.xpath("//input[@id='txtSearchKey']")).sendKeys(Rename);
		Log.message("File name has ben entered");
		SkySiteUtils.waitTill(30000);		
		searchiconbutton.click();
		Log.message("Global search button has ben clicked");
		SkySiteUtils.waitTill(15000); 		
		int File_Count=0;
		int i = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//i[@class='icon icon-download ico-lg']"));//Taking Download Count
		for (WebElement Element : allElements)
		{ 
			File_Count = File_Count+1; 	
		}
		Log.message("File Download Count is: "+File_Count);
		//Getting Folder Names and machine
		for(i = 1;i<=File_Count;i++)
		{					
			String fName=driver.findElement(By.xpath("(//*[@id='div_SearchContent']/li/div/div[1]/h4/a)["+i+"]")).getText().toString();
			Log.message("File Name is:"+fName);
			String Exp_ProjectaName=driver.findElement(By.xpath("(//*[@id='div_SearchContent']/li/div/div[1]/h5)["+i+"]")).getText();
			Log.message("Exp Project Name is:"+Exp_ProjectaName);
			if((fName.contains(Rename))&&(Exp_ProjectaName.contains(Collectionname)))
			{	
				result1=true;
				Log.message("Global search with file name Successfully");	
				break;
			}
		}
		if(result1==true)
			return true;
		else
			return false;
	}
	
	@FindBy(xpath = "//span[@class='selectedTreeRow']")
	WebElement selectedTreeRow;
	
	@FindBy(xpath = ".//*[@id='lftpnlMore']")
	WebElement lftpnlMore;
	
	@FindBy(xpath = "(//i[@class='icon icon-download'])[1]")
	WebElement btnicondownload;
	
	/**
	 * Method written for Download folder.
	 *  Scripted By: Sekhar
	 * @throws AWTException
	 */
	
	public boolean Download_Folder(String DownloadPath) throws AWTException
	{			
		SkySiteUtils.waitTill(5000);		
		// Calling delete files from download folder script
		driver.switchTo().defaultContent();
		fnaHomePage = new FnaHomePage(driver).get();
		fnaHomePage.Delete_Files_From_Folder(DownloadPath);
		SkySiteUtils.waitTill(5000);		
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); 
		SkySiteUtils.waitTill(5000);
		String foldername=selectedTreeRow.getText();
		Log.message("Folder Name is:"+foldername);
		SkySiteUtils.waitTill(3000);
		lftpnlMore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		btnicondownload.click();	
		Log.message("Download folder button has been clicked");
		SkySiteUtils.waitTill(8000);
		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);

		if (browserName.contains("firefox")) 
		{
			// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();

			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}
		SkySiteUtils.waitTill(25000);
		// After checking whether download folder or not
		String ActualFoldername = null;

		File[] files = new File(DownloadPath).listFiles();

		for (File file : files)
		{
			if (file.isFile()) 
			{
				ActualFoldername = file.getName();// Getting Folder Name into a variable
				Log.message("Actual Folder name is:" + ActualFoldername);
				SkySiteUtils.waitTill(1000);
				Long ActualFolderSize = file.length();
				Log.message("Actual Folder size is:" + ActualFolderSize);
				SkySiteUtils.waitTill(5000);
				if (ActualFolderSize != 0) 
				{
					Log.message("Downloaded folder is available in downloads!!!");
				} 
				else
				{
					Log.message("Downloaded folder is NOT available in downloads!!!");
				}
			}
		}
		if (ActualFoldername.contains(foldername))
			return true;
		else
			return false;
	}
	
	@FindBy(xpath = "//table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span")
	WebElement foldertree;

	@FindBy(xpath = "//i[@class='icon icon-modify-folder']")
	WebElement modifyfolder;
	
	@FindBy(xpath = "//*[@id='txtFolderName']")
	WebElement txtFolderName;
	
	@FindBy(xpath = "//*[@id='btnNewFolderSave']")
	WebElement btnNewFolderSave;

	
	/**
	 * Method written for Modify folder
	 *  Scripted By: Sekhar
	 * @throws AWTException
	 */
	
	public boolean Modify_Folder(String RenameFolder) throws AWTException
	{	
		
		SkySiteUtils.waitTill(5000);
		//driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		foldertree.click();
		Log.message("folder tree has been clicked");
		SkySiteUtils.waitTill(2000);
		lftpnlMore.click();  
		Log.message("more option button has been clicked");
		SkySiteUtils.waitTill(2000);
		modifyfolder.click();
		Log.message("rename folder button has been clicked");
		SkySiteUtils.waitTill(2000);
		txtFolderName.clear();
		Log.message("folder name has been cleared");
		SkySiteUtils.waitTill(2000);
		txtFolderName.sendKeys(RenameFolder);
		Log.message("folder name has been entered:"+RenameFolder);
		SkySiteUtils.waitTill(2000);
		btnNewFolderSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(2000);
		String Foldername = selectedTreeRow.getText();
		Log.message("Folder Rename is:"+Foldername);
		SkySiteUtils.waitTill(5000);
		if(Foldername.contains(RenameFolder))
			return true;
		else
			return false;
	}
	
	/**
	 * Method written for Random Folder Name 
	 * Scripted By: Sekhar
	 * @return
	 */

	public String Random_Foldername() 
	{

		String str = "Folder";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}
	
	/**
	 * Method written for Random AlbumName Scripted By: Sekhar
	 * 
	 * @return
	 */

	public String Random_Albumname() 
	{

		String str = "Album";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}

	
	@FindBy(xpath = "//*[@id='ProjectMenu1_Album']/a")
	WebElement btnalbum;

	@FindBy(xpath = "//*[@id='btnAddFolder']")
	WebElement btncreatealbum;

	@FindBy(xpath = "//*[@id='txtAlbumName']")
	WebElement txtalbumname;

	@FindBy(xpath = "//*[@id='btnAlbumSave']")
	WebElement btnAlbumSave;
	
	@FindBy(xpath = "(//span[@class='selectedTreeRow'])[1]")
	WebElement slttreefolder;

	/**
	 * Method written for Adding Album 
	 * Scripted By: Sekhar
	 * @return
	 * @throws AWTException
	 */
	public boolean Adding_Album(String AlbumName) throws AWTException
	{
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		btnalbum.click();
		Log.message("Album button has been clicked");
		SkySiteUtils.waitTill(5000);
		// Switch to Frame
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switch to I Frame Success!!");
		SkySiteUtils.waitTill(5000);
		btncreatealbum.click(); // Adding on New Folder
		Log.message("Create album button has been clicked");
		SkySiteUtils.waitTill(5000);
		txtalbumname.sendKeys(AlbumName);
		Log.message("Album name has been entered");
		SkySiteUtils.waitTill(3000);
		btnAlbumSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(5000);
		String Album_name = slttreefolder.getText();
		Log.message("Act Album Name is:" + Album_name);
		if (Album_name.contains(AlbumName)) 
		{
			Log.message("Adding the Album validation successfull ");
			return true;
		} 
		else 
		{
			Log.message("Adding the Album validation Unsuccessfull ");
			return false;
		}
	}
	
	@FindBy(xpath = "//*[@id='treeRefresh']")
	WebElement treeRefresh;

	@FindBy(xpath = "//*[@id='dvAlbumImageContainer']/button")
	WebElement btnuploadphoto;
	
	@FindBy(xpath = ".//*[@id='fine-uploader']/div/div[2]/div/div[2]/input")		
	WebElement btnselctFile1;//modify sekhar
	
	@FindBy(xpath = "//*[@id='dvAlbumImageContainer']/div")		
	WebElement dvAlbumImageContainer;
	/**
	 * Method written for Upload multiple photos 
	 * Scripted By: Sekhar
	 * @return
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean Upload_MultiplePhotos(String PhotosPath) throws AWTException, IOException
	{
		SkySiteUtils.waitTill(5000);
		treeRefresh.click();
		Log.message("refresh button has been clicked");
		SkySiteUtils.waitTill(5000);
		btnuploadphoto.click();
		Log.message("upload button has been clicked");
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(10000);
		btnselctFile1.click();
		Log.message("select photos has been clicked");
		SkySiteUtils.waitTill(25000);
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));

		String expFilename = null;
		File[] files = new File(PhotosPath).listFiles();

		for (File file : files) 
		{
			if (file.isFile()) 
			{
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(5000);
			}
		}
		output.flush();
		output.close();

		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
		// Executing .exe autoIt file
		Runtime.getRuntime().exec(AutoItexe_Path + " " + PhotosPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(30000);

		try
		{
			File file = new File(tmpFileName);
			if (file.delete()) 
			{
				Log.message(file.getName() + " is deleted!");
			} 
			else 
			{
				Log.message("Delete operation is failed.");
			}
		} 
		catch (Exception e)
		{
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(10000);
		String BeforeUpload_Count = driver.findElement(By.xpath("//*[@id='lblFileCount']")).getText().toString();// Getting label counts
		Log.message("Before upload count is: " + BeforeUpload_Count);// Getting Lite user counts
		SkySiteUtils.waitTill(5000);
		String[] y = BeforeUpload_Count.split(" ");
		String AvlUpload_Count = y[0];
		int CountBefore = Integer.parseInt(AvlUpload_Count);
		Log.message("Avl before upload photo count is: " + CountBefore);// Getting Lite user counts
		SkySiteUtils.waitTill(15000);
		driver.findElement(By.xpath("//button[@id='btnFileUpload']")).click();// Clicking on Upload button
		Log.message("Upload button has been clicked");
		SkySiteUtils.waitTill(50000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(10000);
		//SkySiteUtils.waitForElementLoadTime(driver, dvAlbumImageContainer, 60);
		int AfterUpload_Count = driver.findElements(By.xpath("//*[@id='dvAlbumImageContainer']/div")).size();// Getting	label counts
		Log.message("After upload photo count is: " + AfterUpload_Count);// Getting Lite user counts
		
		SkySiteUtils.waitTill(5000);
		if (CountBefore == AfterUpload_Count)
			return true;
		else
			return false;
	}

	@FindBy(css = ".btn.btn-primary.am-show-tooltip.test-tool")
	WebElement btnmore;
	
	@FindBy(xpath = "//i[@class='icon icon-download']")
	WebElement downloadalbum;
	
	/**
	 * Method written for Album Download.
	 *  Scripted By: Sekhar
	 * @throws AWTException
	 */
	
	public boolean Album_Download(String DownloadPath) throws AWTException
	{			
		SkySiteUtils.waitTill(5000);		
		// Calling delete files from download folder script
		driver.switchTo().defaultContent();
		fnaHomePage = new FnaHomePage(driver).get();
		fnaHomePage.Delete_Files_From_Folder(DownloadPath);
		SkySiteUtils.waitTill(5000);		
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); 
		SkySiteUtils.waitTill(5000);
		String Albumname=selectedTreeRow.getText();
		Log.message("Album Name is:"+Albumname);
		SkySiteUtils.waitTill(3000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		downloadalbum.click();	
		Log.message("Download album button has been clicked");
		SkySiteUtils.waitTill(8000);
		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);

		if (browserName.contains("firefox")) 
		{
			// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();

			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}
		SkySiteUtils.waitTill(25000);
		// After checking whether download folder or not
		String ActualFoldername = null;

		File[] files = new File(DownloadPath).listFiles();

		for (File file : files)
		{
			if (file.isFile()) 
			{
				ActualFoldername = file.getName();// Getting Folder Name into a variable
				Log.message("Actual album name is:" + ActualFoldername);
				SkySiteUtils.waitTill(1000);
				Long ActualFolderSize = file.length();
				Log.message("Actual album size is:" + ActualFolderSize);
				SkySiteUtils.waitTill(5000);
				if (ActualFolderSize != 0) 
				{
					Log.message("Downloaded album is available in downloads!!!");
				} 
				else
				{
					Log.message("Downloaded album is NOT available in downloads!!!");
				}
			}
		}
		if (ActualFoldername.contains(Albumname))
			return true;
		else
			return false;
	}
	
	@FindBy(xpath = "//i[@class='icon icon-star ico-lg icon-album']")
	WebElement btnalbumheader;
	
	/**
	 * Method written for select album 
	 * Scripted By: Sekhar
	 * @return
	 * @throws AWTException
	 */
	public boolean Select_Album(String AlbumName) throws AWTException
	{
		boolean result1 = false;
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		btnalbumheader.click();
		Log.message("Album header button has been clicked");
		SkySiteUtils.waitTill(5000);
		// Counting available folders
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		driver.findElement(By.xpath("(//span[@class='standartTreeRow' and @style='padding-left: 5px; padding-right: 5px;'])[2]")).click();
		SkySiteUtils.waitTill(3000);
		int Album_Count = 0;
		int i = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("(//span[@style='padding-left: 5px; padding-right: 5px;'])"));
		for (WebElement Element : allElements) 
		{
			Album_Count = Album_Count + 1;
		}
		Log.message("Album Count: " + Album_Count);
		// Getting Album Names and machine
		for (i = 2; i <= Album_Count; i++) 
		{
			String Exp_AlbumName = driver.findElement(By.xpath("(//span[@style='padding-left: 5px; padding-right: 5px;'])[" + i + "]")).getText();
			Log.message("Exp Album Name: " + Exp_AlbumName);
			if (Exp_AlbumName.contentEquals(AlbumName))
			{
				result1 = true;
				Log.message("Exp Album is Available");
				break;
			}
		}
		if (result1 == true)
		{
			// select the Folder
			driver.findElement(By.xpath("(//span[@style='padding-left: 5px; padding-right: 5px;'])[" + i + "]")).click();// Select Folder
			Log.message("selected Album has been clicked ");
			SkySiteUtils.waitTill(10000);
		} 
		else
		{
			result1 = false;
			Log.message("Album is Not Available.....");
		}		
		if(result1 == true)	
			return true;		
		else		
			return false;		
	}
	
	/**
	 * Method written for album slide show 
	 * Scripted By: Sekhar
	 * @return
	 * @throws AWTException
	 */
	public boolean Album_Slideshow() throws AWTException
	{
		boolean result1 = false;
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("//*[@id='dvAlbumImageContainer']/div[1]/div/a")).click();
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				.withTimeout(120, TimeUnit.SECONDS)//max wait 120 seconds
				.pollingEvery(1,TimeUnit.MILLISECONDS)//For every 1 seconds checks expected condition satisfies
				.ignoring(NoSuchElementException.class);//ignores if in between get exceptions
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='slideralbmchild']/div[1]/div/div[2]")));//wait for element clickable								
		
		String Exp_PhotoName = driver.findElement(By.xpath("//*[@id='ulImageSliderMaxContainer']/li[1]/span")).getText();//Getting from app
		Log.message("Exp Album Name: "+Exp_PhotoName);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ulImageSliderMaxContainer']/li[2]/span")));//wait for element clickable								
		
		String Exp_PhotoName1 = driver.findElement(By.xpath("//*[@id='ulImageSliderMaxContainer']/li[2]/span")).getText();//Getting from app
		Log.message("Exp Album Name: "+Exp_PhotoName1);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ulImageSliderMaxContainer']/li[3]/span")));//wait for element clickable								
		
		String Exp_PhotoName2 = driver.findElement(By.xpath("//*[@id='ulImageSliderMaxContainer']/li[3]/span")).getText();//Getting from app
		Log.message("Exp Album Name: "+Exp_PhotoName2);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ulImageSliderMaxContainer']/li[1]/span")));//wait for element clickable								
		SkySiteUtils.waitTill(5000);
		if((Exp_PhotoName.contains("Hydrangeas.jpg"))&&(Exp_PhotoName1.contains("Chrysanthemum.jpg"))
				&&(Exp_PhotoName2.contains("Desert.jpg")))
		{
			result1=true;
			Log.message("Album is Slide Show successfully!!!");		
		}
		else
		{
			result1=false;
			Log.message("Album is Slide Show successfully!!!");			
		}
		if(result1==true)
			return true;
		else
			return false;
	}
	
	@FindBy(xpath = "//i[@class='icon icon-modify-folder']")
	WebElement btnmodifyfolder;
	
	@FindBy(xpath = "//*[@id='txtAlbumName']")
	WebElement txtAlbumName;
	
	
	/**
	 * Method written for Modify album 
	 * Scripted By: Sekhar
	 * @return
	 * @throws AWTException
	 */
	public boolean Modify_Album(String RenameAlbum) throws AWTException
	{
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		btnalbumheader.click();
		Log.message("Album header button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		driver.findElement(By.xpath("(//span[@class='standartTreeRow' and @style='padding-left: 5px; padding-right: 5px;'])[2]")).click();
		SkySiteUtils.waitTill(3000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		btnmodifyfolder.click();
		Log.message("rename album button has been clicked");
		SkySiteUtils.waitTill(3000);
		txtAlbumName.clear();
		Log.message("album name has been cleared");
		SkySiteUtils.waitTill(3000);
		txtAlbumName.sendKeys(RenameAlbum);
		Log.message("album Rename is:"+RenameAlbum);
		SkySiteUtils.waitTill(3000);
		btnAlbumSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(3000);
		String Albumname=selectedTreeRow.getText();
		Log.message("Album Name is:"+Albumname);
		SkySiteUtils.waitTill(3000);
		if(Albumname.contains(RenameAlbum))
			return true;
		else
			return false;
	}
	
	@FindBy(xpath = "//*[@id='Div1']/nav/div[1]/div/button[2]")
	WebElement btnmanage;	
	
	@FindBy(xpath = "//tbody/tr/td[1]/div/table/tbody/tr/td[1]/img")
	WebElement btnupdateinformation;
	
	@FindBy(xpath = "//input[@id='txt_title']")
	WebElement txttitle1;	
	
	@FindBy(xpath = "//input[@id='txt_desc']")
	WebElement txtdesc1;
	
	@FindBy(xpath = "//input[@id='txt_tags']")
	WebElement txttags1;
	
	@FindBy(xpath = "//*[@id='div_otherInfoTable']/div[3]/button[1]")
	WebElement btnupdate1;
	
	@FindBy(xpath = "//*[@id='albumOperation']/div/div/div[3]/button[3]")
	WebElement btnclose;
	
	/**
	 * Method written for Edit photo in image level
	 * Scripted By: Sekhar
	 * @return
	 * @throws AWTException
	 */
	public boolean Edit_Photo(String Title,String Description,String SearchTags) throws AWTException
	{
		boolean result1=true;
		SkySiteUtils.waitTill(3000);
		btnmanage.click();
		Log.message("manage button has been clicked");
		SkySiteUtils.waitTill(3000);		
		btnupdateinformation.click();
		Log.message("update information has been clicked");
		SkySiteUtils.waitTill(3000);
		txttitle1.clear();
		Log.message("Title has been cleared");
		SkySiteUtils.waitTill(1000);
		txttitle1.sendKeys(Title);
		Log.message("Title has been entered:"+Title);
		SkySiteUtils.waitTill(1000);	
		txtdesc1.clear();
		Log.message("Description has been cleared");
		SkySiteUtils.waitTill(1000);
		txtdesc1.sendKeys(Description);
		Log.message("Description has been entered:"+Description);
		SkySiteUtils.waitTill(1000);	
		txttags1.clear();
		Log.message("SearchTags has been cleared");
		SkySiteUtils.waitTill(1000);
		txttags1.sendKeys(SearchTags);
		Log.message("SearchTags has been entered:"+SearchTags);
		SkySiteUtils.waitTill(3000);
		btnupdate1.click();
		Log.message("update button has been clicked");
		SkySiteUtils.waitTill(15000);
		/*driver.switchTo().defaultContent();
		String Msg_notify=driver.findElement(By.xpath("//span[@class='noty_text']")).getText();
		System.out.println("Msg Notify is:"+Msg_notify);
		SkySiteUtils.waitTill(3000);*/
		//driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		//SkySiteUtils.waitTill(3000);
		btnclose.click();
		Log.message("close button has been clicked");
		SkySiteUtils.waitTill(5000);
		String Exp_Title=driver.findElement(By.xpath("//*[@id='dvAlbumImageContainer']/div[1]/div/span")).getText();
		System.out.println("Exp Title Name is:"+Exp_Title);
		SkySiteUtils.waitTill(3000);
		if(Exp_Title.equals(Title))
		{
			result1=true;
			Log.message("Edit photo in image level Successfully");
		}
		else
		{
			result1=false;
			Log.message("Edit photo in image level UnSuccessfully");
		}
		if(result1==true)
			return true;
		else
			return false;		
	}
	
	/**
	 * Method written for module search with photo name
	 *  Scripted By: Sekhar
	 * @throws AWTException
	 */
	
	public boolean Module_SearchwithPhotoname(String Title,String Description,String Collectionname) throws AWTException
	{	
		boolean result1=false;
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		btnsearchicon.click();  
		Log.message("search icon button has been clicked ");
		SkySiteUtils.waitTill(2000);
		btnSearchcontroleDropDown.click();
		Log.message("DropDown icon button has been clicked ");
		SkySiteUtils.waitTill(5000); 		
		if(!driver.findElement(By.id("SearchsettingSelectall")).isSelected())
		{
			driver.findElement(By.id("SearchsettingSelectall")).click();
		}       
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.id("SearchsettingSelectall")).click();
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.id("ChkSearchSettingPhoto")).click();        
		if ( !driver.findElement(By.id("ChkSearchSettingPhoto")).isSelected())
		{
			driver.findElement(By.id("ChkSearchSettingPhoto")).click();
		} 
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//input[@id='txtSearchKey']")).clear();
		SkySiteUtils.waitTill(30000);
		driver.findElement(By.xpath("//input[@id='txtSearchKey']")).sendKeys(Title);
		Log.message("Title name has been entered");
		SkySiteUtils.waitTill(40000);		
		searchiconbutton.click();
		Log.message("Global search button has ben clicked");
		SkySiteUtils.waitTill(15000); 		
		String Exp_AlbumTitle = driver.findElement(By.xpath("//*[@id='div_SearchContent']/li/div/div[1]/h4")).getText();//Getting from App
    	System.out.println("Exp Album Title: "+Exp_AlbumTitle);
    	SkySiteUtils.waitTill(3000);
    	String Exp_AlbumDes = driver.findElement(By.xpath("//*[@id='div_SearchContent']/li/div/div[1]/h5[1]")).getText();//Getting from App
    	System.out.println("Exp Album Description: "+Exp_AlbumDes);
    	String Exp_AlbumProject = driver.findElement(By.xpath(".//*[@id='div_SearchContent']/li/div/div[1]/h5[2]")).getText();//Getting from App
    	System.out.println("Exp Album Project Name: "+Exp_AlbumProject);
    	SkySiteUtils.waitTill(3000);
    	if((Exp_AlbumTitle.equals(Title))&&(Exp_AlbumDes.equals(Description))&&(Exp_AlbumProject.contains(Collectionname)))
    	{    		
    		result1=true;
    		Log.message("module Search with photo name successfully!!!");
    	}
    	else
    	{
    		result1=false;
    		Log.message("module Search with photo name Unsuccessfully!!!");    	
    	}
    	if(result1==true)
    		return true;
    	else
    		return false;
	}
	
	/**
	 * Method written for Global search with photo name
	 *  Scripted By: Sekhar
	 * @throws AWTException
	 */
	
	public boolean Global_SearchwithPhotoname(String Title,String Description,String Collectionname) throws AWTException
	{	
		boolean result1=false;
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		btnsearchicon.click();  
		Log.message("search icon button has been clicked ");
		SkySiteUtils.waitTill(2000);
		btnSearchcontroleDropDown.click();
		Log.message("DropDown icon button has been clicked ");
		SkySiteUtils.waitTill(5000); 		
		driver.findElement(By.id("SearchsettingSelectall")).click();        
		if(!driver.findElement(By.id("SearchsettingSelectall")).isSelected())
		{
			driver.findElement(By.id("SearchsettingSelectall")).click();
		}		
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//input[@id='txtSearchKey']")).clear();
		SkySiteUtils.waitTill(30000);
		driver.findElement(By.xpath("//input[@id='txtSearchKey']")).sendKeys(Title);
		Log.message("Title name has been entered");
		SkySiteUtils.waitTill(40000);		
		searchiconbutton.click();
		Log.message("Global search button has ben clicked");
		SkySiteUtils.waitTill(15000); 		
		String Exp_AlbumTitle = driver.findElement(By.xpath("//*[@id='div_SearchContent']/li/div/div[1]/h4")).getText();//Getting from App
    	System.out.println("Exp Album Title: "+Exp_AlbumTitle);
    	SkySiteUtils.waitTill(3000);
    	String Exp_AlbumDes = driver.findElement(By.xpath("//*[@id='div_SearchContent']/li/div/div[1]/h5[1]")).getText();//Getting from App
    	System.out.println("Exp Album Description: "+Exp_AlbumDes);
    	String Exp_AlbumProject = driver.findElement(By.xpath(".//*[@id='div_SearchContent']/li/div/div[1]/h5[2]")).getText();//Getting from App
    	System.out.println("Exp Album Project Name: "+Exp_AlbumProject);
    	SkySiteUtils.waitTill(3000);
    	if((Exp_AlbumTitle.equals(Title))&&(Exp_AlbumDes.equals(Description))&&(Exp_AlbumProject.contains(Collectionname)))
    	{    		
    		result1=true;
    		Log.message("Global Search with photo name successfully!!!");
    	}
    	else
    	{
    		result1=false;
    		Log.message("Global Search with photo name Unsuccessfully!!!");    	
    	}
    	if(result1==true)
    		return true;
    	else
    		return false;
	}
	
	/**
	 * Method written for Random(Subject)
	 *  Scripted By: Sekhar
	 * 
	 * @return
	 */

	public String Random_Subject()
	{

		String str = "Subject";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}
	
	 @FindBy(css = ".icon.icon-star.ico-lg.icon-communication")
     WebElement btniconcommunication;
	 
	 @FindBy(css = "#btnNewCommunication")
     WebElement btnNewCommunication;
	
	 @FindBy(xpath = "//*[@id='txtMessageBody_container']/table/tbody/tr[2]/td")
     WebElement txtMessageBody;
	
	 @FindBy(css = "#btnToReceipients")
     WebElement btnToReceipients;
	 
	 @FindBy(css = "#btnAddInvitesAndClose")
     WebElement btnAddInvitesAndClose;
	 
	 @FindBy(css = "#txtSubject")
     WebElement txtSubject;
	 
	 @FindBy(css = "#tinymce")
     WebElement txttinymce;
	 
	 @FindBy(css = "#btnSaveCommunication")
     WebElement btnSaveCommunication;
	
	 @FindBy(css = "#btnResetSearch")
     WebElement btnResetSearch;
	
	 @FindBy(css = "#rgtpnlMore")
	 WebElement btnrgtpnlMore;
	    
	 @FindBy(xpath = "//*[@id='FileActionMenu']/li[1]/a")
	 WebElement btndrpattachcom;
	    
	 @FindBy(xpath = "//input[@name='qqfile']")
	 WebElement btnselectfile;
	    
	 @FindBy(xpath = "//button[@id='btnFileUpload']")
	 WebElement btnFileUpload;
	    
	 @FindBy(xpath = "//*[@id='FileActionMenu']/li[2]/a")
	 WebElement btndrpattachcoll;
	 
	 @FindBy(xpath = "//*[@id='divFilesGrid']/div[2]/table/tbody/tr[2]/td[1]/img")
	 WebElement chkdivFilesGrid;	
	    
	 @FindBy(xpath = "(//input[@class='btn btn-primary'])[2]")
	 WebElement btnbtnprimary;
	 /** 
	  * Method written for Attachment from computer and collection
	  * Scripted By: Sekhar     
	  * @throws IOException 	 
	  * @throws AWTException 
	  */
		
	 public boolean Attchments_communication(String Contact,String Email,String SubjectName,String FolderPath) throws IOException 
	 {
		 boolean result1=false;
		 boolean result2=false;
		 boolean result3=false;
		 driver.switchTo().defaultContent();
		 btniconcommunication.click();
		 Log.message("Communication button has been clicked"); 
		 SkySiteUtils.waitTill(5000);
		 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		 Log.message("Swich to I Frame Success!!"); 
		 SkySiteUtils.waitTill(5000);
		 btnNewCommunication.click();//click add 
		 Log.message("Add button has been clicked"); 
		 SkySiteUtils.waitForElement(driver, txtMessageBody, 120);   
		 btnToReceipients.click();//click on TO button
		 Log.message("ToReceipients button has been clicked");
		 SkySiteUtils.waitTill(5000);
		 driver.switchTo().defaultContent();		 
		 int File_Count=0;
		 int i = 0;
		 List<WebElement> allElements = driver.findElements(By.xpath("//*[@id='divGridRecp']/div[2]/table/tbody/tr/td[1]/img"));//Taking check box Count
		 for (WebElement Element : allElements)
		 { 
			 File_Count = File_Count+1; 	
		 }
			 Log.message("Check box Count is: "+File_Count);
			 //Getting Folder Names and machine
			 for(i = 1;i<=File_Count;i++)
			 {					
				 String fName=driver.findElement(By.xpath("(//*[@id='divGridRecp']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
				 Log.message("Contact Name is:"+fName);			
				 String Act_MailID=driver.findElement(By.xpath("(//*[@id='divGridRecp']/div[2]/table/tbody/tr/td[4])["+i+"]")).getText();
				 Log.message("Act Email ID is:"+Act_MailID);				
				 SkySiteUtils.waitTill(2000);
				 if(((fName.trim()).contentEquals(Contact.trim()))&&((Act_MailID.trim()).contentEquals(Email.trim())))
				 {	
					 result1=true;					 
					 Log.message("Expected Contact Selected Successfully");	
					 driver.findElement(By.xpath("(//*[@id='divGridRecp']/div[2]/table/tbody/tr/td[1]/img)["+i+"]")).click();//select Check box
					 SkySiteUtils.waitTill(5000);
					 break;
				 }
			 }
			 if(result1==true)
			 {			
				 SkySiteUtils.waitTill(3000);			
				 String Exp_ContactName=driver.findElement(By.xpath("(//*[@id='divGridRecp']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
				 Log.message("Exp Contact Name is:"+Exp_ContactName);			
			 }
			 else
			 {
				 Log.message("Expected Contact Selected UnSuccessfully");					
			 }
			 SkySiteUtils.waitTill(3000);	 
			 btnAddInvitesAndClose.click();//click on Save button
			 Log.message("save & close button has been clicked");
			 SkySiteUtils.waitTill(3000);
			 driver.switchTo().frame(driver.findElement(By.id("myFrame")));//Switch to my frame
			 SkySiteUtils.waitTill(3000);		
			 txtSubject.sendKeys(SubjectName); 
			 Log.message("subject name has been entered");
			 SkySiteUtils.waitTill(5000);
			 driver.switchTo().frame(driver.findElement(By.id("txtMessageBody_ifr")));//Switch to message frame
			 SkySiteUtils.waitTill(3000);
			 txttinymce.sendKeys(SubjectName);
			 Log.message("subject body has been entered");		
			 SkySiteUtils.waitTill(5000);
			 driver.switchTo().defaultContent();
			 driver.switchTo().frame(driver.findElement(By.id("myFrame")));//Switch to my frame
			 SkySiteUtils.waitTill(5000);		 
			 btnrgtpnlMore.click();
			 Log.message("Add button has been clicked");			
			 SkySiteUtils.waitTill(3000);
			 btndrpattachcom.click();
			 Log.message("Attach from computer button has been clicked");	
			 SkySiteUtils.waitTill(8000);
			 String parentHandle = driver.getWindowHandle(); //Getting parent window
			 Log.message(parentHandle);
			 for (String winHandle : driver.getWindowHandles())
			 {
				 driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
			 }
			 SkySiteUtils.waitTill(10000);
			 driver.switchTo().defaultContent();
			 btnselectfile.click();
			 Log.message("Select files button has been clicked");				
			 SkySiteUtils.waitTill(5000);			 
			// Writing File names into a text file for using in AutoIT Script
			 BufferedWriter output;
			 randomFileName rn = new randomFileName();
			 //String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
			 String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
			 output = new BufferedWriter(new FileWriter(tmpFileName, true));			 
			 String expFilename = null;
			 File[] files = new File(FolderPath).listFiles();
			 
			 for (File file : files) 
			 {
				 if (file.isFile()) 
				 {
					 expFilename = file.getName();// Getting File Names into a variable
					 Log.message("Expected File name is:" + expFilename);
					 output.append('"' + expFilename + '"');
					 output.append(" ");
					 SkySiteUtils.waitTill(5000);
				 }	
			 }
			 output.flush();
			 output.close(); 
			 
			 String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
			 // Executing .exe autoIt file
			 Runtime.getRuntime().exec(AutoItexe_Path + " " + FolderPath + " " + tmpFileName);
			 Log.message("AutoIT Script Executed!!");
			 SkySiteUtils.waitTill(30000);			 
			 try 
			 {
				 File file = new File(tmpFileName);
				 if (file.delete()) 
				 {
					 Log.message(file.getName() + " is deleted!");
				 }
				 else 
				 {
					 Log.message("Delete operation is failed.");
				}
			 } 
			 catch (Exception e)
			 {
				 Log.message("Exception occured!!!" + e);
			 }
			 SkySiteUtils.waitTill(8000);
			 btnFileUpload.click();// Clicking on Upload button
			 Log.message("Upload button has been clicked");
			 SkySiteUtils.waitTill(30000);
			 driver.switchTo().window(parentHandle);// Switch back to folder page
			 SkySiteUtils.waitTill(5000);		
			 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			 SkySiteUtils.waitTill(5000);
			 if((driver.findElement(By.xpath("//*[@id='divAttachment']/div[2]/table/tbody/tr[2]/td[1]/a[2]")).isDisplayed())
					 &&(driver.findElement(By.xpath("//i[@class='icon icon-files']")).isDisplayed()))
			 {
				 result2=true;
				 Log.message("Attachment File From Computer Successfuuly");
			 }
			 else
			 {
				 result2=false;
				 Log.message("Attachment File From Computer UnSuccessfuuly");			
			 }
			 SkySiteUtils.waitTill(5000);
			 btnrgtpnlMore.click();//click on Add button
			 Log.message("add button has been clicked");						
			 SkySiteUtils.waitTill(2000);
			 btndrpattachcoll.click();//clickon Attach Fron your Project
			 Log.message("attach from collection has been clicked");						
			 SkySiteUtils.waitTill(15000);
			 String parentHandle1 = driver.getWindowHandle(); //Getting parent window
			 Log.message(parentHandle1);
			 for (String winHandle : driver.getWindowHandles())
			 {
				 driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
			 }
			 SkySiteUtils.waitTill(2000);
			 driver.switchTo().defaultContent();
			 chkdivFilesGrid.click();//click on Check box
			 Log.message("check box button has been clicked");						
			 SkySiteUtils.waitTill(2000);
			 btnbtnprimary.click();
			 Log.message("add & close button has been clicked");						
			 SkySiteUtils.waitTill(5000);							 
			 driver.switchTo().window(parentHandle1);//Switch back to folder page 	
			 SkySiteUtils.waitTill(5000);
			 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			 SkySiteUtils.waitTill(5000);
			 if((driver.findElement(By.xpath("//*[@id='divAttachment']/div[2]/table/tbody/tr[3]/td[1]/a[2]")).isDisplayed())
					 &&(driver.findElement(By.xpath("//i[@class='icon icon-collection']")).isDisplayed()))
			 {
				 result3=true;
				 Log.message("Attach From Project Successfully");
			 }
			 else
			 {
				 result3=false;
				 Log.message("Attach From Project UnSuccessfully");			 
			 }
			 SkySiteUtils.waitTill(5000);
			 btnSaveCommunication.click();
			 Log.message("save & send button has been clicked");
			 SkySiteUtils.waitTill(5000);
			 btnResetSearch.click();//click on Reset button
			 Log.message("Reset button has been clicked");
			 SkySiteUtils.waitTill(5000);
			 String Subject=driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]")).getText();
			 Log.message("Subject name is:"+Subject);
			 SkySiteUtils.waitTill(3000);
			 String Tocontact=driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[3]")).getText();
			 Log.message("To contact name is:"+Tocontact);
			 SkySiteUtils.waitTill(3000);		 
			 if((Subject.contentEquals(SubjectName))&&(Contact.contentEquals(Tocontact))
					 &&(result1==true)&&(result2==true)&&(result3==true))
				 return true;
			 else
				 return false;
	   	}
	 
	 /**
	  * Method written for Random(Task)
	  *  Scripted By: Sekhar	
	  * @return
	  */

	 public String Random_Task()
	 {
		 	
		 String str = "Task";
		 Random r = new Random(System.currentTimeMillis());
		 String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		 return abc;		 
	 }
	 
	 

		@FindBy(css = "#ProjectMenu1_Task>a")
		WebElement btntask;	
		
		@FindBy(xpath = "//*[@id='SelTaskAssignList']/div[1]/table/tbody/tr[2]/td[2]/div")
		WebElement SelTaskAssignList;
		
		@FindBy(css = "#btnNewTask")
		WebElement btnNewTask;
		
		@FindBy(css = "#txtTaskName")
		WebElement txtTaskName;
		
		@FindBy(css = "#txtTaskSubject")
		WebElement txtTaskSubject;
		
		@FindBy(css = "#txtDate")
		WebElement txtDate;
		
		@FindBy(css = ".day.active")
		WebElement dayactive;
		
		@FindBy(css = "#Img1")
		WebElement datefar;
		
		@FindBy(xpath = "html/body/div[8]/div[1]/table/thead/tr[1]/th[3]")
		WebElement datenext;
		
		@FindBy(xpath = "html/body/div[8]/div[1]/table/tbody/tr[2]/td[4]")
		WebElement duedate;
		
		@FindBy(xpath = "(//button[@class='btn dropdown-toggle btn-default'])[1]")
		WebElement  btndefault;
			
		@FindBy(xpath = "(//span[@class='text'])[2]")
		WebElement  btninprogress;
		
		@FindBy(xpath = "//body[@id='tinymce']")
		WebElement tinymce;
		
		@FindBy(css = "#btnAddTUFromAB")
		WebElement btnAddTUFromAB;
		
		@FindBy(css = "#rbTeamMembers")
		WebElement rbTeamMembers;
		
		@FindBy(css = "#btn_addMeemberAndclose")
		WebElement btnaddMeemberAndclose;
		
	/** 
	 * Method written for Adding Task with subject
	 * Scripted By: Sekhar     
	 * @throws AWTException 
	 */	
		
	public boolean Add_Task_withSubject(String TaskName,String SubjectName,String Contact,String Email)
	{
		boolean result1=false;  		
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		btntask.click();
		Log.message("Task button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));//switch to my frame
		Log.message("Swich to I Frame Success!!"); 
		SkySiteUtils.waitTill(3000);
		btnNewTask.click();//click on the add new task
		Log.message("Add button has been clicked");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, SelTaskAssignList, 120);  
		txtTaskName.sendKeys(TaskName);
		Log.message("Task name has been entered");
		txtTaskSubject.sendKeys(SubjectName);
		Log.message("Subject name has been entered");
		SkySiteUtils.waitTill(2000);
		//driver.findElement(By.xpath("(//i[@class='icon icon-calendar'])[1]")).click();
		//SkySiteUtils.waitTill(3000);
		txtDate.click();
		Log.message("date button has been clicked");
		SkySiteUtils.waitTill(2000);	
		dayactive.click();
		Log.message("active day has been clicked");
		SkySiteUtils.waitTill(2000);	 		
		datefar.click();	
		Log.message("date icon button has been clicked");
		SkySiteUtils.waitTill(3000); 		
		datenext.click();
		Log.message("forward date button has been clicked");
		SkySiteUtils.waitTill(3000);
		duedate.click();
		Log.message("due date has been clicked");
		SkySiteUtils.waitTill(3000);
		btndefault.click();
		Log.message("default drop down button has been clicked");
		btninprogress.click();
		Log.message("In progress has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("txtNote_ifr")));//switch to text note frame
		Log.message("Switch to II Frame Success!!"); 
		SkySiteUtils.waitTill(3000);
		tinymce.sendKeys(SubjectName);//enter the Description
		Log.message("Description has been entered");
		SkySiteUtils.waitTill(5000); 		
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));//switch to my frame
		Log.message("Switch to I Frame Success!!"); 	
		SkySiteUtils.waitTill(3000);
		btnAddTUFromAB.click();//click on Add Member
		Log.message("Add member has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent(); 		
		rbTeamMembers.click();//click on radio button
		Log.message("collection team member radio button has been clicked");
		SkySiteUtils.waitTill(8000);  		
		int Checkbox_Count=0;
		int i = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[@id='divGridPTeam']/div[2]/table/tbody/tr/td[1]/img"));//Taking check box Count
		for (WebElement Element : allElements)
		{ 
			Checkbox_Count = Checkbox_Count+1; 	
		}
		Log.message("Check box Count is: "+Checkbox_Count);
		//Getting Folder Names and machine
		for(i = 1;i<=Checkbox_Count;i++)
		{					
			String ConatctName=driver.findElement(By.xpath("(//*[@id='divGridPTeam']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText().toString();
			Log.message("Contact Name is:"+ConatctName);
			String Act_MailID=driver.findElement(By.xpath("(//*[@id='divGridPTeam']/div[2]/table/tbody/tr/td[4])["+i+"]")).getText().toString();
			Log.message("Act Email ID is:"+Act_MailID);			
			SkySiteUtils.waitTill(3000);
			if((ConatctName.contentEquals(Contact))&&((Act_MailID.trim()).contentEquals(Email.trim())))
			{	
				result1=true;
				Log.message("Expected Contact Selected Successfully");	
				driver.findElement(By.xpath("(//*[@id='divGridPTeam']/div[2]/table/tbody/tr/td[1]/img)["+i+"]")).click();//select Check box
				SkySiteUtils.waitTill(5000);
				break;
			}	
		}
		if(result1==true)
		{
			Log.message("Expected Contact Selected Successfully");	
		}
		else
		{
			Log.message("Expected Contact Selected UnSuccessfully");	
		}
		SkySiteUtils.waitTill(5000);
		btnaddMeemberAndclose.click();
		Log.message("Add member & close has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("switch to my frame");
		SkySiteUtils.waitTill(3000);
		String Exp_Contact = driver.findElement(By.xpath("//*[@id='SelTaskAssignList']/div[2]/table/tbody/tr[2]/td[2]/a")).getText();
		Log.message("Exp Contact name is:"+Exp_Contact);	
		SkySiteUtils.waitTill(5000);
		if(Exp_Contact.contentEquals(Contact)) 	  			
			return true;	  			
	  	else
	  		Log.message("Expected Contact Unsuccessfull");
		return false;  		
	}
	
	 @FindBy(css = "#rgtpnlMore")
	 WebElement btniconiconadd;
		
	 @FindBy(xpath = "//*[@id='FileActionMenu']/li[1]/a")
	 WebElement btnFileActionMenu;	 	
		 
	 @FindBy(xpath = "//*[@id='FileActionMenu']/li[2]/a")
	 WebElement btnFileActioncoll;	
		 
	 @FindBy(xpath = "//*[@id='divFilesGrid']/div[2]/table/tbody/tr[2]/td[1]/img")
	 WebElement chkboxdivFilesGrid;
			 				
	 @FindBy(xpath = "//input[@id='btnSave']")
	 WebElement btnbtnSave;
		
	 @FindBy(xpath = "//button[@id='bdnReset']")
	 WebElement btnbdnReset;
		 
	 /** 
	  * Method written for Attach file(internal and external)in task
	  * Scripted By: Sekhar     
	  * @throws IOException 
	  * @throws AWTException 
	  */
	 public boolean Attach_file_task(String FolderPath,String TaskName,String SubjectName,String Contact) throws IOException
	 {
		 boolean result1=false;  
		 boolean result2=false;
		 SkySiteUtils.waitTill(3000);
		 btniconiconadd.click();
		 Log.message("Add button has been clicked");
		 SkySiteUtils.waitTill(3000);
		 btnFileActionMenu.click();
		 Log.message("Attach from your computer button has been clicked");
		 SkySiteUtils.waitTill(10000);  		 				
		 String parentHandle = driver.getWindowHandle(); //Getting parent window
		 Log.message(parentHandle);
		 for(String winHandle : driver.getWindowHandles())
		 {
			 driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		 }	
		 SkySiteUtils.waitTill(10000);
		 driver.switchTo().defaultContent();
		 btnselectfile.click();//click on choose File button
		 SkySiteUtils.waitTill(5000);	
		 
		 // Writing File names into a text file for using in AutoIT Script
		 BufferedWriter output;
		 randomFileName rn = new randomFileName();
		 //String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		 String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		 output = new BufferedWriter(new FileWriter(tmpFileName, true));
		 
		 String expFilename = null;
		 File[] files = new File(FolderPath).listFiles();
		 
		 for (File file : files) 
		 {
			 if (file.isFile()) 
			 {
				 expFilename = file.getName();// Getting File Names into a variable
				 Log.message("Expected File name is:" + expFilename);
				 output.append('"' + expFilename + '"');
				 output.append(" ");
				 SkySiteUtils.waitTill(5000);
			 }		
		 }
		 output.flush();
		 output.close();
		 
		 String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
		 // Executing .exe autoIt file
		 Runtime.getRuntime().exec(AutoItexe_Path + " " + FolderPath + " " + tmpFileName);
		 Log.message("AutoIT Script Executed!!");
		 SkySiteUtils.waitTill(30000);
		 
		 try 
		 {
			 File file = new File(tmpFileName);
			 if (file.delete()) 
			 {
				 Log.message(file.getName() + " is deleted!");
			 }	
			 else 
			 {
				 Log.message("Delete operation is failed.");
			 }	
		 } 
		 catch (Exception e)
		 {
			 Log.message("Exception occured!!!" + e);
		 }
		 SkySiteUtils.waitTill(8000);
		 btnFileUpload.click();// Clicking on Upload button
		 Log.message("Upload button has been clicked");
		 SkySiteUtils.waitTill(30000);
		 driver.switchTo().window(parentHandle);// Switch back to folder page
		 SkySiteUtils.waitTill(5000);  		
		 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		 SkySiteUtils.waitTill(5000);
		 String Exp_FileName=driver.findElement(By.xpath("//*[@id='divAttachment']/div[2]/table/tbody/tr[2]/td[1]/a[2]")).getText();
		 Log.message("Exp File Name is:"+Exp_FileName);
		 if(driver.findElement(By.xpath("//*[@id='divAttachment']/div[2]/table/tbody/tr[2]/td[1]/a[2]")).isDisplayed())
		 {
			 result1=true;
			 Log.message("Attachment File From Computer Successfully");
		 }		
		 else	
		 {
			 result1=false;
			 Log.message("Attachment File From Computer UnSuccessfully");  	
		 }	
		 SkySiteUtils.waitTill(2000);
		 btniconiconadd.click();
		 Log.message("Add button has been clicked");
		 SkySiteUtils.waitTill(2000);
		 btnFileActioncoll.click();
		 Log.message("Attach From your collection button has been clicked");
		 SkySiteUtils.waitTill(6000);
		 String parentHandle1 = driver.getWindowHandle(); //Getting parent window
		 Log.message(parentHandle1);
		 for(String winHandle : driver.getWindowHandles())
		 {
			 driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		 }	
		 SkySiteUtils.waitTill(2000);
		 driver.switchTo().defaultContent();
		 chkboxdivFilesGrid.click();
		 Log.message("all check box button has been clicked");
		 SkySiteUtils.waitTill(2000);
		 btnbtnprimary.click(); 
		 Log.message("Add&close button has been clicked");
		 SkySiteUtils.waitTill(3000);				 
		 driver.switchTo().window(parentHandle1);//Switch back to folder page 	
		 SkySiteUtils.waitTill(5000);
		 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		 SkySiteUtils.waitTill(5000);
		 String Exp_FileName2=driver.findElement(By.xpath("//*[@id='divAttachment']/div[2]/table/tbody/tr[3]/td[1]/a[2]")).getText();
		 Log.message("Exp File name2 is:"+Exp_FileName2);
		 if(driver.findElement(By.xpath("//*[@id='divAttachment']/div[2]/table/tbody/tr[3]/td[1]/a[2]")).isDisplayed())
		 {
			 result2=true;
			 Log.message("Attach From Project Successfully");									
		 }	
		 else	
		 {
			 result2=false;
			 Log.message("Attach From Project UnSuccessfully");		
		 }	
		 SkySiteUtils.waitTill(3000);
		 btnbtnSave.click();
		 Log.message("create task button has been clicked");
		 SkySiteUtils.waitTill(5000);
		 btnbdnReset.click();
		 Log.message("Reset button has been clicked");
		 SkySiteUtils.waitTill(5000);
		 String Exp_TaskName=driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[3]/a[2]")).getText();
		 Log.message("Exp Task Name is:"+Exp_TaskName);
		 SkySiteUtils.waitTill(2000);
		 String Subject_Name=driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[4]")).getText();
		 Log.message("Subject Name is:"+Subject_Name);
		 SkySiteUtils.waitTill(2000);
		 String Assigned_To=driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[6]")).getText();
		 Log.message("Assigned To is:"+Assigned_To);
		 if((Exp_TaskName.contentEquals(TaskName))&&(Subject_Name.contentEquals(SubjectName))
				 &&(Assigned_To.contentEquals(Contact)))
	  			return true;
		 else
			 return false;			
	 }	
		
	 
	 // Deleting files from a folder
	 	public boolean Delete_Files_From_Folder(String Folder_Path) 
	 	{
	 		try
	 		{
	 			SkySiteUtils.waitTill(5000);
	 			Log.message("Cleaning download folder!!! ");
	 			File file = new File(Folder_Path);
	 			String[] myFiles;
	 			if(file.isDirectory())
	 			{
	 				myFiles = file.list();
	 				for (int i = 0; i < myFiles.length; i++) 
	 				{
	 					File myFile = new File(file, myFiles[i]);
	 					myFile.delete();
	 					SkySiteUtils.waitTill(5000);
	 				}
	 				Log.message("Available Folders/Files are deleted from download folder successfully!!!");
	 			}
	 		} // end try
	 		catch (Exception e)
	 		{
	 			Log.message("Available Folders/Files are deleted from download folder successfully!!!");
	 		}
	 		return false;
	 	}
	 @FindBy(xpath = "(//i[@class='icon icon-export ico-lg'])[2]")
	 WebElement btniconexport1;
		
	 @FindBy(xpath = "(//i[@class='icon icon-csv icon-lg'])[3]")
	 WebElement btnexportascsv2;
		
	 @FindBy(xpath = "//*[@id='Button1']")
	// @FindBy(xpath = "//*[@id='rgtpnlMore']")
	 WebElement btnmoreoption;
	    
	    
	    /** 
	     * Method written for export as csv in collection File.
	     * Scripted By: Sekhar  	
		 * @throws IOException 
	     * @throws AWTException 
	     */

		public boolean Export_CSV_CollectionFile(String DownloadPath,String DownloadPath1,String fileName) throws IOException, AWTException
	    {  
	    	boolean result1=false;    	
	    	SkySiteUtils.waitTill(5000); 
	    	
			this.Delete_Files_From_Folder(DownloadPath);
			SkySiteUtils.waitTill(5000);	
			
			btnmoreoption.click();
	    	Log.message("more option button has been clicked");
	    	SkySiteUtils.waitTill(5000);  
	    	
	    	//mouse over operations
	       	Actions actions = new Actions(driver); 
	       	WebElement menuHoverLink = driver.findElement(By.xpath("(//i[@class='icon icon-export'])[3]")); //select Export Folder
	       	
	       	actions.moveToElement(menuHoverLink); 
	       	WebElement subLink = driver.findElement(By.xpath("(//i[@class='icon icon-csv icon-lg'])[5]")); //select Export to CSV
	       	actions.moveToElement(subLink); 
	       	actions.click();     
	       	actions.perform(); 	
	     	SkySiteUtils.waitTill(7000);        	
	    	// Get Browser name on run-time.
	    	Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
	    	String browserName = caps.getBrowserName();
	    	Log.message("Browser name on run-time is: " + browserName);
	    	
	    	if (browserName.contains("firefox")) 
	    	{
	    		// Handling Download PopUp of firefox browser using robot
	    		Robot robot1 = null;
	    		robot1 = new Robot();			
	    		robot1.keyPress(KeyEvent.VK_ALT);		
	    		robot1.keyPress(KeyEvent.VK_S);
	    		SkySiteUtils.waitTill(4000);
	    		robot1.keyRelease(KeyEvent.VK_S);
	    		robot1.keyRelease(KeyEvent.VK_ALT);			
	    		SkySiteUtils.waitTill(3000);
	    		robot1.keyPress(KeyEvent.VK_ENTER);
	    		robot1.keyRelease(KeyEvent.VK_ENTER);
	    		SkySiteUtils.waitTill(20000);
	    	}
	    	SkySiteUtils.waitTill(25000);
	    	// After checking whether download file or not
	    	String expFilename1 = null;
	    	
	    	File[] files1 = new File(DownloadPath).listFiles();
	    	
	    	for(File file1 : files1)
			{
				if(file1.isFile()) 
				{
					expFilename1=file1.getName();//Getting File Names into a variable
					Log.message("Expected File name is:"+expFilename1);				
				}
			}
			SkySiteUtils.waitTill(10000);
			//Working on CSV file
			String csvFileToRead = DownloadPath1 +expFilename1;
			Log.message(csvFileToRead);		
			BufferedReader br = null;
			String line = ""; 
			
			String splitBy = ",";
			int count = 0;		
			String DocumentName = null;
			try
			{
				br = new BufferedReader(new FileReader(csvFileToRead)); 
				while ((line = br.readLine()) != null) 
				{  
					count=count+1;
					Log.message("Downloaded csv file from data in : "+count+ " rows");
					String[] ActValue = line.split(splitBy);
					DocumentName = ActValue[0];
					DocumentName = DocumentName.replace("\"", "");
					Log.message("File Name from csv file is: "+ DocumentName);						
				}
				Log.message("Downloaded csv file have data in rows count is:"+count);
				Log.message(DocumentName);
				Log.message(fileName);
				if(DocumentName.trim().contentEquals(fileName.trim()))
				{
					result1=true;
					Log.message("Validation the Export as CSV in Collection file Successfull!!!!!");
				}
				else
				{
					result1=false;
					Log.message("Validation the Export as CSV in Collection file UnSuccessfull!!!!!");				
				}
				br.close();
			}		
			catch (FileNotFoundException e)
			{  
				e.printStackTrace();
			}
			catch (IOException e)
			{ 
				e.printStackTrace();
			}		
			if(result1==true)
				return true;
			else
				return false;
	    }
		
		
		@FindBy(xpath = "//i[@class='icon icon-star ico-lg icon-files']")
		WebElement btniconfiles;	
		
		@FindBy(xpath = "(//i[@class='icon icon-xls icon-lg'])[3]")
		WebElement btnexportasexcel2;
		
		/** 
	     * Method written for export as excel in collection File.
	     * Scripted By: Sekhar     
		 * @throws IOException 
		 * @throws IOException 
	     * @throws AWTException 
	     */
		
	    public boolean Export_Excel_CollectionFile(String DownloadPath,String DownloadPath1) throws IOException, AWTException,InvalidFormatException
	    {  
	    	boolean result1=false;    	
	    	SkySiteUtils.waitTill(3000);
	    	driver.switchTo().defaultContent();
	    	btniconfiles.click();
	    	Log.message("files button has been clicked");
	    	SkySiteUtils.waitTill(5000);    	
	   		// Calling delete files from download folder script
	    	this.Delete_Files_From_Folder(DownloadPath);
	    	SkySiteUtils.waitTill(5000);
	    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));    	
	    	SkySiteUtils.waitTill(5000);
	    	btnmoreoption.click();
	    	Log.message("more option button has been clicked");
	    	SkySiteUtils.waitTill(5000);  
	    	
	    	//mouse over operations
	       	Actions actions = new Actions(driver); 
	       	WebElement menuHoverLink = driver.findElement(By.xpath("(//i[@class='icon icon-export'])[3]")); //select Export Folder
	       	
	       	actions.moveToElement(menuHoverLink); 
	       	WebElement subLink = driver.findElement(By.xpath("(//i[@class='icon icon-xls icon-lg'])[5]")); //select Export to CSV
	       	actions.moveToElement(subLink); 
	       	actions.click();      
	       	actions.perform(); 	
	     	SkySiteUtils.waitTill(7000);
	     	    	
	    	// Get Browser name on run-time.
	    	Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
	    	String browserName = caps.getBrowserName();
	    	Log.message("Browser name on run-time is: " + browserName);
	    	
	    	if (browserName.contains("firefox")) 
	    	{
	    		// Handling Download PopUp of firefox browser using robot
	    		Robot robot1 = null;
	    		robot1 = new Robot();			
	    		robot1.keyPress(KeyEvent.VK_ALT);		
	    		robot1.keyPress(KeyEvent.VK_S);
	    		SkySiteUtils.waitTill(4000);
	    		robot1.keyRelease(KeyEvent.VK_S);
	    		robot1.keyRelease(KeyEvent.VK_ALT);			
	    		SkySiteUtils.waitTill(3000);
	    		robot1.keyPress(KeyEvent.VK_ENTER);
	    		robot1.keyRelease(KeyEvent.VK_ENTER);
	    		SkySiteUtils.waitTill(20000);
	    	}
	    	SkySiteUtils.waitTill(25000);
	    	// After checking whether download file or not
	    	String expFilename = null;
	    	
	    	File[] files = new File(DownloadPath).listFiles();
	    	
	    	for(File file : files)
			{
				if(file.isFile()) 
				{
					expFilename=file.getName();//Getting File Names into a variable
					Log.message("Expected File name is:"+expFilename);				
				}
			}
			SkySiteUtils.waitTill(10000);
			//Working on Excel file
			String xlsxFileToRead = DownloadPath1 +expFilename;
			Log.message(xlsxFileToRead);
								
			InputStream ExcelFileToRead = new FileInputStream(xlsxFileToRead);
			XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);   

			XSSFSheet sheet=wb.getSheetAt(0);
			XSSFRow row; 
			XSSFCell cell;

			Iterator<Row> rows = sheet.rowIterator();
			
			System.out.println(rows);
			
			while (rows.hasNext())
			{
				row=(XSSFRow) rows.next();
				Iterator cells = row.cellIterator();
				
				while (cells.hasNext())
				{
					cell=(XSSFCell) cells.next();
			
					if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)
					{
						Log.message(cell.getStringCellValue()+" ");
					}
					else if(cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC)
					{
						Log.message(cell.getNumericCellValue()+" ");
					}
					else
					{
						//System.out.println("Excel as File UnSuccessfully Validated");
					}
				}	
				System.out.println();	
				try
				{
					ExcelFileToRead.close();
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
				}	
			}			
			SkySiteUtils.waitTill(20000);
			result1=true;
			if(result1==true)
			{
				Log.message("Excel as File Successfully Validated");
				return true;
			}
			else
			{
				Log.message("Excel as File UnSuccessfully Validated");
				return false;   
			}
	    }
	    

	    @FindBy(css = "#lftpnlMore")
		 WebElement btnlftpnlMore;
		
	   
	   /** 
	    * Method written for export as csv in collection Folder.
	    * Scripted By: Sekhar  	
		 * @throws IOException 
	    * @throws AWTException 
	    */

		public boolean Export_CSV_CollectionFolder(String DownloadPath,String DownloadPath1,String fileName) throws IOException, AWTException
	   {  
	   	boolean result1=false;    	
	   	SkySiteUtils.waitTill(5000);  
	   	// Calling delete files from download folder script
	   	this.Delete_Files_From_Folder(DownloadPath);
	   	SkySiteUtils.waitTill(5000);
	   	//driver.switchTo().frame(driver.findElement(By.id("myFrame")));    	
	   	//SkySiteUtils.waitTill(5000);
	   	btnlftpnlMore.click();
	   	Log.message("more button has been clicked");
	   	SkySiteUtils.waitTill(5000); 
	   	
	  //mouse over operations
	   	Actions actions = new Actions(driver); 
	   	WebElement menuHoverLink = driver.findElement(By.xpath("//i[@class='icon icon-export']")); //select Export Folder
	   	
	   	actions.moveToElement(menuHoverLink); 
	   	WebElement subLink = driver.findElement(By.xpath("(//span[@class='pull-left'])[1]")); //select Export to CSV
	   	actions.moveToElement(subLink); 
	   	actions.click(); 
	   	actions.perform(); 	
	 	SkySiteUtils.waitTill(5000); 
	 	
	   	// Get Browser name on run-time.
	   	Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
	   	String browserName = caps.getBrowserName();
	   	Log.message("Browser name on run-time is: " + browserName);
	   	
	   	if (browserName.contains("firefox")) 
	   	{
	   		// Handling Download PopUp of firefox browser using robot
	   		Robot robot1 = null;
	   		robot1 = new Robot();			
	   		robot1.keyPress(KeyEvent.VK_ALT);		
	   		robot1.keyPress(KeyEvent.VK_S);
	   		SkySiteUtils.waitTill(4000);
	   		robot1.keyRelease(KeyEvent.VK_S);
	   		robot1.keyRelease(KeyEvent.VK_ALT);			
	   		SkySiteUtils.waitTill(3000);
	   		robot1.keyPress(KeyEvent.VK_ENTER);
	   		robot1.keyRelease(KeyEvent.VK_ENTER);
	   		SkySiteUtils.waitTill(20000);
	   	}
	   	SkySiteUtils.waitTill(25000);
	   	// After checking whether download file or not
	   	String expFilename1 = null;
	   	
	   	File[] files1 = new File(DownloadPath).listFiles();
	   	
	   	for(File file1 : files1)
			{
				if(file1.isFile()) 
				{
					expFilename1=file1.getName();//Getting File Names into a variable
					Log.message("Expected File name is:"+expFilename1);				
				}
			}
			SkySiteUtils.waitTill(10000);
			//Working on CSV file
			String csvFileToRead = DownloadPath1 +expFilename1;
			Log.message(csvFileToRead);		
			BufferedReader br = null;
			String line = ""; 
			
			String splitBy = ",";
			int count = 0;		
			String DocumentName = null;
			try
			{
				br = new BufferedReader(new FileReader(csvFileToRead)); 
				while ((line = br.readLine()) != null) 
				{  
					count=count+1;
					Log.message("Downloaded csv file from data in : "+count+ " rows");
					String[] ActValue = line.split(splitBy);
					DocumentName = ActValue[0];
					DocumentName = DocumentName.replace("\"", "");
					Log.message("File Name from csv file is: "+ DocumentName);						
				}
				Log.message("Downloaded csv file have data in rows count is:"+count);
				Log.message(DocumentName);
				if(DocumentName.trim().contains(fileName.trim()))
				{
					result1=true;
					Log.message("Validation the Export as CSV in Collection folder Successfull!!!!!");
				}
				else
				{
					result1=false;
					Log.message("Validation the Export as CSV in Collection folder UnSuccessfull!!!!!");				
				}
				br.close();
			}		
			catch (FileNotFoundException e)
			{  
				e.printStackTrace();
			}
			catch (IOException e)
			{ 
				e.printStackTrace();
			}		
			if(result1==true)
				return true;
			else
				return false;
	   }
		
		/** 
		    * Method written for export as excel in collection Folder.
		    * Scripted By: Sekhar     
			 * @throws IOException 
		    * @throws AWTException 
		    */
			
		   public boolean Export_Excel_CollectionFolder(String DownloadPath,String DownloadPath1) throws IOException, AWTException,InvalidFormatException
		   {  
		   	boolean result1=false;    	
		   	SkySiteUtils.waitTill(3000);
		   	driver.switchTo().defaultContent();
		   	btniconfiles.click();
		   	Log.message("files button has been clicked");
		   	SkySiteUtils.waitTill(5000);    	
		   	// Calling delete files from download folder script
		 	this.Delete_Files_From_Folder(DownloadPath);
		   	SkySiteUtils.waitTill(5000);
		   	driver.switchTo().frame(driver.findElement(By.id("myFrame")));    	
		   	SkySiteUtils.waitTill(5000);   	
			btnlftpnlMore.click();
		   	Log.message("more button has been clicked");
		   	SkySiteUtils.waitTill(5000);    	
		  //mouse over operations
		   	Actions actions = new Actions(driver); 
		   	WebElement menuHoverLink = driver.findElement(By.xpath("//i[@class='icon icon-export']")); //select Export Folder
		   	
		   	actions.moveToElement(menuHoverLink); 
		   	WebElement subLink = driver.findElement(By.xpath("(//span[@class='pull-left'])[2]")); //select Export to Excel
		   	actions.moveToElement(subLink); 
		   	actions.click(); 
		   	actions.perform(); 	
		 	SkySiteUtils.waitTill(5000);    	
		   	// Get Browser name on run-time.
		   	Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		   	String browserName = caps.getBrowserName();
		   	Log.message("Browser name on run-time is: " + browserName);
		   	
		   	if (browserName.contains("firefox")) 
		   	{
		   		// Handling Download PopUp of firefox browser using robot
		   		Robot robot1 = null;
		   		robot1 = new Robot();			
		   		robot1.keyPress(KeyEvent.VK_ALT);		
		   		robot1.keyPress(KeyEvent.VK_S);
		   		SkySiteUtils.waitTill(4000);
		   		robot1.keyRelease(KeyEvent.VK_S);
		   		robot1.keyRelease(KeyEvent.VK_ALT);			
		   		SkySiteUtils.waitTill(3000);
		   		robot1.keyPress(KeyEvent.VK_ENTER);
		   		robot1.keyRelease(KeyEvent.VK_ENTER);
		   		SkySiteUtils.waitTill(20000);
		   	}
		   	SkySiteUtils.waitTill(25000);
		   	// After checking whether download file or not
		   	String expFilename = null;
		   	
		   	File[] files = new File(DownloadPath).listFiles();
		   	
		   	for(File file : files)
				{
					if(file.isFile()) 
					{
						expFilename=file.getName();//Getting File Names into a variable
						Log.message("Expected File name is:"+expFilename);				
					}
				}
				SkySiteUtils.waitTill(10000);
				//Working on Excel file
				String xlsxFileToRead = DownloadPath1 +expFilename;
				Log.message(xlsxFileToRead);
									
				InputStream ExcelFileToRead = new FileInputStream(xlsxFileToRead);
				XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);   

				XSSFSheet sheet=wb.getSheetAt(0);
				XSSFRow row; 
				XSSFCell cell;

				Iterator<Row> rows = sheet.rowIterator();
				
				System.out.println(rows);
				
				while (rows.hasNext())
				{
					row=(XSSFRow) rows.next();
					Iterator cells = row.cellIterator();
					
					while (cells.hasNext())
					{
						cell=(XSSFCell) cells.next();
				
						if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)
						{
							Log.message(cell.getStringCellValue()+" ");
						}
						else if(cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC)
						{
							Log.message(cell.getNumericCellValue()+" ");
						}
						else
						{
							//System.out.println("Excel as File UnSuccessfully Validated");
						}
					}	
					System.out.println();	
					try
					{
						ExcelFileToRead.close();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}	
				}			
				SkySiteUtils.waitTill(20000);
				result1=true;
				if(result1==true)
				{
					Log.message("Excel as File Successfully Validated");
					return true;
				}
				else
				{
					Log.message("Excel as File UnSuccessfully Validated");
					return false;   
				}
		   }		   
		  
		   @FindBy(css = "#expBtn")
		   WebElement btnexpBtn;
		   
		   @FindBy(xpath = "//i[@class='icon icon-csv icon-lg']")
		   WebElement btniconiconcsv1;
		  	

		  /** 
		   * Method written for export as csv in Communication.
		   * Scripted By: Sekhar  	
		  	 * @throws IOException 
		   * @throws AWTException 
		   */

		  public boolean Export_CSV_Communication(String DownloadPath,String DownloadPath1,String fileName) throws IOException, AWTException
		  {  
		  	boolean result1=false;    	
		  	SkySiteUtils.waitTill(5000);		
		  	// Calling delete files from download folder script
		  	this.Delete_Files_From_Folder(DownloadPath);	
		  	SkySiteUtils.waitTill(5000);
			driver.switchTo().defaultContent();
		  	btniconcommunication.click();
		  	Log.message("communications button has been clicked");
		  	SkySiteUtils.waitTill(2000);	
		  	driver.switchTo().frame(driver.findElement(By.id("myFrame")));    	
		  	SkySiteUtils.waitTill(2000);
		  	btnexpBtn.click();
		  	Log.message("Export button has been clicked");
		  	SkySiteUtils.waitTill(3000); 
		  	btniconiconcsv1.click();
		  	Log.message("Export as csv button has been clicked");
		  	SkySiteUtils.waitTill(5000);
		  	// Get Browser name on run-time.
		  	Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		  	String browserName = caps.getBrowserName();
		  	Log.message("Browser name on run-time is: " + browserName);
		  	
		  	if (browserName.contains("firefox")) 
		  	{
		  		// Handling Download PopUp of firefox browser using robot
		  		Robot robot1 = null;
		  		robot1 = new Robot();			
		  		robot1.keyPress(KeyEvent.VK_ALT);		
		  		robot1.keyPress(KeyEvent.VK_S);
		  		SkySiteUtils.waitTill(4000);
		  		robot1.keyRelease(KeyEvent.VK_S);
		  		robot1.keyRelease(KeyEvent.VK_ALT);			
		  		SkySiteUtils.waitTill(3000);
		  		robot1.keyPress(KeyEvent.VK_ENTER);
		  		robot1.keyRelease(KeyEvent.VK_ENTER);
		  		SkySiteUtils.waitTill(20000);
		  	}
		  	SkySiteUtils.waitTill(25000);
		  	// After checking whether download file or not
		  	String expFilename1 = null;
		  	
		  	File[] files1 = new File(DownloadPath).listFiles();
		  	
		  	for(File file1 : files1)
		  		{
		  			if(file1.isFile()) 
		  			{
		  				expFilename1=file1.getName();//Getting File Names into a variable
		  				Log.message("Expected File name is:"+expFilename1);				
		  			}
		  		}
		  		SkySiteUtils.waitTill(10000);
		  		//Working on CSV file
		  		String csvFileToRead = DownloadPath1 +expFilename1;
		  		Log.message(csvFileToRead);		
		  		BufferedReader br = null;
		  		String line = ""; 
		  		
		  		String splitBy = ",";
		  		int count = 0;		
		  		String DocumentName = null;
		  		try
		  		{
		  			br = new BufferedReader(new FileReader(csvFileToRead)); 
		  			while ((line = br.readLine()) != null) 
		  			{  
		  				count=count+1;
		  				Log.message("Downloaded csv file from data in : "+count+ " rows");
		  				String[] ActValue = line.split(splitBy);
		  				DocumentName = ActValue[3];
		  				DocumentName = DocumentName.replace("\"", "");
		  				Log.message("File Name from csv file is: "+ DocumentName);						
		  			}
		  			Log.message("Downloaded csv file have data in rows count is:"+count);
		  			Log.message(DocumentName);
		  			Log.message(fileName);
		  			if(DocumentName.trim().equalsIgnoreCase(fileName.trim()))
		  			{
		  				result1=true;
		  				Log.message("Validation the Export as CSV in Communication Successfull!!!!!");
		  			}
		  			else
		  			{
		  				result1=false;
		  				Log.message("Validation the Export as CSV in Communication UnSuccessfull!!!!!");				
		  			}
		  			br.close();
		  		}		
		  		catch (FileNotFoundException e)
		  		{  
		  			e.printStackTrace();
		  		}
		  		catch (IOException e)
		  		{ 
		  			e.printStackTrace();
		  		}		
		  		if(result1==true)
		  			return true;
		  		else
		  			return false;
		  }	

			@FindBy(xpath = "//i[@class='icon icon-xls icon-lg']")
			WebElement btniconiconxls1;	
			
		/** 
		 * Method written for export as excel in Communication.
		 * Scripted By: Sekhar     
		 * @throws IOException 
		 * @throws AWTException 
		 */
			
		public boolean Export_Excel_Communication(String DownloadPath,String DownloadPath1) throws IOException, AWTException,InvalidFormatException
		{  
			boolean result1=false;    	
			SkySiteUtils.waitTill(3000);
			driver.switchTo().defaultContent();	  	
			// Calling delete files from download folder script
			this.Delete_Files_From_Folder(DownloadPath);
			SkySiteUtils.waitTill(5000); 	
			btniconcommunication.click();
			Log.message("Communicatios button has been clicked");
			SkySiteUtils.waitTill(2000);	
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));    	
			SkySiteUtils.waitTill(2000);	
			btnexpBtn.click();
			Log.message("Export button has been clicked");
			SkySiteUtils.waitTill(3000); 
			btniconiconxls1.click();
			Log.message("Export as Export button has been clicked");
			SkySiteUtils.waitTill(5000);	  	
			// Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: " + browserName);
			
			if (browserName.contains("firefox")) 
			{
				// Handling Download PopUp of firefox browser using robot
				Robot robot1 = null;
				robot1 = new Robot();			
				robot1.keyPress(KeyEvent.VK_ALT);		
				robot1.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(4000);
				robot1.keyRelease(KeyEvent.VK_S);
				robot1.keyRelease(KeyEvent.VK_ALT);			
				SkySiteUtils.waitTill(3000);
				robot1.keyPress(KeyEvent.VK_ENTER);
				robot1.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(20000);
			}
			SkySiteUtils.waitTill(25000);
					
			// After checking whether download file or not
			String expFilename = null;
			
			File[] files = new File(DownloadPath).listFiles();
			
			for(File file : files)
				{
					if(file.isFile()) 
					{
						expFilename=file.getName();//Getting File Names into a variable
						Log.message("Expected File name is:"+expFilename);				
					}
				}
				SkySiteUtils.waitTill(10000);
				//Working on Excel file
				String xlsxFileToRead = DownloadPath1 +expFilename;
				Log.message(xlsxFileToRead);
									
				InputStream ExcelFileToRead = new FileInputStream(xlsxFileToRead);
				XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);   

				XSSFSheet sheet=wb.getSheetAt(0);
				XSSFRow row; 
				XSSFCell cell;

				Iterator<Row> rows = sheet.rowIterator();
				
				System.out.println(rows);
				
				while (rows.hasNext())
				{
					row=(XSSFRow) rows.next();
					Iterator cells = row.cellIterator();
					
					while (cells.hasNext())
					{
						cell=(XSSFCell) cells.next();
				
						if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)
						{
							Log.message(cell.getStringCellValue()+" ");
						}
						else if(cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC)
						{
							Log.message(cell.getNumericCellValue()+" ");
						}
						else
						{
							//System.out.println("Excel as File UnSuccessfully Validated");
						}
					}	
					System.out.println();	
					try
					{
						ExcelFileToRead.close();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}	
				}			
				SkySiteUtils.waitTill(20000);
				result1=true;
				if(result1==true)
				{
					Log.message("Excel as File Successfully Validated");
					return true;
				}
				else
				{
					Log.message("Excel as File UnSuccessfully Validated");
					return false;   
				}		
			}
		@FindBy(xpath = "//i[@class='icon icon-contacts ico-lg']")
		WebElement btncontacts;	
		
		@FindBy(xpath = ".//*[@id='btnExpImpAddBook']")
		WebElement btnExpImpAddBook;
		
		@FindBy(xpath = ".//*[@id='tdbtnExpImpAddBook']/ul/li[1]/a")
		WebElement btnexportcsv;
		
		/** 
		 * Method written for export as csv in Contact.
		 * Scripted By: Sekhar  	
		 * @throws IOException 
		 * @throws AWTException 
		 */
		
		  public boolean Export_CSV_Contact(String DownloadPath,String DownloadPath1,String fileName) throws IOException, AWTException
		  {  
		  	boolean result1=false;    	
		  	SkySiteUtils.waitTill(5000);		
		  	// Calling delete files from download folder script
		  	this.Delete_Files_From_Folder(DownloadPath);	
		  	SkySiteUtils.waitTill(5000);
			driver.switchTo().defaultContent();
			btncontacts.click();
		  	Log.message("contacts button has been clicked");
		  	SkySiteUtils.waitTill(2000);	
		  	driver.switchTo().frame(driver.findElement(By.id("myFrame")));    	
		  	SkySiteUtils.waitTill(2000);
		  	btnExpImpAddBook.click();
		  	Log.message("Export button has been clicked");
		  	SkySiteUtils.waitTill(3000); 
		  	btnexportcsv.click();
		  	Log.message("Export as csv button has been clicked");
		  	SkySiteUtils.waitTill(5000);
		  	// Get Browser name on run-time.
		  	Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		  	String browserName = caps.getBrowserName();
		  	Log.message("Browser name on run-time is: " + browserName);
		  	
		  	if (browserName.contains("firefox")) 
		  	{
		  		// Handling Download PopUp of firefox browser using robot
		  		Robot robot1 = null;
		  		robot1 = new Robot();			
		  		robot1.keyPress(KeyEvent.VK_ALT);		
		  		robot1.keyPress(KeyEvent.VK_S);
		  		SkySiteUtils.waitTill(4000);
		  		robot1.keyRelease(KeyEvent.VK_S);
		  		robot1.keyRelease(KeyEvent.VK_ALT);			
		  		SkySiteUtils.waitTill(3000);
		  		robot1.keyPress(KeyEvent.VK_ENTER);
		  		robot1.keyRelease(KeyEvent.VK_ENTER);
		  		SkySiteUtils.waitTill(20000);
		  	}
		  	SkySiteUtils.waitTill(25000);
		  	// After checking whether download file or not
		  	String expFilename1 = null;
		  	
		  	File[] files1 = new File(DownloadPath).listFiles();
		  	
		  	for(File file1 : files1)
		  		{
		  			if(file1.isFile()) 
		  			{
		  				expFilename1=file1.getName();//Getting File Names into a variable
		  				Log.message("Expected File name is:"+expFilename1);				
		  			}
		  		}
		  		SkySiteUtils.waitTill(10000);
		  		//Working on CSV file
		  		String csvFileToRead = DownloadPath1 +expFilename1;
		  		Log.message(csvFileToRead);		
		  		BufferedReader br = null;
		  		String line = ""; 
		  		
		  		String splitBy = ",";
		  		int count = 0;		
		  		String DocumentName = null;
		  		try
		  		{
		  			br = new BufferedReader(new FileReader(csvFileToRead)); 
		  			while ((line = br.readLine()) != null) 
		  			{  
		  				count=count+1;
		  				Log.message("Downloaded csv file from data in : "+count+ " rows");
		  				String[] ActValue = line.split(splitBy);
		  				DocumentName = ActValue[0];
		  				DocumentName = DocumentName.replace("\"", "");
		  				Log.message("File Name from csv file is: "+ DocumentName);						
		  			}
		  			Log.message("Downloaded csv file have data in rows count is:"+count);
		  			Log.message(DocumentName);
		  			Log.message(fileName);
		  			if(DocumentName.trim().equalsIgnoreCase(fileName.trim()))
		  			{
		  				result1=true;
		  				Log.message("Validation the Export as CSV in Communication Successfull!!!!!");
		  			}
		  			else
		  			{
		  				result1=false;
		  				Log.message("Validation the Export as CSV in Communication UnSuccessfull!!!!!");				
		  			}
		  			br.close();
		  		}		
		  		catch (FileNotFoundException e)
		  		{  
		  			e.printStackTrace();
		  		}
		  		catch (IOException e)
		  		{ 
		  			e.printStackTrace();
		  		}		
		  		if(result1==true)
		  			return true;
		  		else
		  			return false;
		  }	

	  @FindBy(xpath = ".//*[@id='tdbtnExpImpAddBook']/ul/li[2]/a")
	  WebElement btnexportexcel;	
			
		/** 
		 * Method written for export as excel in Contact.
		 * Scripted By: Sekhar     
		 * @throws IOException 
		 * @throws AWTException 
		 */
			
		public boolean Export_Excel_Contact(String DownloadPath,String DownloadPath1) throws IOException, AWTException,InvalidFormatException
		{  
			boolean result1=false;    	
			SkySiteUtils.waitTill(3000);
			driver.switchTo().defaultContent();	  	
			// Calling delete files from download folder script
			this.Delete_Files_From_Folder(DownloadPath);
			SkySiteUtils.waitTill(5000); 	
			btncontacts.click();
			Log.message("contacts button has been clicked");
			SkySiteUtils.waitTill(5000);	
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));    	
			SkySiteUtils.waitTill(2000);	
			btnExpImpAddBook.click();
			Log.message("Export button has been clicked");
			SkySiteUtils.waitTill(3000); 
			btnexportexcel.click();
			Log.message("Export as Export button has been clicked");
			SkySiteUtils.waitTill(5000);	  	
			// Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: " + browserName);
			
			if (browserName.contains("firefox")) 
			{
				// Handling Download PopUp of firefox browser using robot
				Robot robot1 = null;
				robot1 = new Robot();			
				robot1.keyPress(KeyEvent.VK_ALT);		
				robot1.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(4000);
				robot1.keyRelease(KeyEvent.VK_S);
				robot1.keyRelease(KeyEvent.VK_ALT);			
				SkySiteUtils.waitTill(3000);
				robot1.keyPress(KeyEvent.VK_ENTER);
				robot1.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(20000);
			}
			SkySiteUtils.waitTill(25000);
					
			// After checking whether download file or not
			String expFilename = null;
			
			File[] files = new File(DownloadPath).listFiles();
			
			for(File file : files)
				{
					if(file.isFile()) 
					{
						expFilename=file.getName();//Getting File Names into a variable
						Log.message("Expected File name is:"+expFilename);				
					}
				}
				SkySiteUtils.waitTill(10000);
				//Working on Excel file
				String xlsxFileToRead = DownloadPath1 +expFilename;
				Log.message(xlsxFileToRead);
									
				InputStream ExcelFileToRead = new FileInputStream(xlsxFileToRead);
				XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);   

				XSSFSheet sheet=wb.getSheetAt(0);
				XSSFRow row; 
				XSSFCell cell;

				Iterator<Row> rows = sheet.rowIterator();
				
				System.out.println(rows);
				
				while (rows.hasNext())
				{
					row=(XSSFRow) rows.next();
					Iterator cells = row.cellIterator();
					
					while (cells.hasNext())
					{
						cell=(XSSFCell) cells.next();
				
						if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)
						{
							Log.message(cell.getStringCellValue()+" ");
						}
						else if(cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC)
						{
							Log.message(cell.getNumericCellValue()+" ");
						}
						else
						{
							//System.out.println("Excel as File UnSuccessfully Validated");
						}
					}	
					System.out.println();	
					try
					{
						ExcelFileToRead.close();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}	
				}			
				SkySiteUtils.waitTill(20000);
				result1=true;
				if(result1==true)
				{
					Log.message("Excel as File Successfully Validated");
					return true;
				}
				else
				{
					Log.message("Excel as File UnSuccessfully Validated");
					return false;   
				}		
			}	
}